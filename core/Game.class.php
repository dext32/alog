<?php

class Game{

	/**
	 * @var null
	 */
	private $pdo = null;
		
	/**
     * @var null
     */
    private $session = null;
	
	public function __construct($pdo,$session){
		
		if (is_object($pdo)) {
			$this->pdo = $pdo;
		}

		if (is_object($session)) {
            $this->session = $session;
        }
		
		$langPath = 'text/game.lang.php';

		if (file_exists($langPath)) {
			$lang = [];
			require $langPath;
			$this->info = $lang['game'];
		}	
		
	}

	public function setSessionGame($gameId){
		if(empty($gameId)){ return false; }
		elseif (!is_numeric($gameId)) { return false; }
		else {
			$gameId = strip_tags($gameId);
			$gameExist = $this->pdo->prepare('SELECT id FROM games WHERE id = :gameId');
            $gameExist->bindParam(':gameId', $gameId, PDO::PARAM_STR);
            $gameExist->execute();
			
			if ( $gameExist->rowCount() == 0 ) { return false; } 
			else {
				$this->session->set(['gameId' => $gameId], false);
			}
		}
	}
	
	public function getGameData($getGameId = NULL){
		if(isset($_SESSION['gameId'])) { $gameId = $_SESSION['gameId']; }
			elseif (isset($getGameId)) { $gameId = $getGameId; }
				else { return $this->info[0]; }
		$gameData = $this->pdo->prepare('SELECT * FROM games WHERE id = :gameId');
		$gameData->bindParam(':gameId', $gameId, PDO::PARAM_STR);
		$gameData->execute();
		return $gameData->fetch();
	}
	
    public function getGameName($gameId){
        if ( !is_numeric($gameId) ) { return false; }
        $queryGameName = $this->pdo->prepare('SELECT name FROM games WHERE id = :gameId');
        $queryGameName -> bindParam(':gameId',$gameId, PDO::PARAM_INT);
        $queryGameName -> execute();
        $gameData = $queryGameName -> fetch(PDO::FETCH_ASSOC);
        return $gameData['name'];
    }
    
	public function getGameList(){
		$listGames = $this->pdo->prepare('SELECT id,name FROM `games`');
		$listGames->execute();
		return $listGames->fetchAll(PDO::FETCH_ASSOC);
	}
    
    public function getDefaultGame(){
        $defaultGame = $this->getGameList();
        $this->setSessionGame($defaultGame[0]['id']);
        return $defaultGame[0];
    }
	
	public function addNewGame($newGame){
		if (!is_array($newGame)) {
            return false;
        } elseif (!array_key_exists('name', $newGame) || !array_key_exists('max_slots', $newGame)) {
            return false;
        } else {			
            $newGame = array_map('strip_tags', $newGame);

            if (empty($newGame['name']) || empty($newGame['max_slots'])) {
                $return = $this->info[1];
            } else {
                $gameExist = $this->pdo->prepare('SELECT id FROM games WHERE name = :newGame');
                $gameExist->bindParam(':newGame', $newGame['name'], PDO::PARAM_STR);
                $gameExist->execute();

                if ($gameExist->rowCount() == 0) {
					$setNewGame = $this->pdo->prepare('INSERT INTO games (name,max_slots) VALUES (:gameName,:gameMaxSlots)');
					$setNewGame->bindParam(':gameName', $newGame['name'], PDO::PARAM_STR);
					$setNewGame->bindParam(':gameMaxSlots', $newGame['max_slots'], PDO::PARAM_INT);
					$setNewGame->execute();
					$return = $this->info[2];
                } else {
                    $return = $this->info[3];
                }
            }
        }
        echo $return;
	}
	
	public function deleteGame($gameId){
		if(!is_numeric($gameId)) { echo $this->info['4']; return true; }
		$gameExist = $this->pdo->prepare('SELECT id FROM games WHERE id = :gameId');
		$gameExist->bindParam(':gameId', $gameId, PDO::PARAM_STR);
		$gameExist->execute();
		
		if ($gameExist->rowCount() == 0) {
			$return = $this->info['5'];
		} else {
			$deleteGame = $this->pdo->prepare('DELETE FROM games WHERE id = :gameId');
			$deleteGame->bindParam(':gameId', $gameId, PDO::PARAM_STR);
			$deleteGame->execute();
			$return = $this->info['6'];
		}
		echo $return;
	}
	
}

?>
<?php 

	class Queue{
		
		/**
		 * @var null
		 */
		private $pdo = null;
			
		public function __construct($pdo){
			
			if (is_object($pdo)) {
            $this->pdo = $pdo;
			}

			/*
			$langPath = 'text/auth.lang.php';

			if (file_exists($langPath)) {
				$lang = [];
				require_once $langPath;
				$this->info = $lang['auth'];
			}
			*/
			
		}

		public function initializeQueue($user,$game,$meet){
			
			$data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
			
			if (!is_array($user) || !is_array($game) || !is_array($meet)) {
				return false;
			} elseif (empty($data['login']) || empty($data['id'])) {
				return false;
			} else {
				//Sprawdzenie czy kolejka juz uruchomiona
				$alreadyLobby = $this->pdo->prepare("SELECT id,slots FROM queue WHERE `createdBy` LIKE :createdBy");
				$alreadyLobby->bindValue(':createdBy', $user["id"]);
				$alreadyLobby->execute();
				$alreadyBusyPlayer = $this->pdo->prepare("SELECT users.id, queue.slots, queue.id AS queueid FROM users INNER JOIN queue ON users.lobby = queue.id WHERE users.id = :userId");
                $alreadyBusyPlayer->bindValue(':userId', $user["id"]);
				$alreadyBusyPlayer->execute();
				
				if ( $alreadyLobby->rowCount() != 0 ){ 
					$myLobby = $alreadyLobby->fetch(PDO::FETCH_ASSOC);
					//echo "Gracze (". $myLobby['slots']."/". $game['max_slots'] .") <br>"; 
                    return array("id" => $myLobby['id'], "slots" => $myLobby['slots'], "max_slots" => $game['max_slots']);
				} elseif ( $alreadyBusyPlayer->rowCount() != 0 ) {
					$myLobby = $alreadyBusyPlayer->fetch(PDO::FETCH_ASSOC);
					//echo "Gracze (". $myLobby['slots']."/". $game['max_slots'] .") <br>"; 
                    return array("id" => $myLobby['queueid'] ,"slots" => $myLobby['slots'], "max_slots" => $game['max_slots']);
				} else {
						//Wyszukuje kolejki dla $game["name"]
						$findFreeLobby = $this->pdo->prepare("SELECT * FROM queue WHERE `team1` = :userTeam OR `team2` = :userTeam AND `game` = :gameId AND `slots` <= :gameMaxSlots AND `createdBy` LIKE :createdBy");
						$findFreeLobby->bindParam(':userTeam', $user["team"]);
						$findFreeLobby->bindParam(':gameId', $game["id"]);
						$findFreeLobby->bindParam(':gameMaxSlots', $game["max_slots"]);
						$findFreeLobby->bindValue(':createdBy', '%');
						$findFreeLobby->execute();
						
						if($findFreeLobby->rowCount() == 0){
							// Starting to create new lobby..
							$createLobby = $this->pdo->prepare("INSERT INTO queue (game,team1,team2,slots,createdBy) VALUES (:gameId,:meetTeam1,:meetTeam2,:reservedSlots,:createdBy)");
							$createLobby->bindParam(':gameId', $game["id"]);
							$createLobby->bindParam(':meetTeam1', $meet["team1"]);
                            $createLobby->bindParam(':meetTeam2', $meet["team2"]);
							$createLobby->bindValue(':reservedSlots', '1', PDO::PARAM_STR);
							$createLobby->bindParam(':createdBy', $user["id"]);
							$createLobby->execute();
								
							$findFreeLobby->bindParam(':createdBy', $user["id"]);
							while($findFreeLobby->rowCount() === 0){$findFreeLobby->execute();}
						} else {
							// Dodaje się do kolejki
							$freeLobby = $findFreeLobby->fetch(PDO::FETCH_ASSOC);
							$joinLobby = $this->pdo->prepare("UPDATE queue SET slots = slots + 1 WHERE id = :lobbyId");
							$joinLobby->bindParam(':lobbyId', $freeLobby['id']);
							$joinLobby->execute();
							$signLobbyToPlayer = $this->pdo->prepare("UPDATE users SET lobby = :lobbyId WHERE id = :userId");
							$signLobbyToPlayer->bindParam(':lobbyId', $freeLobby['id']);
							$signLobbyToPlayer->bindParam(':userId', $user['id']);
							$signLobbyToPlayer->execute();
						}
				}
			}
		}

        public function findQueueData($team){
            $getQData = $this->pdo->prepare('SELECT * FROM queue WHERE team1 = :teamId OR team2 = :teamId LIMIT 1');
            $getQData -> bindParam(':teamId',$team,PDO::PARAM_INT);
            $getQData -> execute();
            return $getQData -> fetch();
        }
        
	}

?>
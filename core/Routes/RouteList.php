<?php

    if(false){$Route = new Routes();} // for editor helpers

$Route->Add('testpdo','TestPdoController','display_pdo');
$Route->Add('test_game_stats','TestDependency','gameStats');
$Route->Add('test_csgo','AutomateCSGOController','try_connection');
$Route->Add('test_players','TestDependency','playersLobbyData');

$Route->Add('index', 'IndexController', 'index');
$Route->Add('tester', 'IndexController', 'tester');

$Route->Add('user_panel', 'UserController', 'userPanel');
$Route->Add('user', 'UserController', 'userProfile');
$Route->Add('user_edit', 'UserController', 'userEdit');

$Route->Add('login', 'AuthController', 'login');
$Route->Add('login_action', 'AuthController', 'loginAction');
$Route->Add('logout', 'AuthController', 'logout');
$Route->Add('register', 'AuthController', 'register');
$Route->Add('register_action', 'AuthController', 'registerAction');
$Route->Add('recovery_pass', 'AuthController', 'recoveryPass');

$Route->Add('start_queue', 'QueueController', 'startQueue');

$Route->Add('lobby', 'LobbyController', 'lobby');

$Route->Add('league', 'LeagueController', 'league');
$Route->Add('add_league', 'LeagueController', 'addLeague');

$Route->Add('game', 'GameController', 'game');
$Route->Add('add_game', 'GameController', 'addGame');
$Route->Add('delete_game', 'GameController', 'deleteGame');

$Route->Add('team', 'TeamController', 'team');
$Route->Add('teams', 'TeamController', 'teams');
$Route->Add('team_invite', 'TeamController', 'teamInvite');
$Route->Add('setTeamLeague', 'TeamController', 'setTeamLeague');
$Route->Add('add_team', 'TeamController', 'addTeam');
$Route->Add('leader', 'TeamController', 'leader');


$Route->Add('admin', 'AdminController', 'admin');

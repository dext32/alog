<?php

class Routes
{
    private $routes = [];

    public function Add ($path, $controller, $function)
    {
        array_push($this->routes, Array(
            'path' => $path,
            'controller' => $controller,
            'function' => $function
        ));
    }

    public function Get()
    {
        return $this->routes;
    }

    public function RunController($page, DI\Container $container)
    {
        // Set default controller
        $controller_class = 'IndexController';
        $controller_function = 'index';

        // Find proper controller and set
        foreach ($this->routes as $route)
        {
            if($route['path'] == $page)
            {
                $controller_class = $route['controller'];
                $controller_function = $route['function'];
            }
        }

        // Run controller
        $controller = $container->get($controller_class);
        $controller->$controller_function();
    }
}
<?php

class Functions{

    public static function dateCompare($to_date,$type=null,$time_left=null){
        $date1 = new DateTime("now");
        $date2 = new DateTime($to_date);
        $seconds = $date2->getTimestamp()-$date1->getTimestamp();        
        if($type == "text"){
            $D = intval(intval($seconds) / (3600*24));
            $H = (intval($seconds) / 3600) % 24;
            $m = (intval($seconds) / 60) % 60;
            $s = (intval($seconds)) % 60;
            $time_result = sprintf(($D>=1)?"%01d dni %02d:%02d:%02d":"%02d dni %02d godz. %02d min.", $D, $H, $m, $s);
        } elseif ($type == "minutes" && !is_null($time_left)) {
            $time_result = array( "ready_to_start" => ($seconds/60 < $time_left)?TRUE:FALSE , "minutes_left" => intval($seconds/60) );
        } else {
            $time_result = $seconds;
        }
        return $time_result;
    }
    
    
    public static function SendMail($subject,$message,$to){
        require 'PHPMailer/PHPMailerAutoload.php';

        require_once('PHPMailer/class.phpmailer.php');
        require_once('PHPMailer/class.smtp.php');
        $mail = new PHPMailer();
        $mail->From = core::$config['mail']['from'];
        $mail->FromName = core::$config['mail']['fromName'];
        $mail->Host = core::$config['mail']['host'];
        $mail->Mailer = core::$config['mail']['mailer'];
        $mail->SMTPAuth = core::$config['mail']['SMTPAuth'];
        $mail->Username = core::$config['mail']['username'];
        $mail->Password = core::$config['mail']['password'];
        $mail->Port = core::$config['mail']['port'];
        $mail->CharSet = 'UTF-8';
        $mail->Subject = $subject;
        $mail->Body = $message;
        $mail->SMTPAutoTLS = false;
        $mail->SMTPSecure = '';
        //$mail->AddAddress ($to,$name." ".$surname);
        $mail->AddAddress ($to,core::$config['mail']['from']);

        $mail->IsHTML(true);
        if($mail->Send()){                      
            echo '';
        } else {
            echo 'E-mail nie mógł zostać wysłany';
        }

    }
    
    public static function UploadFile($filename = null){
        $return = null;
        $target_dir = "uploads/";
        $target_file = is_null($filename)?$target_dir . basename($_FILES["fileToUpload"]["name"]):$target_dir . $filename.".".strtolower(pathinfo($_FILES["fileToUpload"]["name"],PATHINFO_EXTENSION));
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                $return = "Plik nie jest obrazem. ";
                $uploadOk = 0;
            }
        
        // Check if file already exists
        if (file_exists($target_file)) {
            $return .= "Plik o podanej nazwie już istnieje. ";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 2500000) {
            $return .= "Plik ma zbyt duży rozmiar. ";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" ) {
            $return .= "Nie dozwolony format pliku. ";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $return .= "Twój plik nie został przesłany. ";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $return .= "Plik ". basename( $_FILES["fileToUpload"]["name"]). " został przesłany. ";
            } else {
                $return .= "Sorry, there was an error uploading your file. ";
            }
        }
        return $return;
    }
    
    
    public static function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public static function Exception($message)
    {
        try {
            throw new Exception($message);
        }catch (Throwable $e)
        {
            Functions::showException($e);
            exit();
        }
    }


    public static function showException(Throwable $e)
    {
        if (Core::$config['system']['environment'] == 'production')
        {

            echo '<script type="text/javascript">document.body.innerHTML = \'\';</script>';

            echo '<div style="background-color: white; width:1160px; padding:100px; padding-bottom: 100px; margin-top:40px; margin-left: auto; margin-right: auto; text-align: center; font-size: 20px;">';
            echo 'We\'re sorry, something went wrong. <br />';
            echo 'Please contact with Administrator and give him this code ( '.$e->getCode().' )';
            echo '</div>';

            return;
        }

        echo '<script type="text/javascript">document.body.innerHTML = \'\';</script>';

        echo '<div style="background-color: white; width:1160px; padding-bottom: 300px; margin-top:40px; margin-left: auto; margin-right: auto;">';

        echo '<div style="width:1200px; margin-left: auto; margin-right: auto">';

        echo '<div style="width:1160px; padding:20px; background-color: #A11;">Exception</div>';
        echo '<div style="width:1000px; background-color: white; padding:30px;">';
        print_r($e->getMessage());
        echo '<br />';
        echo '<b>' . $e->getFile() . '</b> on line: <b>' . $e->getLine() . '</b>';
        echo '</div>';
        echo '<div style="width:1160px; padding:20px; margin-bottom:20px; background-color:#EEE;">';
        echo '<pre>';
        echo($e->getTraceAsString());
        echo '</pre> <br /><br />';

        $file= file($e->getFile());
        echo '<pre>';
        for ($i = $e->getLine()-10 - 1; $i <= $e->getLine() -1 -1 ; $i++)
        {
            if (isset($file[$i]))
                echo '   '.($i+1).'. '. $file[$i];
        }
        echo '-> '.($e->getLine()).'. '.$file[$e->getLine()-1];
        for ($i = $e->getLine(); $i <= $e->getLine() +10 -1 ; $i++)
        {
            if (isset($file[$i]))
                echo '   '.($i+1).'. '.$file[$i];
        }
        echo '</pre>';

        echo '</div>';

        echo '</div>';
        echo '</div>';

    }

    public static function dd($value)
    {
        echo '<script type="text/javascript">document.body.innerHTML = \'\';</script>';

        echo '<div style="padding:30px;"><pre>';
        print_r($value);
        echo '</pre></div>';

        die();
    }

}
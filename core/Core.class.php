<?php


class Core
{

    public static $config;

    /**
     * @var \DI\Container
     */
    public $DI;


    private $configData = [
        0 => 'config/config.ini',
        1 => 'text/global.lang.php'
    ];

    public function __construct()
    {
        if (file_exists($this->configData[0]) && (filesize($this->configData[0]) !== 0)) {
            $file = parse_ini_file($this->configData[0], true);
            self::$config = $file;
        } else {
            return false;
        }

        header('Content-Type: text/html; charset=' . self::$config['system']['charset']);

        error_reporting(self::$config['system']['errorReporting']); // Added another error catching, now it hide only warnings

        if (phpversion() < self::$config['system']['PHPVersion']) {
            die('Your server must be in' . self::$config['system']['PHPVersion'] . ' PHP version or higher.');
        }

        return null;
    }

    public function runApplication($pdo, $session)
    {
        if(isset($_GET['ajax'])) { require 'jscripts/ajax/main.php'; }
        elseif(isset($_POST['json'])) { require 'jscripts/json/main.php'; }
        else {

            $templatePath = 'template/';

            // Include header file
            require $templatePath.'header.php';


            // Create Route object and Add Route List
            $Route = new Routes();
            require 'core/Routes/RouteList.php';


            // Get page from address and put it into page variable
            $page = null;
            if (isset($_GET['page']))
                $page = $_GET['page'];

            // Run Controller
            $Route->RunController($page, $this->DI);

            // Include footer file
            require $templatePath.'footer.php';

        }
    }

    public function DBconnection()
    {
        $db = new PDO(self::$config['db']['driver'] . 'dbname=' . self::$config['db']['db_name'] . ';host=' . self::$config['db']['host'], self::$config['db']['user'], self::$config['db']['password']);

        if($db)
            return $db;
        else
            throw new Exception('Error during connection to database.');
    }

    public function buildDI(PDO $pdo, Session $session)
    {
        $DI = new \DI\Container();

        $DI->set('PDO', $pdo);
        $DI->set('Session', $session);

        $this->DI = $DI;
    }
}

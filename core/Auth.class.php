<?php
/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/

/**
 * Class auth
 */
class Auth
{
	/**
	 * @var null
	 */
	private $pdo = null;

	/**
	 * @var null
	 */
	private $session = null;

	/**
	 * @var array
	 */
	private $info = [];

	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * @param $pdo
	 * @param $session
	 */
	public function __construct($pdo, $session)
	{
		if (is_object($pdo)) {
			$this->pdo = $pdo;
		}

		if (is_object($session)) {
			$this->session = $session;
		}

		$langPath = 'text/auth.lang.php';

		if (file_exists($langPath)) {
			$lang = [];
			require_once $langPath;
			$this->info = $lang['auth'];
		}

		$this->userRepository = new UserRepository($pdo);
	}

	/**
	 * @param $data
	 * @return array|bool
	 */
	public function login($data)
	{
		if (!is_array($data)) {
			return false;
		} elseif (!array_key_exists('login', $data) || !array_key_exists('password', $data)) {
			return false;
		} else {
			$data = array_map('strip_tags', $data);

			if (empty($data['login']) || empty($data['password'])) {
				$return = $this->info[0];
			} else {
				$data['password'] = hash('sha512', $data['password']);

				$stmt = $this->pdo->prepare('SELECT count(id) as number, id, privilege FROM users WHERE login = :login AND password = :password');
				$stmt->bindParam(':login', $data['login'], PDO::PARAM_STR);
				$stmt->bindParam(':password', $data['password'], PDO::PARAM_STR);
				$stmt->execute();

				$result = $stmt->fetch();

				if ($result['number'] == 1) {
					$this->session->set(['login' => $data['login'], 'id' => $result['id'], 'privilege' => $result['privilege'], 'loggedin' => true], true);
					$return = [0 => $this->info[1], 1 => true];
				} else {
					$return = $this->info[2];
				}
			}
		}
		return $return;
	}

	/**
	 * @param $data
	 * @return array|bool
	 */
	public function register($data)
	{
		if (!is_array($data)) {
			return false;
		} elseif (!array_key_exists('login', $data) || !array_key_exists('password', $data) || !array_key_exists('email', $data)) {
			return false;
		} else {
			$data = array_map('strip_tags', $data);
			$data = array_map('trim', $data);

			$userLogin = $data['login'];
			$userEmail = $data['email'];
			if (empty($userLogin) || empty($data['password']) || empty($userEmail)) {
				$return = $this->info[0];
			} else if (!filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
				$return = $this->info[3];
			} else if ($this->userRepository->checkIfUserExists($userLogin)) {
				$return = $this->info[4];
			} else if ($this->userRepository->checkIfEmailExists($userEmail)) {
				$return = $this->info[5];
			} else if (empty($data['name'])) {
				$return = $this->info[6];
			} else if (empty($data['surname'])) {
				$return = $this->info[7];
			} else if (empty($data['university'])) {
				$return = $this->info[8];
			} else if (empty($data['adress1'])) {
				$return = $this->info[9];
			} else if (empty($data['adress2'])) {
				$return = $this->info[10];
			} else if (empty($data['city'])) {
				$return = $this->info[11];
			} else if (empty($data['postal'])) {
				$return = $this->info[12];
			} else {
				$data['password'] = hash('sha512', $data['password']);

				$additional = ['IP' => $_SERVER['REMOTE_ADDR'], 'user_agent' => $_SERVER['HTTP_USER_AGENT']];
				$additional = array_map('strip_tags', $additional);
				$additional = array_map('trim', $additional);

				$stmt = $this->pdo->prepare('INSERT INTO users (login,password,email,name,surname,university,adress,ip,user_agent,date) VALUES (:login, :password, :email, :name, :surname, :university, :adress,"' . $additional['IP'] . '", "' . $additional['user_agent'] . '", now())');
				$stmt->bindParam(':login', $userLogin, PDO::PARAM_STR);
				$stmt->bindParam(':password', $data['password'], PDO::PARAM_STR);
				$stmt->bindParam(':email', $userEmail, PDO::PARAM_STR);
				$stmt->bindParam(':name', $data['name'], PDO::PARAM_STR);
				$stmt->bindParam(':surname', $data['surname'], PDO::PARAM_STR);
				$stmt->bindValue(':university', $data['university']);
				$data['adress'] = $data['adress1'] . " " . $data['adress2'] . " " . $data['city'] . " " . $data['postal'];
				$stmt->bindValue(':adress', $data['adress'], PDO::PARAM_STR);
				$stmt->execute();

				if ($stmt->rowCount() == 1) {
					$return = [0 => $this->info[13], 1 => true];
				} else {
					$return = $this->info[14];
				}
			}
			return $return;
		}
	}


	public function update($userId, $data)
	{
		if (!is_array($data)) {
			return false;
		} elseif (!array_key_exists('login', $data) || !array_key_exists('password', $data) || !array_key_exists('email', $data)) {
			return false;
		} else {
			$user = new User($this->pdo);
			$userLocalData = $user->getUserAllData($userId, $data['login']);

			$data = array_map('strip_tags', $data);
			$data = array_map('trim', $data);

			$queryVars = " id = " . $userLocalData['id'];

			$email = $data['email'];
			if (empty($data['login']) || empty($data['password']) || empty($email)) {
				return $this->info[15]; //email, haslo i email są wymagane
			} elseif ($data['password'] != $data['password_confirmation']) {
				return $this->info[16]; //powtorz haslo poprawnie
			}

			$queryVars .= " ,password = :dataPassword ";
			if ($userLocalData['password'] != hash('sha512', $data['password'])) {
				$shaPw = hash('sha512', $data['password']);
			} else {
				$shaPw = $userLocalData['password'];
			}

			$queryVars .= " ,login = :dataLogin ";
			if ($userLocalData['login'] != $data['login']) {
				if ($this->userRepository->checkIfUserExists($data['login'])) {
					$return = $this->info[4];
				} else {
					$dataLogin = $data['login'];
				}
			} else {
				$dataLogin = $userLocalData['login'];
			}

			$queryVars .= " ,email = :dataEmail ";
			if ($userLocalData['email'] != $email) {
				if ($this->userRepository->checkIfEmailExists($email)) {
					$return = $this->info[5];
				} else {
					$dataEmail = $email;
				}
			} else {
				$dataEmail = $userLocalData['email'];
			}

			$queryVars .= " ,name = :dataName ";
			if ($userLocalData['name'] != $data['name']) {
				$dataName = $data['name'];
			} else {
				$dataName = $userLocalData['name'];
			}

			$queryVars .= " ,surname = :dataSurname ";
			if ($userLocalData['surname'] != $data['surname']) {
				$dataSurname = $data['surname'];
			} else {
				$dataSurname = $userLocalData['surname'];
			}

			$queryVars .= " ,adress = :dataAdress ";
			if ($userLocalData['adress'] != $data['adress']) {
				$dataAdress = $data['adress'];
			} else {
				$dataAdress = $userLocalData['adress'];
			}

			$valuesToUpdate = [
				'userId' => $userLocalData['id'],
				'dataPassword' => $shaPw,
				'dataLogin' => $dataLogin,
				'dataEmail' => $dataEmail,
				'dataName' => $dataName,
				'dataSurname' => $dataSurname,
				'dataAdress' => $dataAdress

			];

			$operationSuccess = $this->userRepository->updateUserData($queryVars, $valuesToUpdate);
			if ($operationSuccess) {
				$return = $this->info[17];
                header( "Refresh:2" );
			} //profil zaktualizowano pomyslnie
			return $return;
		}
	}

	public function recoveryPass($email)
	{
		$length = 8;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$sourcePass = $randomString;
		$randomString = hash('sha512', $randomString);
		$successOperation = $this->userRepository->generateRandomPasswordByUserEmail($randomString, $email);

		if ($successOperation) {
			Functions::SendMail("Resetowanie hasła do konta na ALoGames.", "<html><body><h1>Academic League of Games</h1><br><p>Twoje nowe hasło : " . $sourcePass . "</p><p>Pozdrawiamy,<br>ekipa ALoG<br></p><p>Wiadomość została wygenerowana automatycznie, prosimy nie odpowiadać na tego maila.</p></body></html>", $email);
			return $this->info[18];
		} else {
			return "Operation failed";
		}
	}


	/**
	 * @return null
	 */
	public function logout()
	{
		session_destroy();
		header('Location: index.php');
		return null;
	}
}

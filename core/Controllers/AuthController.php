<?php

class AuthController extends Controller
{

    public function login()
    {
        global $pdo;

        require 'template/login.php';
    }


    public function loginAction()
    {
        global $pdo;
        global $session;

        $login = new Auth($pdo, $session);
        $result = $login -> login(['login' => $_POST['login'], 'password' => $_POST['password']]);

        if(is_array($result) && $result[1] == true)
        {
            echo $result[0];
            header("Refresh:0;URL=index.php");
        } else {
            echo $result;
        }
    }

    public function logout()
    {
        global $pdo;
        global $session;

        $login = new Auth($pdo, $session);
        $login -> logout();
    }


    public function register()
    {
        global $pdo;
        global $session;

        $user = new User($pdo);
        $universityList = $user->getAvaliableUniversity();
        require 'template/register.php';
    }


    public function registerAction()
    {
        global $pdo;
        global $session;

        $register = new Auth($pdo, $session);
        $result = $register -> register(['login' => $_POST['login'], 'password' => $_POST['password'], 'email' => $_POST['email'], 'name' => $_POST['name'], 'surname' => $_POST['surname'], 'university' => $_POST['university'], 'adress1' => $_POST['adres1'], 'adress2' => $_POST['adres2'], 'city' => $_POST['city'], 'postal' => $_POST['postal']]);
        if (is_array($result) && $result[1] == true) {
            header('Location: index.php?page=register&done=true');
        } else {
            header('Location: index.php?page=register&done=false&alert='.$result);
        }
    }


    public function recoveryPass()
    {
        global $pdo;
        global $session;

        $recovery = new Auth($pdo, $session);
        $result = $recovery -> recoveryPass($_POST['email']);
        //$result = "Dostępne wkrótce.";
        echo "<div class='container'><div class=\"alert alert-light\" role=\"alert\">".$result."</div></div>";
        require "template/welcome.php";
    }

}
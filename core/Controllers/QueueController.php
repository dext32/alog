<?php


class QueueController extends Controller
{
    public function startQueue()
    {
        global $pdo;
        global $session;

        $user = new User($pdo);
        $userData = $user -> getUserQueueData();
        $game = new Game($pdo);                     // todo -- There is missing $session atribute, did it work before?
        $gameData = $game -> getGameData();
        $queue = new Queue($pdo);
        $queue -> initializeQueue($userData,['name' => $gameData['name'], 'max_slots' => $gameData['max_slots']]);
        //$queue -> initializeQueue($userData,$game);
    }
}
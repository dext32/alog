<?php


class LobbyController extends Controller
{
    public function lobby()
    {

        global $pdo;
        global $session;
        global $userData;
        global $team;
        global $user;
        global $games;


        if (isset ($_POST['info_upload'])) {
            if ($_FILES["fileToUpload"]["size"] > 0) {
                $copy_file_name_inc = 1;
                $scrsht_name = $_POST['lobby']."-".$userData['id'];
                //$scrsht_name = basename($_FILES["fileToUpload"]["name"]);
                $scrsht_ext = strtolower(pathinfo($_FILES["fileToUpload"]["name"],PATHINFO_EXTENSION));
                $scrsht_path = $scrsht_name.".".$scrsht_ext;
                while(file_exists("uploads/".$scrsht_path)){
                    $scrsht_name = $scrsht_name."(".$copy_file_name_inc++.")";
                    $scrsht_path = $scrsht_name.".".$scrsht_ext;
                }
                $result = Functions::UploadFile($scrsht_name);
            }
            if (isset($_POST['score1']) && isset($_POST['score2'])) {
                $info = $_POST['team1']." ".$_POST['score1']." : ".$_POST['score2']." ".$_POST['team2'];
                $result .= "<br>Dziękujemy za udostępnienie wyniku.";
                echo "<div class=\"alert alert-light\" role=\"alert\">".$result."</div>";
            }
        }
        require "template/lobby.php";

        if (isset($_POST['info_upload'])){
            if ($_FILES["fileToUpload"]["size"] > 0) {
                $lobby -> toDbLobbyReport($info, $scrsht_path);
            } else {
                $lobby -> toDbLobbyReport($info);
            }
        }
    }
}
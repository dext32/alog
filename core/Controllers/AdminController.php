<?php


class AdminController extends Controller
{

    public function admin()
    {
        global $pdo;
        global $session;

        global $games;
        global $user;
        global $team;
        global $league;

        global $userData;
        global $teamData;
        global $leagueData;


        $admin = new Admin($pdo, $session);

        if (isset ($_POST['update_lobby'])) {
            $lobby = new Lobby($pdo, $session);
            $lobby->setConnectionParameter($_POST['id'], array($_POST['ip'], $_POST['pw']));
        }

        if (isset ($_POST['add_new_meet'])) {
            $cpt1 = $admin->checkExistCaptain($_POST['team1']);
            $cpt2 = $admin->checkExistCaptain($_POST['team2']);
            if ($cpt1 && $cpt2) {
                $newMeet_status = $admin->setNeewMeet($_POST['team1'], $_POST['team2'], $_POST['league'], $_POST['date']);
                if ($newMeet_status) $_GET['new_meet_status'] = "true";
            } else {
                if (!$cpt1) $_GET['error'] = "cpt1_not_exist";
                if (!$cpt2) $_GET['error1'] = "cpt2_not_exist";
            }
        }

        if (isset($_GET['a'])) {
            if ($_GET['a'] == "delete_meet") {
                $deleteResult = $admin->deleteMeet($_POST['meet_id']);
                $_GET['delete_meet_result'] = $deleteResult;
            }
            if ($_GET['a'] == "lol_players_list") {
                return require 'template/admin_all_players.php';
            }
        }

        if (isset ($_POST['set_captain'])) {
            if ($admin->setTeamCaptain($_POST['captain'])) $_GET['set_captain_status'] = "true";
        }

        if (isset($_GET['board'])) {
            if (isset($_POST['match_end'])) {
                $match_end_result = $admin->setLeagueMeetEnd(array("lobby" => $_POST['lobby'], "team1" => $_POST['team1'], "team2" => $_POST['team2'], "score1" => $_POST['score1'], "score2" => $_POST['score2'], "date" => $_POST['date']));
                if ($match_end_result) {
                    $_GET['status'] = "match_end_true";
                } else {
                    $_GET['status'] = "match_end_false";
                }
            }
            require 'template/admin_board.php';
        } else {
            require 'template/admin.php';
        }

    }
}
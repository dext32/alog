<?php

class UserController extends Controller
{

    public function userPanel()
    {
        global $pdo;

        $user   = new User($pdo);
        $team   = new Team($pdo);
        $league = new League($pdo);
        $userData = $user -> getUserAllData();
        $teamData = $team -> getDataTeam($userData['team']);
        $league -> selectLeague($teamData['league']);

        $uniTeams = $team->getTeamsFromUniversity($userData['university']);

        require 'template/user_panel.php';
    }

    public function userProfile()
    {
        global $pdo;
        global $session;

        $team   = new Team($pdo);
        $league = new League($pdo);
        $games  = new Game($pdo, $session);

        if (!isset($user)) $user = new User($pdo);
        $userPubData    = $user->getUserPublicData($_GET['i']);
        $uniTeams       = $team->getTeamsFromUniversity($userPubData['university']);
        if (!is_null($userPubData['team'])) {
            $userTeamData = $team->getDataTeam($userPubData['team']);

            $team->id = $userTeamData['id'];
            $league->id = $userTeamData['league'];
            $userTeamStats = $league -> getLeagueScoresMini($team);
        }
        require 'template/public_player.php';
    }

    public function userEdit()
    {
        global $pdo;
        global $session;

        $user   = new User($pdo);
        $team   = new Team($pdo);
        $userData = $user -> getUserAllData();
        $teamData = $team -> getDataTeam($userData['team']);
        if (isset($_POST['user_update'])) {
            $auth = new Auth($pdo,$session);
            $updateResult = $auth -> update($userData['id'],['login' => $_POST['login'], 'password' => $_POST['password'], 'password_confirmation' => $_POST['password_confirmation'], 'email' => $_POST['email'], 'name' => $_POST['name'], 'surname' => $_POST['surname'], 'adress' => $_POST['adress']]);
            echo "<div class='container'><div class=\"alert alert-light\" role=\"alert\">".$updateResult."</div></div>";
        }
        // todo -- REMOVE this HTML from controller

        if (isset($_POST['avatar_upload'])) {
            $swapResult = $user->changeUserAvatar($userData['id']);
            if ($swapResult == true || $swapResult == null) {
                if ($_FILES["fileToUpload"]["size"] > 0) {
                    $avatar_name = "avatar-user-".$userData['id'];
                    $avatarResult = Functions::UploadFile($avatar_name);
                }
            }
        }
        if (isset($_POST['user_game_login_update'])){
            if (isset($_POST['hs-battletag']) ) {
                if ($user->getUserGameLogins($userData['id'],core::$config['game']['hs_id'], $_POST['hs-battletag'])) {
                    echo "<div class='container'><div class=\"alert alert-light\" role=\"alert\">Zaktualizowano battle.tag gry Hearthstone</div></div>";
                }
            } elseif ( isset($_POST['lol-login']) ) {
                if ($user->getUserGameLogins($userData['id'],core::$config['game']['lol_id'], $_POST['lol-login'])) {
                    echo "<div class='container'><div class=\"alert alert-light\" role=\"alert\">Zaktualizowano nazwę użytkownika gry League of Legends</div></div>";
                }
            }

        }
        require 'template/user_edit.php';
    }

}
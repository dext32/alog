<?php

class GameController extends Controller
{
    
    public function game()
    {
        global $pdo;
        global $session;

        global $team;
        global $games;
        global $league;
        global $teamData;
        global $userData;

        if(isset($_GET['id'])){
            $games -> setSessionGame($_GET['id']);
            $gameData = $games->getGameData();
            if ($teamData['game'] == $_GET['id']){
                $league -> selectLeague($teamData['league']);
            } else {
                $defaultLeague  = $league -> getDefaultLeague($_GET['id']);
                $league -> selectLeague($defaultLeague['id']);
            }
        } else {
            $defaultGame = $games->getDefaultGame();
            $gameId = (isset($teamData['game'])) ? $teamData['game'] : $defaultGame['id'];
            $defaultLeague  = $league -> getDefaultLeague($gameId);
            $league -> selectLeague($defaultLeague['id']);
            $gameData       = $games->getGameData($gameId);
            $_GET['id'] = $defaultGame['id'];
        }
        $leagueScores = $league->getLeagueScores();
        $leagueScoresMini = $league->getLeagueScoresMini();
        $leagueScoresMyTeam = $league->getLeagueScoresMini($team);

        require "template/games.php";
    }

    public function deleteGame()
    {
        global $pdo;
        global $session;

        $user = new User($pdo);
        if($user -> isUserAdmin()){
            $game = new Game($pdo,$session);
            $game -> deleteGame($_GET['id']);
        }
    }

    public function addGame()
    {
        global $pdo;
        global $session;

        $user = new User($pdo);
        if ($user->isUserAdmin()) {
            $game = new Game($pdo, $session);
            $game->addNewGame(['name' => $_GET['name'], 'max_slots' => $_GET['max_slots']]);
        }
}
}
<?php

class TestDependency extends Controller
{

    protected $autoGame;

    public function __construct(AutomateGameService $autoGame)
    {
        parent::__construct();

        /*
         * When you need AutomateGameService tell it in constructor
         * Don't care about PDO or something else, it would be done automatically
         */
        $this->autoGame = $autoGame;

    }

    public function playersLobbyData(){
        $lobbyPlayersData  = $this->autoGame->getPlayersDataFromLobby([1,8,12]);

        echo '<pre>';
        print_r($lobbyPlayersData);
        echo '</pre>';
    }

    public function gameStats()
    {
        /*
         * There you see only what you need: AutoGameService
         * You don't need PDO or Repository There.
         */

        $input_id = null;
        if (isset($_GET['id']))
            $input_id = $_GET['id'];
        else
            throw new Exception('Script needs GET[id]');


        $stats = $this->autoGame->getStatisticsOfTheMatch($input_id);

        // There we have stats for view
        echo '<pre>';
        print_r($stats);
        echo '</pre>';
    }

}
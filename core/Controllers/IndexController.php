<?php


class IndexController extends Controller
{
    protected $userRepository;

    public function __construct()
    {

    }

    public function index()
    {
        global $pdo;
        global $session;


        if($session -> exists('id')){
            //header("Refresh:0;URL=index.php?page=game");
            require "template/welcome.php";
        } else {
            require "template/welcome.php";
        }
    }


    public function tester()
    {
        $input = Input::GET(['val', 'val2', 'val3']);

        $input->validate([
            'val' => [ Input::REQUIRED, Input::MAX_LENGTH(5), Input::MIN_LENGTH(2), Input::EMAIL ],
            'val2' => [ Input::REQUIRED ]
        ]);

        //val 3 is optional and musn't be validated, but if you are using it be careful

        if ($input->validate_message)
        {
            throw new Exception('Validation failed : '. $input->validate_message);
        }

        Functions::dd( $input->val );

        // You can use Variables from _GET or _POST like:
        // $input->value

        // Is this better than:

        // if(isset (_GET['value'])){
        //    _GET['value']
        // }


    }
}
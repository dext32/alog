<?php

class LeagueController extends Controller
{
    public function league()
    {
        global $league;
        global $team;

        if( isset($_GET['ind']) ){
            $league->selectLeague($_GET['ind']);
            $leagueData   = $league -> getLeagueData();
            $leagueScores = $league -> getLeagueScores();
            $leagueChilds = $league -> getLeagueChilds($_GET['ind']);
            require "template/league.php";
        } else {
            header("Refresh:0;URL=index.php?page=game");
        }
    }

    public function addLeague()
    {
        global $pdo;
        global $session;

        $user = new User($pdo);
        if($user -> isUserAdmin()){
            $league = new League($pdo,$session);
            $league -> setNewLeague($_GET['name'],$_GET['gameId']);
        }
    }
}
<?php


class teamController extends Controller
{
    public function team()
    {
        global $pdo;
        global $session;

        $user   = new User($pdo);
        $userData = $user->getUserAllData();
        $games  = new Game($pdo, $session);
        $league = new League($pdo);
        if ($userData['team'] === NULL) {
            $team = new Team($pdo);
            $invList = $team->listInvites("0", $user);
            $uniTeams = $team->getTeamsFromUniversity($userData['university']);
            if (isset($_POST['applyToTeam'])) {
                $team->setRequestAddToTeam($userData, $_POST['team']);
            }
            if (isset($_POST['acceptInvite'])) {
                $team->setFlagAcceptPlToPlInvite($userData, $_POST['team']);
            }
            require 'template/no_team.php';
        } else {
            $team = new Team($pdo);
            $teamData = $team->getDataTeam($userData['team']);
            $teamMembers = $team->getTeamMembers($userData['team']);
            $invList = $team->listInvites($userData['team'], $user);
            require 'template/my_team.php';
        }
    }

    public function teams()
    {
        global $pdo;
        global $team;
        global $session;

        if (isset($_GET['i'])) {
            $league = new League($pdo);

            $publicTeamData = $team->getDataTeam($_GET['i']);
            if ($publicTeamData['league'] != null) {
                $league->selectLeague($publicTeamData['league']);
                $leagueData = $league->getLeagueData();

                $leagueDiagram = $league->resultDiagram($team);
                $leagueScores = $league->getLeagueScores();
                $leagueScoresMini = $league->getLeagueScoresMini();
                $leagueScoresMyTeam = $league->getLeagueScoresMini($team);
            }
            require 'template/public_team.php';
        }
    }

    public function setTeamLeague()
    {
        global $pdo;
        global $session;

        $user = new User($pdo);
        $userData = $user->getUserAllData();
        $team = new Team($pdo);
        $leaderTeams = $team->getCreatedTeams($userData);
        $league = new League($pdo);
        $games  = new Game($pdo, $session);
        $setTeamLeague = $league->addTeamToLeague($leaderTeams, $_GET['team'], $_GET['league']);
        require 'template/leader.php';
    }

    public function addTeam()
    {
        global $pdo;
        global $session;

        if (isset($_GET['tag']) && isset($_GET['name'])) {
            $user = new User($pdo);
            $userData = $user->getUserAllData();
            $team = new Team($pdo);
            $team->addNewTeam($userData, ['tag' => $_GET['tag'], 'name' => $_GET['name'], 'game' => $_GET['game']]);
            header("Refresh:0;URL=index.php?page=leader");
        }
    }

    public function leader()
    {
        global $pdo;
        global $session;
        global $games;

        $user = new User($pdo);
        $team = new Team($pdo);

        $league = new League($pdo);
        $userData = $user->getUserAllData();
        $leaderTeams = $team->getCreatedTeams($userData);
        $gameList = $games->getGameList();
        if ($user->isUserLeader()) {
            if (isset($_POST['addUserToTeam'])) {
                $result = $team->addToTeam($_POST['user'], $_POST['team'], $_POST['type']);
                echo "<div class=\"alert alert-light\" role=\"alert\">" . $result . "</div>";
            } elseif
            (isset($_POST['removeInvite'])) {
                $result = $team->removeInvite($_POST['user'], $_POST['team'], $_POST['type']);
                echo "<div class=\"alert alert-light\" role=\"alert\">" . $result . "</div>";
            } elseif
            (isset($_GET['a'])) {
                if ($_GET['a'] == "delete_member") {
                    $leader_kick_member_result = $team->kickFromTeam($_POST['team_id'], $_POST['member_id']);
                }
            }
            if (isset($_POST['avatar_upload'])) {
                $swapResult = $team->changeTeamAvatar($_POST['teamid']);
                if ($swapResult == true || $swapResult == null) {
                    if ($_FILES["fileToUpload"]["size"] > 0) {
                        $avatar_name = "avatar-team-" . $_POST['teamid'];
                        $avatarResult = Functions::UploadFile($avatar_name);
                    }
                }
            }
            require 'template/leader.php';
        }
    }

    public function teamInvite()
    {
        global $pdo;

        if(isset($_GET['inviteTo']))
        {
            $user = new User($pdo);
            $userData = $user -> getUserAllData();
            $team = new Team($pdo);
            $team->inviteToTeam($userData,$_GET['inviteTo']);
        }
    }
}
<?php

class AutomateCSGOController extends Controller {


    protected $csgo_service;

    /**
     * AutomateCSGOController constructor.
     */
    public function __construct(AutomateGameCSGO $csgo_service)
    {
        $this->csgo_service = $csgo_service;
    }

    public function try_connection(){

        $this->csgo_service->setConnectionFromLobbyId(4);
        $this->csgo_service->setTeamsFromLobbyData(['8' => 'WIT', '12' => 'PJATK']);

        $action = $this->csgo_service->establishConnection();
        //$action = $this->csgo_service->generateConfig("csgo_1");

        echo '<pre>';
        print_r($action);
        echo '</pre>';

    }

}
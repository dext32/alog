<?php

class Collection {

    private $collections = [
        'dataCollection' => [],
        'objectCollection' => []
    ];

    public function Add ($collection, $type, $function = null, $parameters = null)
    {
        if ($type == 'dataCollection') {
            foreach ($this->collections['objectCollection'] as $collectionObject) {
                if ( $collection == get_class($collectionObject) ) {
                    if ( isset($parameters) ) {
                        $collectionData = $collectionObject->$function( implode(',',$parameters) );
                    } else {
                        $collectionData = $collectionObject->$function();
                    }
                }
            }
            $this->collections['dataCollection'][$function] = $collectionData;
        } elseif ($type == 'objectCollection') {
            $this->collections['objectCollection'][get_class($collection)] = $collection;
        }
    }

    public function Get()
    {
        return $this->collections;
    }
}
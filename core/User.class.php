<?php

class User
{
    /**
     * @var
     */
    private $pdo;

    /**
     * @var UserRepository
     */
    private $UserRepository;

    /**
     * User constructor.
     * @param $pdo
     */
    public function __construct(PDO $pdo)
    {
        if (is_object($pdo)) {
            $this->pdo = $pdo;
        }

        $this->UserRepository = new UserRepository($pdo);
    }

    /**
     * @return bool
     */
    public function getUserData()
    {
        $data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
        $data = array_map('strip_tags', $data);

        if (empty($data['login']) || empty($data['id'])) {
            return false;
        } else {
            $stmt = $this->pdo->prepare('SELECT ip, user_agent, date FROM users WHERE id = :id AND login = :login');
            $stmt->bindParam(':login', $data['login'], PDO::PARAM_STR);
            $stmt->bindParam(':id', $data['id'], PDO::PARAM_STR);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
	
	public function getUserQueueData()
    {
        $data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
        $data = array_map('strip_tags', $data);

        if (empty($data['login']) || empty($data['id'])) {
            return false;
        } else {
            $stmt = $this->pdo->prepare('SELECT id, level, login, team, lobby FROM users WHERE id = :id AND login = :login');
            $stmt->bindParam(':login', $data['login'], PDO::PARAM_STR);
            $stmt->bindParam(':id', $data['id'], PDO::PARAM_STR);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
	
    public function getUserPublicData($userId){
        $getUserData = $this->pdo->prepare('SELECT id,name,surname,level,login,team,university FROM users WHERE id = :userId');
        $getUserData -> bindParam(':userId',$userId,PDO::PARAM_INT);
        $getUserData -> execute();
        return $getUserData->fetch();
    }
    
	public function getUserAllData($userId = null, $userLogin = null)
    {
        if (isset($userId)) {
            $data = ['login' => $userLogin, 'id' => $userId];
        } else if( isset($_SESSION['login']) && isset($_SESSION['id']) ){
            $data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
        } else{
            return false;
        }
        $data = array_map('strip_tags', $data);

        if (empty($data['login']) || empty($data['id'])) {
            return false;
        } else {
            $stmt = $this->pdo->prepare('SELECT * FROM users WHERE id = :id AND login = :login');
            $stmt->bindParam(':login', $data['login'], PDO::PARAM_STR);
            $stmt->bindParam(':id', $data['id'], PDO::PARAM_STR);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
	
	public function getUserMinData()
    {
        $data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
        $data = array_map('strip_tags', $data);

        if (empty($data['login']) || empty($data['id'])) {
            return false;
        } else {
            $stmt = $this->pdo->prepare('SELECT id,login,team,university FROM users WHERE id = :id AND login = :login');
            $stmt->bindParam(':login', $data['login'], PDO::PARAM_STR);
            $stmt->bindParam(':id', $data['id'], PDO::PARAM_STR);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
    
    public function getUserName($userId){
        if (!is_numeric($userId)) {return false;}
        else {
            $queryUserName = $this->pdo->prepare('SELECT login FROM users WHERE id = :userId');
            $queryUserName -> bindParam(':userId', $userId);
            $queryUserName -> execute();
            $stringName = $queryUserName->fetch(PDO::FETCH_ASSOC);
            return $stringName['login'];
        }
    }
    
    public function getUniversityName($userId){
        $getUni = $this->pdo->prepare('SELECT `university`.`name` AS name FROM `university` INNER JOIN `users` ON `university`.`id` = `users`.`university` WHERE `users`.`id` = :userId');
        $getUni -> bindParam(':userId', $userId, PDO::PARAM_INT);
        $getUni -> execute();
        $uniName = $getUni->fetch(PDO::FETCH_ASSOC);
        return $uniName['name'];
    }
    
    public function getUserAvatar($userId){
        $fileExt = array("jpg","jpeg","png");
        $returnAvatar = null;
        foreach ($fileExt as $ext) {
            $avatarPath = "uploads/avatar-user-".$userId.".".$ext;
            if (file_exists($avatarPath)) $returnAvatar = $avatarPath;
        }
        if(!is_null($returnAvatar)) {
            return $returnAvatar;
        } else {
            return "images/avatar-default.png";
        }
    }
    
    public function changeUserAvatar($userId){
        $currentAvatar = $this->getUserAvatar($userId);
        if (!is_null($currentAvatar) && $currentAvatar != "images/avatar-default.png") {
            return unlink($currentAvatar);
        } else {
            return null;
        }
    }
    
    public function getUserLink($userId){
        return "index.php?page=user&i=".$userId;
    }
    
    public function getUserGameLogins($userId,$gameId, $gameLogin = null){
        if (!is_null($gameLogin)){
            $checkUserGame = $this->pdo->prepare('SELECT login FROM usersgames WHERE user = :userId AND game = :gameId');
                $checkUserGame -> bindParam(':userId', $userId, PDO::PARAM_INT);
                $checkUserGame -> bindParam(':gameId', $gameId, PDO::PARAM_INT);
            $checkUserGame -> execute();
            $arrCheckUserGame = $checkUserGame -> fetch();
            if (is_array($arrCheckUserGame) && count($arrCheckUserGame) > 0 ) {
                $setUserGame = $this->pdo->prepare('UPDATE `usersgames` SET `login`= :gameLogin WHERE `user` = :userId AND `game` = :gameId');
                    $setUserGame -> bindParam(':userId', $userId, PDO::PARAM_INT);
                    $setUserGame -> bindParam(':gameId', $gameId, PDO::PARAM_INT);
                    $setUserGame -> bindParam(':gameLogin', $gameLogin, PDO::PARAM_STR);
                return $setUserGame -> execute();
            } else {
                $setUserGame = $this->pdo->prepare('INSERT INTO usersgames (`user`,`game`,`login`) VALUES (:userId,:gameId,:gameLogin)');
                    $setUserGame -> bindParam(':userId', $userId, PDO::PARAM_INT);
                    $setUserGame -> bindParam(':gameId', $gameId, PDO::PARAM_INT);
                    $setUserGame -> bindParam(':gameLogin', $gameLogin, PDO::PARAM_STR);
                return $setUserGame -> execute();
            }
        } else {
            $getUserGame = $this->pdo->prepare('SELECT * FROM usersgames WHERE user = :userId AND game = :gameId');
            $getUserGame -> bindParam(':userId', $userId, PDO::PARAM_INT);
            $getUserGame -> bindParam(':gameId', $gameId, PDO::PARAM_INT);
            $getUserGame -> execute();
            return $getUserGame -> fetch(PDO::FETCH_ASSOC);
        }
    }

    /**
     * @param array $inputTeams
     * @return array
     */
    public function getPlayersDataByTeams(array $inputTeams){
        $game_id_from_lobby_data = 6;
        $players_id_list = [];

        $players_from_teams =  $this->UserRepository->getPlayersDataByTeams($inputTeams);
        foreach ($players_from_teams as $playerKey){
            array_push($players_id_list,$playerKey['id']);
        }

        $players_with_games = $this->UserRepository->getPlayersGamesIds($players_id_list, $game_id_from_lobby_data);
        $players_rest_data  = $this->UserRepository->getPlayersUserData($players_id_list);

        foreach ($players_from_teams as $playerTeamKey => $playerTeamVal){

            $players_from_teams[$playerTeamKey]['game_login'] = null;
            foreach ($players_with_games as $playerGameKey) {

                if ($players_from_teams[$playerTeamKey]['id'] == $playerGameKey['user']) {
                    $players_from_teams[$playerTeamKey]['game_login'] = $playerGameKey['login'];
                }

            }


            foreach ($players_rest_data as $playerDataKey => $playerDataVal) {

                if ($players_from_teams[$playerTeamKey]['id'] == $players_rest_data[$playerDataKey]['user_id']) {
                    foreach ($playerDataVal as $playerDataArrayKey => $playerDataArrayVal){
                        $players_from_teams[$playerTeamKey][$playerDataArrayKey] = $playerDataArrayVal;
                    }
                }

            }

        }

        return $players_from_teams;
    }
    
    public function getUsersList($hint){
        if ( !empty($hint) ){
            $hint = $hint."%";
            $queryUsersList = $this->pdo->prepare("SELECT * FROM `users` WHERE `login` LIKE :hint");
            $queryUsersList -> bindParam(":hint", $hint, PDO::PARAM_STR);
            $queryUsersList -> execute();
            return $queryUsersList -> fetchAll(PDO::FETCH_ASSOC);
        }
    }
	
	public function setOnlineActivity(){
		$data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
        $data = array_map('strip_tags', $data);

        if (empty($data['login']) || empty($data['id'])) {
            return false;
        } else {
            $stmt = $this->pdo->prepare('UPDATE users SET last_activity = now() WHERE id = :userId');
            $stmt->bindParam(':userId', $data['id'], PDO::PARAM_STR);
            $stmt->execute();
        }
	}
	
	public function isUserAdmin(){
		return ($_SESSION['privilege']=='0')?true:false;
	}
	public function isUserLeader(){
		return ($_SESSION['privilege']=='2')?true:false;
	}
	
	public function getUserUniversityName(){
		$data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
		$data = array_map('strip_tags', $data);
		if (empty($data['login']) || empty($data['id'])) {
            return false;
        } else {
			$userUniversity = $this->pdo->prepare('SELECT university FROM users WHERE id = :userId');
			$userUniversity->bindParam(':userId', $data['id'], PDO::PARAM_INT);
			$userUniversity->execute();
			$universityId = $userUniversity->fetch(PDO::FETCH_ASSOC);
			$getUniversityName = $this->pdo->prepare('SELECT name FROM university WHERE id = :uniId');
			$getUniversityName->bindParam(':uniId', $universityId['university'], PDO::PARAM_INT);
			$getUniversityName->execute();
			$getUniversityNameArray = $getUniversityName->fetch(PDO::FETCH_ASSOC);
			return $getUniversityNameArray['name'];
		}
	}
    
	public function getAvaliableUniversity(){
		$getUniversityList = $this->pdo->prepare('SELECT id,name FROM university');
		$getUniversityList->execute();
		return $getUniversityList->fetchAll(PDO::FETCH_ASSOC);
	}
	
}



class Role
{
    private $pdo;
    public $user_roles = array();
    public $permissions;

    public function __construct($pdo) {
        $this->permissions = array();
        
        if (is_object($pdo)) {
            $this->pdo = $pdo;
        }
    }

    // return a role object with associated permissions
    public function getRolePerms($role_id) {
        $sql = "SELECT t2.perm_desc FROM role_perm as t1
                JOIN permissions as t2 ON t1.perm_id = t2.perm_id
                WHERE t1.role_id = :role_id";
        $sth = $this->pdo->prepare($sql);
        $sth -> bindParam(":role_id", $role_id, PDO::PARAM_INT);
        $sth -> execute();

        while($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->permissions[$row["perm_desc"]] = true;
        }
        return $this;
    }

    // check if a permission is set
    public function hasPerm($permission) {
        return isset($this->permissions[$permission]);
    }
    
    
    // insert a new role
    public function insertRole($role_name) {
        $sql = "INSERT INTO roles (role_name) VALUES (:role_name);";
        //$.sql = "SELECT LAST_INSERT_ID();";
        $sth = $this->pdo->prepare($sql);
        return $sth->execute(array(":role_name" => $role_name));
    }
    
    // insert a new permission
    public function insertPermission($perm_desc) {
        $sql = "INSERT INTO permissions (perm_desc) VALUES (:perm_desc);";
        //$.sql = "SELECT LAST_INSERT_ID();";
        $sth = $this->pdo->prepare($sql);
        return $sth->execute(array(":perm_desc" => $perm_desc));
    }

    // insert array of roles for specified user id
    public function insertUserRoles($user_id, $roles) {
        $sql = "INSERT INTO user_role (user_id, role_id) VALUES (:user_id, :role_id)";
        $sth = $this->pdo->prepare($sql);
        $sth->bindParam(":user_id", $user_id, PDO::PARAM_STR);
        $sth->bindParam(":role_id", $role_id, PDO::PARAM_INT);
        foreach ($roles as $role_id) {
            $sth->execute();
        }
        return true;
    }
    
    // list user roles 
    public function listUserRoles($user_id){
        $sql = "SELECT t1.role_id, t2.role_name FROM user_role as t1
                JOIN roles as t2 ON t1.role_id = t2.role_id
                WHERE t1.user_id = :user_id";
        $sth = $this->pdo->prepare($sql);
        $sth->execute(array(":user_id" => $user_id));

        while($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            array_push($this->user_roles, $row['role_name']);
        }
        return $this->user_roles;
    }

    // delete array of roles, and all associations
    public function deleteRoles($roles) {
        $sql = "DELETE t1, t2, t3 FROM roles as t1
                JOIN user_role as t2 on t1.role_id = t2.role_id
                JOIN role_perm as t3 on t1.role_id = t3.role_id
                WHERE t1.role_id = :role_id";
        $sth = $this->pdo->prepare($sql);
        $sth->bindParam(":role_id", $role_id, PDO::PARAM_INT);
        foreach ($roles as $role_id) {
            $sth->execute();
        }
        return true;
    }

    // delete ALL roles for specified user id
    public function deleteUserRoles($user_id) {
        $sql = "DELETE FROM user_role WHERE user_id = :user_id";
        $sth = $this->pdo->prepare($sql);
        return $sth->execute(array(":user_id" => $user_id));
    }
    
}



class PrivilegedUser extends User
{
    public $roles;
    private $privUserPDO;

    public function __construct($pdo) {
        parent::__construct($pdo);
        $this->privUserPDO = $pdo;
    }

    // override User method
    public function getByUsername($username) {
        $sql = "SELECT * FROM users WHERE login = :username";
        $sth = $this->privUserPDO->prepare($sql);
        $sth->execute(array(":username" => $username));
        $result = $sth->fetchAll();

        if (!empty($result)) {
            $privUser = new PrivilegedUser($this->privUserPDO);
            $privUser->user_id = $result[0]["id"];
            $privUser->username = $username;
            $privUser->password = $result[0]["password"];
            $privUser->email_addr = $result[0]["email"];
            $privUser->initRoles();
            return $privUser;
        } else {
            return false;
        }
    }

    // populate roles with their associated permissions
    public function initRoles() {
        $role = new Role($this->privUserPDO);
        $this->roles = array();
        $sql = "SELECT t1.role_id, t2.role_name FROM user_role as t1
                JOIN roles as t2 ON t1.role_id = t2.role_id
                WHERE t1.user_id = :user_id";
        $sth = $this->privUserPDO->prepare($sql);
        $sth->execute(array(":user_id" => $this->user_id));

        while($row = $sth->fetch(PDO::FETCH_ASSOC)) {
            $this->roles[$row["role_name"]] = $role->getRolePerms($row["role_id"]);
        }
    }

    // check if user has a specific privilege
    public function hasPrivilege($perm) {
        foreach ($this->roles as $role) {
            if ($role->hasPerm($perm)) {
                return true;
            }
        }
        return false;
    }
    
    // check if a user has a specific role
    public function hasRole($role_name) {
        return isset($this->roles[$role_name]);
    }
    
    

    // insert a new role permission association
    public function insertPerm($role_id, $perm_id) {
        $sql = "INSERT INTO role_perm (role_id, perm_id) VALUES (:role_id, :perm_id)";
        $sth = $this->privUserPDO->prepare($sql);
        return $sth->execute(array(":role_id" => $role_id, ":perm_id" => $perm_id));
    }

    // delete ALL role permissions
    public function deletePerms() {
        $sql = "TRUNCATE role_perm";
        $sth = $this->privUserPDO->prepare($sql);
        return $sth->execute();
    }
}
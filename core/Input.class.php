<?php


class Input extends Validatable
{
    private const METHOD_GET = 'method_get';
    private const METHOD_POST = 'method_post';

    private $data = Array();

    public function __construct($array, $method)
    {
        if($method == self::METHOD_POST)
            $this->_post($array);
        if($method == self::METHOD_GET)
            $this->_get($array);

        $this->setValidatableData($this->data);
    }

    public function __get($name)
    {
        if (isset($this->data[$name]))
            return $this->data[$name];
        else
            throw new Exception('Undefined variable "'.$name.'" in object of Input::class');
    }

    public static function GET($variables)
    {
        return new Input($variables, Input::METHOD_GET);
    }
    public static function POST($variables)
    {
        return new Input($variables, Input::METHOD_POST);
    }

    private function _get($array)
    {
        foreach ($array as $element)
        {
            if (isset($_GET[$element]))
                $this->data[$element] = $_GET[$element];
            else
                $this->data[$element] = NULL;
        }
    }
    private function _post($array)
    {
        foreach ($array as $element)
        {
            if (isset($_POST[$element]))
                $this->data[$element] = $_POST[$element];
            else
                $this->data[$element] = NULL;
        }
    }
}
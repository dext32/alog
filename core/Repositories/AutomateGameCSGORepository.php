<?php


/**
 * class AutomateGameCSGORepository
 */
class AutomateGameCSGORepository{

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * AutomateGameCSGORepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return array
     */
    public function getServersData(){
        $csgo_prefix = "csgo%";
        $serverList = $this->pdo->prepare('SELECT * FROM gameservers WHERE name LIKE :csgoName');
        $serverList -> bindParam(':csgoName',$csgo_prefix,PDO::PARAM_STR);
        $serverList -> execute();
        return $serverList->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $serverId
     */
    public function setServerRun($serverId){
        $runServer = $this->pdo->prepare("UPDATE `gameservers` SET `run` = true WHERE id = :serverId");
        $runServer -> bindParam(':serverId',$serverId,PDO::PARAM_INT);
        $runServer -> execute();
    }

    /**
     * @param $serverId
     */
    public function setServerAvaliable($serverId){
        $setServer = $this->pdo->prepare("UPDATE `gameservers` SET `lobby_id` = null, `run` = false, `in_use` = false, `start_time` = null WHERE id = :serverId");
        $setServer -> bindParam(':serverId',$serverId,PDO::PARAM_INT);
        $setServer -> execute();
    }

    /**
     * @param $serverId
     */
    public function setServerUnavialable($serverId, $lobbyId){
        $serverAction = $this->pdo->prepare("UPDATE `gameservers` SET `lobby_id` = :lobbyId, `in_use` = :setUse, `start_time` = now() WHERE id = :serverId");
        $serverAction->bindParam(':lobbyId', $lobbyId, PDO::PARAM_INT);
        $serverAction->bindValue(':setUse', TRUE, PDO::PARAM_BOOL);
        $serverAction->bindParam(':serverId', $serverId, PDO::PARAM_INT);
        $serverAction->execute();
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Paweł
 * Date: 17.10.2018
 * Time: 22:18
 */

/**
 * Class UserRepository
 */
class UserRepository
{
	/**
	 * @var PDO
	 */
	private $pdo;

	/**
	 * UserRepository constructor.
	 * @param PDO $pdo
	 */
	public function __construct(PDO $pdo)
	{
		$this->pdo = $pdo;
	}


	/**
	 * @param string $email
	 * @return bool
	 */
	public function checkIfEmailExists($email)
	{
		$stmt = $this->pdo->prepare('SELECT count(id) as number FROM users WHERE email = :email LIMIT 1');
		$stmt->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->fetch()['number'] == 1 ? true : false;
	}

	/**
	 * @param string $login
	 * @return bool
	 */
	public function checkIfUserExists($login)
	{
		$stmt = $this->pdo->prepare('SELECT count(id) as number FROM users WHERE login = :login LIMIT 1');
		$stmt->bindParam(':login', $login, PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->fetch()['number'] == 1 ? true : false;
	}

	/**
	 * @param string $randomPassword
	 * @param string $byEmail
	 * @return bool
	 */
	public function generateRandomPasswordByUserEmail($randomPassword, $byEmail)
	{
		$queryRecovery = $this->pdo->prepare('UPDATE users SET password = :genPassword WHERE email = :userEmail');
		$queryRecovery->bindParam(':genPassword', $randomPassword, PDO::PARAM_STR);
		$queryRecovery->bindParam(':userEmail', $byEmail, PDO::PARAM_STR);
		return $queryRecovery->execute();
	}

	/**
	 * @param string $queryVars
	 * @param array $boundParams
	 * @return bool
	 */
	public function updateUserData($queryVars, array $boundParams)
	{
		$updateQuery = $this->pdo->prepare('UPDATE users SET' . $queryVars . 'WHERE id = :userId');
		$updateQuery->bindParam(':userId', $boundParams['userId'], PDO::PARAM_INT);
		$updateQuery->bindParam(':dataPassword', $boundParams['dataPassword'], PDO::PARAM_STR);
		$updateQuery->bindParam(':dataLogin', $boundParams['dataLogin'], PDO::PARAM_STR);
		$updateQuery->bindParam(':dataEmail', $boundParams['dataEmail'], PDO::PARAM_STR);
		$updateQuery->bindParam(':dataName', $boundParams['dataName'], PDO::PARAM_STR);
		$updateQuery->bindParam(':dataSurname', $boundParams['dataSurname'], PDO::PARAM_STR);
		$updateQuery->bindParam(':dataAdress', $boundParams['dataAdress'], PDO::PARAM_STR);
		return $updateQuery->execute();
	}

    /**
     * @param array $inputTeams
     * @return array
     */
	public function getPlayersDataByTeams(array $inputTeams)
    {
        $returnedPlayers = [];

        foreach ($inputTeams as $inputTeam){

            $getList = $this->pdo->prepare('SELECT id, team FROM users WHERE team = :userTeam');
            $getList -> bindParam(':userTeam',$inputTeam, PDO::PARAM_INT);
            $getList -> execute();
            $usersList =  $getList -> fetchAll(PDO::FETCH_ASSOC);

            foreach ($usersList as $userKey){
                array_push($returnedPlayers,$userKey);
            }

        }

        return $returnedPlayers;
    }

    /**
     * @param array $inputPlayers
     * @param $gameId
     * @return array
     */
    public function getPlayersGamesIds(array $inputPlayers, $gameId){
        $returnedGames = [];

        foreach ($inputPlayers as $inputPlayer){

            $getList = $this->pdo->prepare('SELECT user, login FROM usersgames WHERE user = :userId AND game = :gameId');
            $getList -> bindParam(':userId',$inputPlayer, PDO::PARAM_INT);
            $getList -> bindParam(':gameId',$gameId, PDO::PARAM_INT);
            $getList -> execute();
            $gamesList =  $getList -> fetchAll(PDO::FETCH_ASSOC);

            foreach ($gamesList as $gamesKey){
                array_push($returnedGames,$gamesKey);
            }

        }

        return $returnedGames;
    }

    /**
     * @param array $inputPlayers
     * @return array
     */
    public function getPlayersUserData(array $inputPlayers){
        $returnedData = [];

        foreach ($inputPlayers as $inputPlayer){

            $getList = $this->pdo->prepare('SELECT id as user_id, login, email, name, surname, university FROM users WHERE id = :userId');
            $getList -> bindParam(':userId',$inputPlayer, PDO::PARAM_INT);
            $getList -> execute();
            $usersList =  $getList -> fetchAll(PDO::FETCH_ASSOC);

            foreach ($usersList as $usersKey){
                array_push($returnedData,$usersKey);
            }

        }

        return $returnedData;
    }
}
<?php 

class Team{
	public $id = null;
	/**
     * @var null
     */
    private $pdo = null;

    /**
     * @var array
     */
    private $info = [];

    /**
     * @param $pdo
     * @param $session
     */
    public function __construct($pdo)
    {
        if (is_object($pdo)) {
            $this->pdo = $pdo;
        }

        $langPath = 'text/team.lang.php';

        if (file_exists($langPath)) {
            $lang = [];
            require_once $langPath; // TODO -- including arrays like this doesn't work
            @$this->info = $lang['team'];
        }
    }

	public function addNewTeam($user,$newTeam){
		if (!is_array($newTeam) && !is_array($user)) {
            return false;
        } elseif (!array_key_exists('tag', $newTeam) || !array_key_exists('name', $newTeam) || !array_key_exists('id', $user)) {
			return false;
        } else {			
            $newTeam = array_map('strip_tags', $newTeam);

            if (empty($newTeam['tag']) || empty($newTeam['name'])) {
                $return = $this->info[1];
            } else {			
				$teamExist = $this->pdo->prepare('SELECT id FROM teams WHERE name = :newTeamName AND tag = :newTeamTag AND game = :newTeamGame');
				$teamExist->bindParam(':newTeamName', $newTeam['name'], PDO::PARAM_STR);
				$teamExist->bindParam(':newTeamTag', $newTeam['tag'], PDO::PARAM_STR);
				$teamExist->bindParam(':newTeamGame', $newTeam['game'], PDO::PARAM_INT);
				$teamExist->execute();

				if ($teamExist->rowCount() == 0) {
					$setNewTeam = $this->pdo->prepare('INSERT INTO teams (`tag`,`name`,`leader`,`university`,`game`) VALUES (:newTeamTag,:newTeamName,:newTeamLeader, :newTeamUniversity, :newTeamGame)');
					$setNewTeam->bindParam(':newTeamTag', $newTeam['tag'], PDO::PARAM_STR);
					$setNewTeam->bindParam(':newTeamName', $newTeam['name'], PDO::PARAM_STR);
					$setNewTeam->bindParam(':newTeamLeader', $user['id'], PDO::PARAM_INT);
					$setNewTeam->bindParam(':newTeamUniversity', $user['university'], PDO::PARAM_INT);
					$setNewTeam->bindParam(':newTeamGame', $newTeam['game'], PDO::PARAM_INT);
					$setNewTeam->execute();
					$return = $this->info[2];
				} else {
					echo "Taka druzyna istnieje";
					$return = $this->info[3];
				}
            }
        }
        echo $return;
	}
	
	
	//Only Admin
	public function deleteTeam($team){
		if (!is_array($team)) {
            return false;
        } elseif (!array_key_exists('id', $team)) {
            return false;
        } else {
			$teamToRemove = $this->pdo->prepare('SELECT id FROM teams WHERE id = :idTeam');
			$teamToRemove->bindParam(':idTeam', $team['id']);
			$teamToRemove->execute();
			if ($teamToRemove->rowCount() == 0) { return $this->info[5]; } else	{
				$teamToRemove = $this->pdo->prepare('DELETE FROM teams WHERE id = :idTeam');
				return $this->info[6];
			}
		}
	}

	public function getTeamMembers($teamId){
		if(empty($teamId)){ 
			return false; 
		} elseif (!is_numeric($teamId)){ 
			return false; 
        } else {
			$getTeamMembers = $this->pdo->prepare('SELECT id,login,name,surname,email,privilege FROM users WHERE team = :idTeam');
			$getTeamMembers->bindParam(':idTeam', $teamId);
			$getTeamMembers->execute();
			return $getTeamMembers->fetchAll(PDO::FETCH_ASSOC);
		}
	}
	
	public function getOnlineMembers($teamId){
		if(empty($teamId)){ 
			return false; 
		} elseif (!is_numeric($teamId)){ 
			return false; 
        } else {
			$getOnlineMembers = $this->pdo->prepare('SELECT id,last_activity FROM users WHERE team = :idTeam');
			$getOnlineMembers->bindParam(':idTeam', $teamId);
			$getOnlineMembers->execute();
			$getOnlineMembersArray = $getOnlineMembers->fetchAll(PDO::FETCH_ASSOC);
			foreach( $getOnlineMembersArray as $row => $value) {
				$date1 = new DateTime("now");
				$date2 = new DateTime($getOnlineMembersArray[$row]['last_activity']);
				$tempCheck = $date1->getTimestamp() - $date2->getTimestamp();
				($tempCheck < 10 ? $boolActivity = 'T' : $boolActivity = 'F');
				$getOnlineMembersArray[$row]['online'] = $boolActivity;
			}
			return $getOnlineMembersArray;
		}
	}
	
	public function getDataTeam($teamId){
		if(empty($teamId)){ 
			return false; 
		} elseif (!is_numeric($teamId)){ 
			return false; 
        } else {
			$teamToReview = $this->pdo->prepare('SELECT * FROM teams WHERE id = :idTeam');
			$teamToReview->bindParam(':idTeam', $teamId);
			$teamToReview->execute();
			if ($teamToReview->rowCount() == 0) { 
				return $this->info[7]; 
			} else { 
				$teamReviewData = $teamToReview->fetch(PDO::FETCH_ASSOC);
				$this->id = $teamReviewData['id'];
				return $teamReviewData;
			}
		}
	}
    
    public function getTeamLink($teamId){
        return "index.php?page=teams&i=".$teamId;
    }
    
    public function getTeamName($teamId){
        if (!is_numeric($teamId)) {return false;}
        else {
            $queryTeamName = $this->pdo->prepare('SELECT name FROM teams WHERE id = :teamId');
            $queryTeamName -> bindParam(':teamId', $teamId);
            $queryTeamName -> execute();
            $stringName = $queryTeamName->fetch(PDO::FETCH_ASSOC);
            return $stringName['name'];
        }
    }
	
    public function getTeamAvatar($teamId){
        $fileExt = array("jpg","jpeg","png");
        $returnAvatar = null;
        foreach ($fileExt as $ext) {
            $avatarPath = "uploads/avatar-team-".$teamId.".".$ext;
            if (file_exists($avatarPath)) $returnAvatar = $avatarPath;
        }
        if(!is_null($returnAvatar)) {
            return $returnAvatar;
        } else {
            return "images/avatar-default.png";
        }
    }
    
    public function getUniversityCoord($teamId){
        $getLeader = $this->pdo->prepare('SELECT users.id,users.login,users.name,users.surname FROM users INNER JOIN teams ON users.id = teams.leader WHERE teams.id = :teamId');
        $getLeader -> bindParam(':teamId',$teamId,PDO::PARAM_INT);
        $getLeader -> execute();
        return $getLeader -> fetch();
    }
    
    public function changeTeamAvatar($teamId){
        $currentAvatar = $this->getTeamAvatar($teamId);
        if (!is_null($currentAvatar) && $currentAvatar != "images/avatar-default.png") {
            return unlink($currentAvatar);
        } else {
            return null;
        }
    }
    
	private function getTeamUniversity($teamId){
		if(empty($teamId)){ 
			return false; 
		} elseif (!is_numeric($teamId)){ 
			return false; 
        } else {
			$teamToReview = $this->pdo->prepare('SELECT university FROM teams WHERE id = :idTeam');
			$teamToReview->bindParam(':idTeam', $teamId);
			$teamToReview->execute();
			if ($teamToReview->rowCount() == 0) 
				{ return $this->info[7]; } 
			else 
				{ return $teamToReview->fetch(PDO::FETCH_ASSOC);
			}
		}
	}
    
    public function getUniversityName($teamId){
        $getUni = $this->pdo->prepare('SELECT `university`.`name` AS name FROM `university` INNER JOIN `teams` ON `university`.`id` = `teams`.`university` WHERE `teams`.`id` = :idTeam');
        $getUni -> bindParam(':idTeam', $teamId);
        $getUni -> execute();
        $uniName = $getUni->fetch(PDO::FETCH_ASSOC);
        return $uniName['name'];
    }
	
    public function getTeamsFromUniversity($uniId){
        if (!is_numeric($uniId)) {return false;}
        else {
            $queryTeamList = $this->pdo->prepare('SELECT * FROM `teams` WHERE university = :uniId');
            $queryTeamList -> bindParam(':uniId', $uniId, PDO::PARAM_INT);
            $queryTeamList -> execute();
            return $queryTeamList -> fetchAll(PDO::FETCH_ASSOC);
        }
    }
    
	public function noTeamWarning($userData){
		if(empty($userData)){ 
			return false; 
		} else {
			$return = $this->info[8];
		}
		echo $return;
	}
	
	public function inviteToTeam($userData,$inviteTo){
		$data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id'], 'inv' => $inviteTo];
		if (empty($data['login']) || empty($data['id']) || empty($data['inv'])) { return false; }
				else {
					$checkInviteExist = $this->pdo->prepare('SELECT * FROM teaminvites WHERE `to` = :toId AND `teamid` = :teamId');
					$checkInviteExist->bindParam(':toId', $data['inv']);
					$checkInviteExist->bindParam(':teamId', $userData['team']);
					$checkInviteExist->execute();
					if ($checkInviteExist->rowCount() == 0) {
                        $checkUniversityValidate = $this->pdo->prepare('SELECT `university` FROM users WHERE id = :toId');
                        $checkUniversityValidate -> bindParam(':toId', $data['inv'], PDO::PARAM_INT);
                        $checkUniversityValidate -> execute();
                        $invToUniversity = $checkUniversityValidate -> fetch(PDO::FETCH_ASSOC);
                        if ( $invToUniversity['university'] == $userData['university']  ) {
                            $iniInvite = $this->pdo->prepare('INSERT INTO teaminvites (`from`,`to`,`teamid`) VALUES (:fromId,:toId,:teamId)');
                            $iniInvite->bindParam(':fromId', $data['id'], PDO::PARAM_STR);
                            $iniInvite->bindParam(':toId', $data['inv'], PDO::PARAM_STR);
                            $iniInvite->bindParam(':teamId', $userData['team'], PDO::PARAM_INT);
                            $iniInvite->execute();
                            $return = $this->info[9];
                        } else {
                            $return = $this->info[10]; //nie można zapraszać spoza uczelni
                        }
                        
					} else {
						$return = $this->info[11];
					}
				}
		echo $return;
	}
	
	public function setRequestAddToTeam($userData,$toTeam){
		if(!array_key_exists('id', $userData) || !array_key_exists('team', $userData) || !array_key_exists('university', $userData) || !is_numeric($toTeam)) {return false;}
		else {
			$teamUniversityData = $this->getTeamUniversity($toTeam);
			if($teamUniversityData['university'] !== $userData['university']){ 
				$return = $this->info[12];
			} elseif ($userData['team'] !== NULL) {
				$return = $this->info[13];
			} else {
				$checkInviteExist = $this->pdo->prepare('SELECT `from`,`teamid` FROM teaminvites WHERE `from` = :fromId AND `teamid` = :toTeamId');
				$checkInviteExist->bindParam(':fromId', $userData['id']);
				$checkInviteExist->bindParam(':toTeamId', $toTeam);
				$checkInviteExist->execute();
				if ($checkInviteExist->rowCount() == 0) {
					$iniInvite = $this->pdo->prepare('INSERT INTO teaminvites (`from`,`teamid`,`type`) VALUES (:fromId,:teamId,:invType)');
					$iniInvite->bindParam(':fromId', $userData['id'], PDO::PARAM_INT);
					$iniInvite->bindParam(':teamId', $toTeam, PDO::PARAM_INT);
					$iniInvite->bindValue(':invType', 2, PDO::PARAM_INT);
					$iniInvite->execute();
					$return = $this->info[14];
				}
				else {
					$return = $this->info[15];
				}
			}
			return $return;
		}
	}
	
	public function setFlagAcceptPlToPlInvite($userData,$teamId){
		if(!array_key_exists('id', $userData) || !is_numeric($teamId)) { return false; }
		else {
			 $checkInviteAvaliable = $this->pdo->prepare('SELECT * FROM teaminvites WHERE `to` = :toId AND `teamid` = :teamId');
			 $checkInviteAvaliable->bindParam(':toId', $userData['id']);
			 $checkInviteAvaliable->bindParam(':teamId', $teamId);
			 $checkInviteAvaliable->execute();
			 if($checkInviteAvaliable->rowCount() !== 0){
				$acceptInvite = $this->pdo->prepare('UPDATE teaminvites SET flag = :flagUp WHERE `to` = :toId AND `teamid` = :teamId');
				$acceptInvite->bindValue(':flagUp', 1);
				$acceptInvite->bindParam(':toId', $userData['id']);
				$acceptInvite->bindParam(':teamId', $teamId);
				$acceptInvite->execute();
				$return = $this->info[16];
			 } else {
				$return = $this->info[17];
			 }
		}
	}
	
	public function setFlagDeclinePlToPlInvite($userData){
		//total erase teaminvite record
	}
	
	public function addToTeam($invId,$teamId,$inviteType){
		$leaderTeams = $this->getCreatedTeams(['id' => $_SESSION['id']]);
        
        $queryCheckInvite = $this->pdo->prepare('SELECT COUNT(*) FROM teaminvites WHERE `to` = :invId OR `from` = :invId AND `teamid` = :teamId');
        $queryCheckInvite -> bindParam(':invId',$invId);
        $queryCheckInvite -> bindParam(':teamId',$teamId);
        $queryCheckInvite -> execute();
        $checkInvite = $queryCheckInvite -> fetch(PDO::FETCH_ASSOC);
        
        $queryCheckUserTeam = $this->pdo->prepare('SELECT `team` FROM users WHERE `id` = :userId');
        $queryCheckUserTeam -> bindParam(':userId',$invId);
        $queryCheckUserTeam -> execute();
        $checkUserTeam = $queryCheckUserTeam -> fetch(PDO::FETCH_ASSOC);
        
        foreach($leaderTeams as $teamKeys){
            if($teamKeys['id'] == $teamId) {
                if( !empty($checkInvite) ) { 
                    if( is_null($checkUserTeam['team']) ) {
                        $queryAddToTeam = $this->pdo->prepare('UPDATE users SET `team` = :teamId WHERE `id` = :userId');
                        $queryAddToTeam -> bindParam(':teamId',$teamId);
                        $queryAddToTeam -> bindParam(':userId',$invId);
                        $queryAddToTeam -> execute();
                        if ($queryAddToTeam) { 
                            if($inviteType == '1') { $this->deleteInvite($invId,$teamId,$inviteType); }
                            elseif ($inviteType == '2') { $this->deleteInvite($invId,$teamId,$inviteType); }
                            $return = $this->info[18]; //udało się dodać użytkownika do teamu
                        }
                    } else { $return = $this->info[19]; } //nie można dodać użytkownika ponieważ jest już w drużynie
                } else { $return = $this->info[20]; } //takie zaproszenie nie istnieje
            }
        }
        return $return;
	}
    
    public function kickFromTeam($teamId, $userId){
        $ownedTeams = $this->getCreatedTeams(array("id" => $_SESSION['id']));
        if (!empty($ownedTeams)) {
            foreach ($ownedTeams as $ownedTeam) {
                if ($ownedTeam['id'] == $teamId) {
                    $membersArray = $this->getTeamMembers($ownedTeam['id']);
                    if (!empty($membersArray)) {
                        foreach ($membersArray as $teamMember){
                            if ($teamMember['id'] == $userId) {
                                $kick_member = $this->pdo->prepare('UPDATE `users` SET team = null WHERE id = :userId');
                                $kick_member -> bindParam(':userId',$teamMember['id'],PDO::PARAM_INT);
                                return $kick_member -> execute();
                            }
                        }
                    }
                }
            }
        } else { return false; }
    }
    
    public function removeInvite($invId,$teamId,$inviteType){
        $leaderTeams = $this->getCreatedTeams(['id' => $_SESSION['id']]);
        
        $queryCheckInvite = $this->pdo->prepare('SELECT COUNT(*) FROM teaminvites WHERE `to` = :invId OR `from` = :invId AND `teamid` = :teamId');
        $queryCheckInvite -> bindParam(':invId',$invId);
        $queryCheckInvite -> bindParam(':teamId',$teamId);
        $queryCheckInvite -> execute();
        $checkInvite = $queryCheckInvite -> fetch(PDO::FETCH_ASSOC);
        
        foreach($leaderTeams as $teamKeys){
            if($teamKeys['id'] == $teamId) {
                if( !empty($checkInvite) ) { 
                    if($inviteType == '1') { $this->deleteInvite($invId,$teamId,$inviteType); }
                    elseif ($inviteType == '2') { $this->deleteInvite($invId,$teamId,$inviteType); }
                    $return = $this->info[21]; //udało się dodać użytkownika do teamu
                } else { $return = $this->info[22]; } //takie zaproszenie nie istnieje
            }
        }
        return $return;
    }
	
    private function deleteInvite($invId,$teamId,$invType){
        $query = 'DELETE FROM `teaminvites` WHERE';
        if ($invType=='1') { $query .= ' `to` = :invId'; }
        if ($invType=='2') { $query .= ' `from` = :invId'; }
        $query .= ' AND `teamid` = :teamId AND `type` = :invType';
        $queryDeleteInv = $this->pdo->prepare($query);
        $queryDeleteInv -> bindParam(':invId',$invId);
        $queryDeleteInv -> bindParam(':teamId',$teamId);
        $queryDeleteInv -> bindParam(':invType',$invType);
        $queryDeleteInv -> execute();
    }
    
	public function listInvites($teamId,User $user){
		if( !is_object($user)) {return false;}
			else {
				if($user->isUserLeader()){
					$listInvitesPltoPl = $this->pdo->prepare('SELECT `from`,`to`,`teamid`,`type` FROM teaminvites WHERE `type` = :invType AND `flag` = :isInvAccepted AND `teamid` = :teamId');
					$listInvitesPltoPl->bindValue(':invType',1);
					$listInvitesPltoPl->bindValue(':isInvAccepted',1);
					$listInvitesPltoPl->bindParam(':teamId',$teamId);
					$listInvitesPltoPl->execute();
					$arrayListInvPltoPl=$listInvitesPltoPl->fetchAll(PDO::FETCH_ASSOC);
					$listRequestedInvites = $this->pdo->prepare('SELECT `from`,`teamid`,`type` FROM teaminvites WHERE `type` = :invType AND `teamid` = :teamId');
					$listRequestedInvites->bindValue(':invType',2);
					$listRequestedInvites->bindParam(':teamId',$teamId);
					$listRequestedInvites->execute();
					$arrayListReqInv = $listRequestedInvites->fetchAll(PDO::FETCH_ASSOC);
					return array_merge($arrayListReqInv,$arrayListInvPltoPl);
				} else {
					$userId = $user->getUserMinData();
					$listPlayerInvites = $this->pdo->prepare('SELECT `from`,`teamid` FROM teaminvites WHERE `to` = :userId');
					$listPlayerInvites->bindParam(':userId',$userId['id']);
					$listPlayerInvites->execute();
					return $listPlayerInvites->fetchAll(PDO::FETCH_ASSOC);
				}
			}
	}
	
	public function getCreatedTeams($userData){
		if(empty($userData)){ 
			return false; 
		} else {
			$listTeams = $this->pdo->prepare('SELECT * FROM `teams` WHERE leader = :teamLeader');
			$listTeams->bindParam(':teamLeader', $userData['id'], PDO::PARAM_INT);
			$listTeams->execute();
			return $listTeams->fetchAll(PDO::FETCH_ASSOC);
		}
	}
	
	public function getAvaliableTeamLeagues($teamData){
		if(!is_array($teamData)) { return false; }
		elseif(!array_key_exists('game', $teamData)) { return false; }
		else {
			$listLeagues = $this->pdo->prepare('SELECT id,name FROM `league` WHERE game = :teamGame');
			$listLeagues->bindParam(':teamGame', $teamData['game'], PDO::PARAM_INT);
			$listLeagues->execute();
			return $listLeagues->fetchAll(PDO::FETCH_ASSOC);
		}
	}
}
?>
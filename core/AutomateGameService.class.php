<?php


class AutomateGameService
{
    protected $automateGameRepository;
    protected $ObjUser;

    public function __construct(AutomateGameRepository $automateGameRepository, User $ObjUser)
    {
        $this->automateGameRepository = $automateGameRepository;
        $this->ObjUser = $ObjUser;
    }

    public function getPlayersDataFromLobby(array $teamList){
        return $this->ObjUser->getPlayersDataByTeams($teamList);
    }

    public function getStatisticsOfTheMatch($match_id)
    {
        // There you cannot use PDO, because you don't see it!

        $score = $this->automateGameRepository->getGameStatisticByGameId($match_id);
        $names = $this->automateGameRepository->getTeamNames($match_id);

        return [
            $names['team_A'] => $score['team_A'],
            $names['team_B'] => $score['team_B']
        ];
    }
}
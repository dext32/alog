<?php

    if(false){$Collection = new Collection();} // for editor helpers

// Object collections only
$Collection->Add(new User($pdo),'objectCollection');
$Collection->Add(new Team($pdo),'objectCollection');


//Non-parameter functions from collected objects
$Collection->Add("User", 'dataCollection', 'getUserAllData');


//Parameterized functions based on non-parameter functions
$collect = $Collection->Get();

$Collection->Add("Team", 'dataCollection', 'getDataTeam', [ $collect['dataCollection']['getUserAllData']['team'] ] );


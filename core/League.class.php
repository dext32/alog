<?php

class League{

	/**
	 * @var null
	 */
	private $pdo = null;
	public $id = null;
	
	public function __construct($pdo){
		
		if (is_object($pdo)) {
			$this->pdo = $pdo;
		}
		
		$langPath = 'text/league.lang.php';

		if (file_exists($langPath)) {
			$lang = [];
			require_once $langPath;
			$this->info = $lang['league'];
		}	
		
	}

	public function selectLeague($id){
		if(!is_numeric($id)) { return false; } 
		else {
			$this->id = $id;
		}
	}
	
	public function getLeagueData(){
		if(isset($this->id)){
			$query = 'SELECT `league`.*, `games`.`name` AS gamename FROM `league` ';
			$query .= 'INNER JOIN `games` ON `games`.`id` = `league`.`game` ';
			$query .= 'WHERE `league`.`id` = :leagueId';
			$queryGetData = $this->pdo->prepare($query);
			$queryGetData->bindParam(':leagueId', $this->id, PDO::PARAM_INT);
			$queryGetData->execute();
			return $queryGetData->fetch(PDO::FETCH_ASSOC);
		} else {
			return false;
		}
	}
	
    public function getLeagueLink(){
        return "index.php?page=league&ind=".$this->id;
    }
    
	public function setNewLeague($name,$gameId){
		if(empty($name) || empty($gameId)){ $return = $this->info[1]; }
		elseif (!is_numeric($gameId)) { $return = $this->info[2]; }
		else {
			$gameId = strip_tags($gameId);
			$name = strip_tags($name);
			$addNewLeague = $this->pdo->prepare('INSERT INTO league (`name`,`game`) VALUES (:name,:gameId)');
            $addNewLeague->bindParam(':name', $name, PDO::PARAM_STR);
			$addNewLeague->bindParam(':gameId', $gameId, PDO::PARAM_INT);
            $addNewLeague->execute();
			$return = $this->info[3];
		}
		return $return;
	}
	
	public function getLeagueName($league){
		if(empty($league)){ return false; }
		else {
			$getLeagueName = $this->pdo->prepare('SELECT name FROM league WHERE id = :leagueId');
			$getLeagueName->bindParam(':leagueId', $league, PDO::PARAM_INT);
			$getLeagueName->execute();
			$getLeagueNameArray = $getLeagueName->fetch(PDO::FETCH_ASSOC);
			return $getLeagueNameArray['name'];
		}
	}
	
    public function getLeagueList($gameId = null){
        if ( is_null($gameId) ) {
            $queryLeagueList = $this->pdo->prepare("SELECT * FROM `league` WHERE type NOT LIKE '%Sub'");
        } elseif ( is_numeric($gameId) ) {
            $queryLeagueList = $this->pdo->prepare("SELECT * FROM `league` WHERE game = :gameId AND type NOT LIKE '%Sub'");
            $queryLeagueList -> bindParam(':gameId', $gameId, PDO::PARAM_INT);
        }
        $queryLeagueList -> execute();
        return $queryLeagueList->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getLeagueChilds($parentLeague, $specificName = null){
        if (isset($parentLeague)){
            $querySpecifyLeagues = "SELECT * FROM `league` WHERE parent = :parentLeague";
                if(isset($specificName)){$querySpecifyLeagues .= " AND name LIKE :leagueName";}
            $queryGetLeagues = $this->pdo->prepare($querySpecifyLeagues);
            $queryGetLeagues -> bindParam(':parentLeague', $parentLeague, PDO::PARAM_INT);
            if(isset($specificName)){$queryGetLeagues -> bindValue(':leagueName', '%'.$specificName.'%');}
            $queryGetLeagues -> execute();
            return $queryGetLeagues -> fetchAll(PDO::FETCH_ASSOC);
        } else {
            return null;
        }
    }
    
    public function getLeagueTeamsNumber($leagueId){
        if (!is_numeric($leagueId)) {return false;}
        else {
            $queryLeagueTeams = $this->pdo->prepare('SELECT COUNT(*) AS teams FROM `teams` WHERE league = :leagueId');
            $queryLeagueTeams -> bindParam(':leagueId', $leagueId, PDO::PARAM_INT);
            $queryLeagueTeams -> execute();
            $teamsNumber = $queryLeagueTeams -> fetch(PDO::FETCH_ASSOC);
            return $teamsNumber['teams'];
        }
    }
    
	private function checkLeaderPrivilege($leaderTeams,$inputTeam){
		foreach($leaderTeams as $leaderTeamArray){
			if($leaderTeamArray['id'] == $inputTeam){
				return TRUE;
			} else {
				$return = FALSE;
			}
		}
		return $return;
	}
	
    public function getDefaultLeague($gameId){
        $queryDefaultLeague = $this->pdo->prepare('SELECT * FROM league WHERE `game` = :gameId LIMIT 1');
        $queryDefaultLeague->bindParam(':gameId', $gameId, PDO::PARAM_INT);
        $queryDefaultLeague->execute();
        return $queryDefaultLeague->fetch();
    }
    
	public function addTeamToLeague($teams,$setTeam,$setLeague){
		if($this->checkLeaderPrivilege($teams,$setTeam)){
			$setTeamLeague = $this->pdo->prepare('UPDATE teams SET `league` = :leagueId WHERE `id` = :teamId');
			$setTeamLeague->bindParam(':teamId', $setTeam, PDO::PARAM_INT);
			$setTeamLeague->bindParam(':leagueId', $setLeague, PDO::PARAM_INT);
			$setTeamLeague->execute();
			return $this->info[4];
		} else {
			return false;
		}
	}
	
	public function getTeamScores($teamId){
		if(is_numeric($teamId)){
			$queryTeamScores = $this->pdo->prepare('SELECT * FROM leaguescores WHERE `team1` = :teamId OR `team2` = :teamId ORDER BY date DESC');
			$queryTeamScores->bindParam(':teamId', $teamId, PDO::PARAM_INT);
			$queryTeamScores->execute();
			return $queryTeamScores->fetchAll(PDO::FETCH_ASSOC);
		} else {
			return false;
		}
	}
	
	private static function sortScores($a, $b)
	{
		if ($a['points'] == $b['points']) {
			return 0;
		}
		return ($a['points'] < $b['points']) ? 1 : -1;
	}

	public function getLeagueScores(){
		if(isset($this->id)){
			$i			 = 0;
            $j           = 1;
			$win_points  = 3;
			$draw_points = 1;
			$lose_points = 0;
			$queryAllLeagueTeams = $this->pdo->prepare('SELECT id,tag FROM teams WHERE league = :leagueId');
			$queryAllLeagueTeams->bindParam(':leagueId', $this->id, PDO::PARAM_INT);
			$queryAllLeagueTeams->execute();
			$allLeagueTeams = $queryAllLeagueTeams->fetchAll(PDO::FETCH_ASSOC);
			foreach($allLeagueTeams as $team){
				$wins	= 0;
				$draws	= 0;
				$loses	= 0;
				$points = 0;
				$TeamResults = $this->getTeamScores($team['id']);
				if(!empty($TeamResults)){
					foreach($TeamResults as $TeamResult){
                        if ($TeamResult['team1'] == $team['id']){
							if($TeamResult['score1']>$TeamResult['score2']){$points+=$win_points; $wins+=1;}
							elseif($TeamResult['score1']==$TeamResult['score2']){$points+=$draw_points; $draws+=1;}
							elseif($TeamResult['score1']<$TeamResult['score2']){$points+=$lose_points; $loses+=1;}
						} elseif ($TeamResult['team2'] == $team['id']) {
							if($TeamResult['score2']>$TeamResult['score1']){$points+=$win_points; $wins+=1;}
							elseif($TeamResult['score2']==$TeamResult['score1']){$points+=$draw_points; $draws+=1;}
							elseif($TeamResult['score2']<$TeamResult['score1']){$points+=$lose_points; $loses+=1;}
						}
					}
				}
				$tableScores[$i++] = array("id" => $team['id'], "tag" => $team['tag'], "place" => '0', "win" => $wins, "draw" => $draws, "lose" => $loses, "points" => $points);
			}
			usort($tableScores, array('League','sortScores'));
            foreach($tableScores as $teamTableScore => $teamScoresValue){
                $tableScores[$teamTableScore]["place"] = $j++;
            }
			return $tableScores;
		} else {
			return false;
		}
	}
	
	public function getLeagueScoresMini($team = null, $top = 3){
		//$top 				= 3;
		$arrayScoresMini 	= array();
		$copyScoresTable = $this->getLeagueScores();

		if (count($copyScoresTable) < $top)
		    $top = count($copyScoresTable);

		for($i=0; $i < $top; $i++){
			$arrayScoresMini[$i] = $copyScoresTable[$i];
		}
        
		if(!is_object($team) && !isset($team->id)) { return $arrayScoresMini; }
		else {
			foreach($copyScoresTable as $teamScores => $teamData){
				if($teamData['id'] == $team->id){ $arrayScoresMini=$teamData; }
			}
			return $arrayScoresMini;
		}
		
	}
    
    public function setLeagueNewMeet($team1,$team2,$league,$date){
        if(!is_numeric($team1) || !is_numeric($team2)) {return false;}
        else {
            $queryInsertNewMeet = $this->pdo->prepare("INSERT INTO `leaguemeets` (`team1`,`team2`,`league`,`date`) VALUES (:team1,:team2,:league,:date)");
            $queryInsertNewMeet->bindParam(':team1', $team1, PDO::PARAM_INT);
            $queryInsertNewMeet->bindParam(':team2', $team2, PDO::PARAM_INT);
            $queryInsertNewMeet->bindParam(':league', $league, PDO::PARAM_INT);
            $queryInsertNewMeet->bindParam(':date', $date, PDO::PARAM_STR);
            $queryInsertNewMeet->execute();
            }
    }
    
    private static function sortMeets($a, $b)
	{
		if ($a['time_to_sort'] == $b['time_to_sort']) {
			return 0;
		}
		return ($a['time_to_sort'] > $b['time_to_sort']) ? 1 : -1;
	}
    
    public function getLeagueMeets($league=null,$team = null){
            $queryString = "SELECT * FROM `leaguemeets`";
            if(!is_null($league)) { $queryString .= " WHERE league = :league"; }
            if(!is_null($team))   { $queryString .= " AND team1 = :team OR team2 = :team"; }
            $queryGetLeagueMeets = $this->pdo->prepare($queryString);
            if(!is_null($league)) { $queryGetLeagueMeets->bindParam(':league', $league, PDO::PARAM_INT); }
            if(!is_null($team))   { $queryGetLeagueMeets->bindParam(':team', $team, PDO::PARAM_INT); }
            $queryGetLeagueMeets->execute();
            $leagueMeets = $queryGetLeagueMeets->fetchAll(PDO::FETCH_ASSOC);
            foreach($leagueMeets as $meetKeys => $meetValues){
                $leagueMeets[$meetKeys]["time_to_sort"] = Functions::dateCompare($meetValues['date']);
                $leagueMeets[$meetKeys]["time_left"] = Functions::dateCompare($meetValues['date'],"text");
            }
            usort($leagueMeets, array('League','sortMeets'));
            return $leagueMeets;
    }
	
    public function getLeagueCloseMeet($teamId, $manualStart = null){
        is_null($manualStart) ? $startOffset = 15 : $startOffset = $manualStart;
        
        if (is_null($teamId)) { return false; }
        $arrayMeets = $this->getLeagueMeets($this->id,$teamId);
        foreach ($arrayMeets as $meetKeys){
            if ($meetKeys['time_to_sort'] > $startOffset*(-60)) {
                if ($meetKeys['time_to_sort'] <= $startOffset*60) {
                    return $meetKeys;
                }
                return array("time_left" => $meetKeys['time_left']);
            }
        }
    }
    
    public function resultDiagram($obTeam, $teamId = null){
        if (!is_null($teamId)) $obTeam->id = $teamId;
        $resultArray = $this -> getLeagueScoresMini($obTeam);
        
        $full_percent = array_sum( array($resultArray['win'],$resultArray['draw'],$resultArray['lose']) );
        $percent_win    = ((($resultArray['win'] / $full_percent) * 100) * 360) / 100;
        $percent_draw   = ((($resultArray['draw'] / $full_percent) * 100) * 360) / 100;
        $percent_lose   = ((($resultArray['lose'] / $full_percent) * 100) * 360) / 100;
        
        return array("data_start1" => 0, "data_value1" => $percent_win, "data_start2" => 0+$percent_win, "data_value2" => $percent_draw, "data_start3" => 0+$percent_win+$percent_draw, "data_value3" => $percent_lose, "data_value_rotate1" => $percent_win+1, "data_value_rotate2" => $percent_draw+1, "data_value_rotate3" => $percent_lose+1);
    }
    
}

?>
<?php

class Admin{
    
    public $passwords = array("pw1","pw2","pw3");
    
	/**
	 * @var null
	 */
	private $pdo = null;
		
	/**
     * @var null
     */
    private $session = null;
	
	public function __construct($pdo,$session){
		
		if (is_object($pdo)) {
			$this->pdo = $pdo;
		}

		if (is_object($session)) {
            $this->session = $session;
        }
		
		$langPath = 'text/admin.lang.php';

		if (file_exists($langPath)) {
			$lang = [];
			require_once $langPath;
			$this->info = $lang['admin'];
		}	
		
	}
    
    public function getAllTeams(){
        $getTeams = $this->pdo->prepare('SELECT * FROM `teams`');
        $getTeams -> execute();
        return $getTeams -> fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getAllLeagues(){
        $getTeams = $this->pdo->prepare('SELECT * FROM `league`');
        $getTeams -> execute();
        return $getTeams -> fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function setNeewMeet($team1,$team2,$league,$date){
        $setMeet = $this->pdo->prepare('INSERT INTO `leaguemeets` (`team1`,`team2`,`league`,`date`) VALUES (:teamId1, :teamId2, :leagueId, :date)');
        $setMeet -> bindParam(':teamId1',$team1,PDO::PARAM_INT);
        $setMeet -> bindParam(':teamId2',$team2,PDO::PARAM_INT);
        $setMeet -> bindParam(':leagueId',$league,PDO::PARAM_INT);
        $setMeet -> bindParam(':date',$date,PDO::PARAM_STR);
        return $setMeet -> execute();
    }

    public function checkExistCaptain($teamId){
        $checkCaptainExist = $this->pdo->prepare('SELECT count(`id`) AS captains_num FROM `users` WHERE `privilege` = 3 AND `team` = :teamId');
        $checkCaptainExist -> bindParam(':teamId',$teamId,PDO::PARAM_INT);
        $checkCaptainExist -> execute();
        $arrCaptainCheck = $checkCaptainExist -> fetch();
        if ($arrCaptainCheck['captains_num'] <= 0) { return false; } else { return true; }
    }
    
    public function setTeamCaptain($userId){
        $setCaptain = $this->pdo->prepare('UPDATE `users` SET `privilege` = 3 WHERE `id` = :userId');
        $setCaptain -> bindParam(':userId',$userId,PDO::PARAM_INT);
        return $setCaptain -> execute();
    }
    
    public function getQueueList(){
        $queue_with_players = array();
        
        $listQueueQuery = $this->pdo->prepare('SELECT queue.*, games.max_slots FROM queue INNER JOIN games ON queue.game = games.id');
        $listQueueQuery -> execute();
        $listQueue = $listQueueQuery -> fetchAll(PDO::FETCH_ASSOC);

        foreach($listQueue as $queueKeys => $queueValues){
            $tmpQueueAllPlayers     = array();
            $tmpQueueDcPlayers      = array();
            
            $queuePlayersQuery = $this->pdo->prepare('SELECT id,lobby FROM users WHERE team = :teamQueue1 OR team = :teamQueue2');
            $queuePlayersQuery -> bindParam(':teamQueue1', $listQueue[$queueKeys]['team1'],PDO::PARAM_INT);
            $queuePlayersQuery -> bindParam(':teamQueue2', $listQueue[$queueKeys]['team2'],PDO::PARAM_INT);
            $queuePlayersQuery -> execute();
            $queuePlayers = $queuePlayersQuery -> fetchAll();

            foreach($queuePlayers as $queuePlayerKey => $queuePlayerVal){
                    array_push($tmpQueueAllPlayers,$queuePlayerVal['id']);
            }
            
            $listQueue[$queueKeys]['players_all'] = $tmpQueueAllPlayers;
            
            foreach($queuePlayers as $playersKeys){
                if (is_null($playersKeys['lobby'])) {
                    array_push($tmpQueueDcPlayers,$playersKeys['id']);
                }
            }
            
            $listQueue[$queueKeys]['players_off'] = $tmpQueueDcPlayers;
        }
        
        /*
        foreach($listQueue as $queueFillKeys => $queueFillValues){
            if ($queueFillValues['slots'] < $queueFillValues['max_slots']) {
                array_push($queue_with_players,$listQueue);
            }
        }
        return $queue_with_players;
        */
        
        return $listQueue;
    }

    public function getQueueListWithPlayers(){

	    $user = new User($this->pdo);

        $queue_list_with_not_connected_players = $this -> getQueueList();

        foreach ($queue_list_with_not_connected_players as $lobby => $lobbyValues) {

            foreach ($queue_list_with_not_connected_players[$lobby]['players_off'] as $player_off => $player_off_id){

                $queue_list_with_not_connected_players[$lobby]['players_off'][$player_off] = $user -> getUserPublicData($player_off_id);
                
            }

        }

        return $queue_list_with_not_connected_players;
    }
    
    public function getLobbyList(){
        $categorizedArray = array("not_configured" => array(), "not_scored" => array(), "finalized" => array());
        
        $getLobbyList = $this->pdo->prepare('SELECT * FROM `lobby` ORDER BY id DESC');
        $getLobbyList -> execute();
        $lobbyList = $getLobbyList -> fetchAll(PDO::FETCH_ASSOC);
        $getLobbyScores = $this->pdo->prepare('SELECT * FROM `leaguescores`');
        $getLobbyScores -> execute();
        $lobbyScores = $getLobbyScores -> fetchAll(PDO::FETCH_ASSOC);

        foreach ($lobbyList as $lobbyData) {
                    
            if ( count(unserialize($lobbyData['server'])) == 0) {
                if ($lobbyData['game'] != core::$config['game']['hs_id']) { //hs not required to configure ip or password
                    array_push($categorizedArray['not_configured'],$lobbyData);
                }
            }
            
            $not_found_score = true;
            foreach ( $lobbyScores as $lobbyScore ) {
                if ( $lobbyScore['lobby'] == $lobbyData['id'] ) { // actually score for that lobby does exist
                    $not_found_score = false;
                    array_push($categorizedArray['finalized'],array_merge($lobbyData,$lobbyScore));
                }
            }
            if ( $not_found_score ) {
                    array_push($categorizedArray['not_scored'],$lobbyData);
                }
                
        }
        
        return $categorizedArray;
    }
    
    public function getAllPlayersArray(){
        $allPlayersArray = $this->pdo->prepare('SELECT * FROM `users` ORDER BY `users`.`team`');
        $allPlayersArray -> execute();
        return $allPlayersArray -> fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getAllGameLogins(){
        $allGameLogins = $this->pdo->prepare('SELECT * FROM usersgames');
        $allGameLogins -> execute();
        return $allGameLogins -> fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getLobbyData($id = null){
        $getLobbyData = $this->pdo->prepare('SELECT * FROM lobby WHERE id = :lobbyId');
            $getLobbyData->bindParam(':lobbyId',$id, PDO::PARAM_INT);
        $getLobbyData->execute();
        return $getLobbyData->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getLobbyReports($id){
        $getLobbyReports = $this -> pdo -> prepare('SELECT * FROM lobbyreports WHERE lobby = :lobbyId');
        $getLobbyReports -> bindParam(':lobbyId',$id,PDO::PARAM_INT);
        $getLobbyReports -> execute();
        return $getLobbyReports -> fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getMeetsList(){
        $getMeetsList = $this->pdo->prepare('SELECT * FROM leaguemeets ORDER BY date DESC');
        $getMeetsList -> execute();
        return $getMeetsList -> fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function deleteMeet($meetId){
        $deleteMeet = $this->pdo->prepare('DELETE FROM `leaguemeets` WHERE id = :meetId');
        $deleteMeet -> bindParam(':meetId',$meetId,PDO::PARAM_INT);
        return $deleteMeet->execute();
    }
    
    public function setLeagueMeetEnd($meetData){
        if (array_key_exists('lobby', $meetData) && array_key_exists('team1', $meetData)  && array_key_exists('team2', $meetData)  && array_key_exists('score1', $meetData)  && array_key_exists('score2', $meetData)  && array_key_exists('date', $meetData)) {
            $clearUsersLobby = $this->pdo->prepare('UPDATE `users` SET lobby = null WHERE lobby = :lobbyId');
            $clearUsersLobby -> bindParam(':lobbyId',$meetData['lobby'],PDO::PARAM_INT);
            $clearUsersLobby -> execute();

            $clearQueue = $this->pdo->prepare('DELETE FROM `queue` WHERE id = :lobbyId');
            $clearQueue -> bindParam(':lobbyId',$meetData['lobby'],PDO::PARAM_INT);
            $clearQueue -> execute();

            $setLeagueScores = $this -> pdo -> prepare('INSERT INTO `leaguescores` (`lobby`,`team1`,`team2`,`score1`,`score2`,`date`) VALUES (:lobbyId,:t1Id,:t2Id,:score1,:score2,:date)');
            $setLeagueScores -> bindParam(':lobbyId',$meetData['lobby'],PDO::PARAM_INT);
            $setLeagueScores -> bindParam(':t1Id',$meetData['team1'],PDO::PARAM_INT);
            $setLeagueScores -> bindParam(':t2Id',$meetData['team2'],PDO::PARAM_INT);
            $setLeagueScores -> bindParam(':score1',$meetData['score1'],PDO::PARAM_INT);
            $setLeagueScores -> bindParam(':score2',$meetData['score2'],PDO::PARAM_INT);
            $setLeagueScores -> bindParam(':date',$meetData['date'],PDO::PARAM_STR);
            $setLeagueScores -> execute();

            if ($clearUsersLobby && $clearQueue && $setLeagueScores) {
                return true;
            } else {
                return false;
            }
        } else {
         return false;   
        }
    }
}

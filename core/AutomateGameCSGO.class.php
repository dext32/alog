<?php

/**
 * Class AutomateGameCSGO
 */
class AutomateGameCSGO{

    /**
     * @var AutomateGameCSGORepository
     */
    private $csgoRepository;

    /**
     * @var AutomateGameService
     */
    private $autoGameService;

    /**
     * @var int
     */
    private $connection_from_lobby_id;

    /**
     * @var array
     */
    private $teams_from_lobby_data = [];

    /**
     * AutomateGameCSGO constructor.
     * @param AutomateGameCSGORepository $csgoRepository
     * @param AutomateGameService $autoGameService
     */
    public function __construct(AutomateGameCSGORepository $csgoRepository, AutomateGameService $autoGameService)
    {
        $this->csgoRepository = $csgoRepository;

        $this->autoGameService = $autoGameService;
    }

    /**
     * @param mixed $connection_from_lobby_id
     */
    public function setConnectionFromLobbyId($connection_from_lobby_id): void
    {
        $this->connection_from_lobby_id = $connection_from_lobby_id;
    }

    /**
     * @param array $teams_from_lobby_data
     */
    public function setTeamsFromLobbyData(array $teams_from_lobby_data): void
    {
        $this->teams_from_lobby_data = $teams_from_lobby_data;
    }

    /**
     * @return array
     */
    public function listServers(){
        return $this->csgoRepository->getServersData();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function checkLobbyServer(){
        if (isset($this->connection_from_lobby_id)){
            $serverList = $this->listServers();

            foreach ($serverList as $serverData) {
                if ($serverData['lobby_id'] == $this->connection_from_lobby_id) {
                    return $serverData;
                }
            }
        } else {
            throw new Exception("Lobby ID is undefined.");
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function tryAvailableServers(){

        $serverList = $this->listServers();

        foreach ($serverList as $serverData){
            if ( $serverData['in_use'] == FALSE ) {
                return $this->reserveServer($serverData['id']);
            }
        }

        return false;
    }


    /**
     * @param $serverId
     * @return bool
     * @throws Exception
     */
    private function reserveServer($serverId){
        if (isset($this->connection_from_lobby_id)) {
            $this->csgoRepository->setServerUnavialable($serverId, $this->connection_from_lobby_id);
            return true;
        } else {
            throw new Exception("Lobby ID is undefined.");
        }
    }

    /**
     * @return array
     */
    private function checkOutdatedServer(){
        $outdated_value = 21600; // 21600 seconds = 6 hours
        $serverList = $this->listServers();

        foreach ($serverList as $serverData) {
            if ( abs(Functions::dateCompare($serverData['start_time'])) > $outdated_value ) {
                $this->setServerAvailable($serverData['id']);
                return $serverData;
            }
        }
    }

    /**
     * @param $serverId
     */
    private function setServerAvailable($serverId){
        $this->csgoRepository->setServerAvaliable($serverId);
    }


    private function setServerRun($serverId){
        $this->csgoRepository->setServerRun($serverId);
    }

    /**
     * @return array
     * @throws Exception
     */
    public function establishConnection(){

        $serverData = $this->checkLobbyServer();

        if ( empty($serverData) ){

            if (!$this->tryAvailableServers()) {

                $cleanServer = $this->checkOutdatedServer();

                if ( !empty($cleanServer) ){

                    //pclose(popen("taskkill /F /IM srcds_csgo1.exe", "r"));

                    $this->tryAvailableServers();
                }

            }

        } else {

            if( !$serverData['run'] )
            {
                $this->generateConfig($serverData['name']);

                $connection = ssh2_connect('localhost', 22);
                ssh2_auth_password($connection, 'MSI', 'bulk');

                    $srcFile = "jscripts/json/csgo/".$serverData['name'].".json";
                    $dstFile = "/D:/ftp/csgo/".$serverData['name'].".json";

                    $sftp = ssh2_sftp($connection);

                    $sftpStream = @fopen('ssh2.sftp://'.intval($sftp).$dstFile, 'w');
                    $data_to_send = @file_get_contents($srcFile);

                    if ($data_to_send === false)
                        throw new Exception("Could not open local file: $srcFile.");

                    if (@fwrite($sftpStream, $data_to_send) === false)
                        throw new Exception("Could not send data from file: $srcFile.");

                    fclose($sftpStream);

                $stream = ssh2_exec($connection, 'php D:\xampp\htdocs\ssh\csgo.php');

                $this->setServerRun($serverData['id']);

                //pclose(popen("start /B E:\csgosl\server\start.bat", "r"));

            }

        }
        return $this->checkLobbyServer();
    }

	public function generateConfig($serverDataName){
        $lobbyId          = $this->connection_from_lobby_id;
        $lobbyPlayersData = $this->autoGameService->
                                getPlayersDataFromLobby(
                                    array_keys(
                                        $this->teams_from_lobby_data
                                    )
                            );
        $lobbyTeamsIds     = array_keys($this->teams_from_lobby_data);
        $lobbyTeamsNames   = array_values($this->teams_from_lobby_data);

		$paternConfig = [
			"matchid" => "csgo_lobby_".$lobbyId,
			"num_maps" => 3,
			"players_per_team" => 1,
			"min_players_to_ready" => 1,
			"min_spectators_to_ready" => 0,
			"skip_veto" => false,
			"veto_first" => "team1",
			"side_type" => "standard",

			"spectators" => [
				"players" => [
					"STEAM_1:1:....",
					"STEAM_1:1:....",
				]
			],
			
			"maplist" => [
				"de_mirage",
				"de_cache",
                "de_inferno",
                "de_nuke",
                "de_train",
                "de_overpass",
                "de_cobblestone"
			],
			
			"favored_percentage_team1" => 50,
			"favored_percentage_text" => "HLTV Bets",

			"team1" => [
				"name" => $lobbyTeamsNames[0],
				"tag" => $lobbyTeamsNames[0],
				"flag" => "PL",
				"logo" => "",
				"players" =>
				[

				]
			],

			"team2" => [
				"name" => $lobbyTeamsNames[1],
				"tag" => $lobbyTeamsNames[1],
				"flag" => "PL",
				"logo" => "",
				"players" =>
				[

				]
			],

			"cvars" => [
				"hostname" => "Academic League of Games - Match server #".$lobbyId
			]
		];

		foreach ($lobbyPlayersData as $playerData){
		    if ($playerData['team'] == $lobbyTeamsIds[0]){
                array_push($paternConfig['team1']['players'],$playerData['game_login']);
            } elseif ($playerData['team'] == $lobbyTeamsIds[1]) {
                array_push($paternConfig['team2']['players'],$playerData['game_login']);
            }
        }

        file_put_contents("jscripts/json/csgo/".$serverDataName.".json",json_encode($paternConfig));
	}
	
}
<?php


class Validatable
{
    public $validate_message = '';
    private $data_to_validate = Array([1, 2]);

    /*
     * Example of use validate:
     *
     *      $input->validate([
     *          'value1' => [Input::MAX_LENGTH(255), Input::STRING ],
     *          'value2' => [Input::REQUIRED, Input::MIN_LENGTH(2)]
     *      ]);
     *
     * or you can use shorter version, but your IDE wouldn't give hints
     *
     *       $input->validate([
     *          'value1' => ['max_length:255', 'string' ],
     *          'value2' => ['required', 'min_length:5']
     *      ]);
     *
     * Function returns 1 if pass or 0 if validation fails.
     * Moreover if validation fails it sets $validate_message
     *
     */
    public function validate($data)
    {
        foreach ($data as $key => $conditions)
        {
            foreach ($conditions as $condition)
            {
                $condition_array = explode(':', $condition);
                $function = 'validate_'.$condition_array[0];

                $return = 0;

                if(count($condition_array)==1)
                    $return = $this->$function([
                        'key' => $key,
                        'value' => $this->data_to_validate[$key]]
                    );
                else
                    $return = $this->$function([
                        'key' => $key,
                        'value' => $this->data_to_validate[$key]]
                        , $condition_array[1]);

                //var_dump($return);
                if (!$return == 0)
                {
                    $this->validate_message = $return;
                    return false;
                }
            }
        }
        return true;
    }

    protected function setValidatableData($data)
    {
        $this->data_to_validate = $data;
    }

    /*
     * There you can crete your own validate condition
     *
     * Use " validate_ " before your condition.
     * Next you can create shortcut at the beginning of file.
     *
     * for example:
     *
     *      const TEST = 'test';
     *      private function validate_test($data)
     *
     *
     * You can use one variable in your function, example:
     *
     *      public static function TEST($value) {return 'test:'.$value}
     *      private function validate_test($data, $value)
     *
     * As you see you can't use " : " character in your function name.
     *
     * $data argument is an array with 'key' and 'value' of testing input
     *
     * In body function return 0 if your test pass
     * or
     * return message which will be shown to user to explain why validate fails
     *
     */

    // TODO maybe get values of messages from lang file?

    public const REQUIRED = 'required';
    private function validate_required($data)
    {
        if(empty($data['value']))
            return  'Zmienna '. $data['key'] . ' jest wymagana.';

        return 0;
    }

    public const NUMERIC = 'numeric';
    private function validate_numeric($data)
    {
        if (!is_numeric($data['value']))
        {
            return 'Zmienna '. $data['key'] . ' musi być liczbą';
        }
        return 0;
    }

    public const EMAIL = 'email';
    private function validate_email($data)
    {
        if (filter_var($data['value'], FILTER_VALIDATE_EMAIL)) {
            return 0;
        } else {
            return "Variable ". $data['key']. " must be an e-mail";
        }
    }

    public static function MAX_LENGTH($l) { return 'max_length:'.$l; }
    private function validate_max_length($data, $length)
    {
        if (strlen($data['value']) > $length)
        {
            return 'Max długość '. $data['key'] . ' to: '.$length;
        }
        return 0;
    }

    public static function MIN_LENGTH($l) { return 'min_length:'.$l; }
    private function validate_min_length($data, $length)
    {
        if (strlen($data['value']) < $length)
        {
            return 'Min długość '. $data['key'] . ' to: '.$length;
        }
        return 0;
    }
}
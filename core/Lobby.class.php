<?php

class Lobby
{
     /**
     * @var
     */
    public $id      = null;
    public $team1   = null;
    public $team2   = null;
    public $team1_players = array();
    public $team2_players = array();
    public $team1_captain = null;
    public $team2_captain = null;
    public $picker = null;
    
    public $userData = array();
    /**
     * @param $pdo
     */
    protected $pdo;
    protected $session = null;
	
	public function __construct($pdo,$session){
		
		if (is_object($pdo)) {
			$this->pdo = $pdo;
		}

		if (is_object($session)) {
            $this->session = $session;
        }
        
        $user = new User($this->pdo);
        $this->userData = $user->getUserQueueData();
        
        $this->startLobby();
    }

    public function setLobbySession(){
        if(isset($_SESSION['lobby'])) {
            $this->id = $_SESSION['lobby'];
        }
    }
    
    public function startLobby(){
        $this->setLobbySession();
        $this->iniLobby();
        $this->setLobbyTeams();
        $this->setLobbyCaptains();
        $this->iniLobbyPicker();
        $this->toDbLobbyPlayers();
    }
    
    public function iniLobby(){
        $getQueueData = $this->pdo->prepare('SELECT * FROM queue WHERE id = :QueueId');
        $getQueueData -> bindParam(':QueueId',$_SESSION['lobby'],PDO::PARAM_INT);
        $getQueueData -> execute();
        $QueueData = $getQueueData -> fetch(PDO::FETCH_ASSOC);
        
        $getLobby = $this->pdo->prepare('SELECT id FROM lobby WHERE id = :lobbyId');
            $getLobby->bindParam(':lobbyId',$this->id, PDO::PARAM_INT);
        $getLobby->execute();
        $lobbyData = $getLobby->fetch(PDO::FETCH_ASSOC);


        if(count($lobbyData) == 0) {
            $setNewLobby = $this->pdo->prepare('INSERT INTO lobby (id,team1,team2,game,start_time) VALUES (:lobbyId,:team1,:team2,:gameId,now())');
            $setNewLobby -> bindParam(':lobbyId',$QueueData['id'],PDO::PARAM_INT);
            $setNewLobby -> bindParam(':team1',$QueueData['team1'],PDO::PARAM_INT);
            $setNewLobby -> bindParam(':team2',$QueueData['team2'],PDO::PARAM_INT);
            $setNewLobby -> bindParam(':gameId',$QueueData['game'],PDO::PARAM_INT);
            $setNewLobby -> execute();
            if ($setNewLobby) { return true; } else { return false; }
        } elseif( count($lobbyData) >= 1 ) {
            //$join
        }
    }
    
    public function getLobbyData($id = null){
        $getLobbyData = $this->pdo->prepare('SELECT * FROM lobby WHERE id = :lobbyId');
            (isset($id)) ? $getLobbyData->bindParam(':lobbyId',$id, PDO::PARAM_INT) : $getLobbyData->bindParam(':lobbyId',$this->id, PDO::PARAM_INT);
        $getLobbyData->execute();
        return $getLobbyData->fetch(PDO::FETCH_ASSOC);
    }
    
    public static function getLobbyDataById($pdo, $id){
        $getLobbyData = $pdo->prepare('SELECT * FROM lobby WHERE id = :lobbyId');
        $getLobbyData->bindParam(':lobbyId',$id, PDO::PARAM_INT);
        $getLobbyData->execute();
        return $getLobbyData->fetch(PDO::FETCH_ASSOC);
    }
    
    public function getConnectionParameter($id = null){
        /*$getConnData = $this->pdo->prepare('SELECT server FROM lobby WHERE id = :lobbyId');
            (isset($id)) ? $getConnData->bindParam(':lobbyId',$id, PDO::PARAM_INT) : $getConnData->bindParam(':lobbyId',$this->id, PDO::PARAM_INT);
        $getConnData -> execute();
        $connData = $getConnData->fetch(PDO::FETCH_ASSOC);
        return unserialize($connData['server']);*/
        return $this->dbSerialArr("server");
    }
        
    public function setConnectionParameter($id = null, $arrayConn = null){
        if (!is_array($arrayConn)) { 
            return false;
        } else {
            $parseConnArray = array("ip" => $arrayConn[0], "pw" => $arrayConn[1]);
        }
        $this->dbSerialArr("server", $parseConnArray, $id);
    }
    
    private static function sortPrivileges($a, $b)
	{
		if ($a['privilege'] == $b['privilege']) {
			return 0;
		}
		return ($a['privilege'] < $b['privilege']) ? 1 : -1;
	}
    
    public function setLobbyTeams(){
        $team = new Team($this->pdo);
        $lobbyData = $this->getLobbyData();

        if(!is_object($team)) { return false; }
        else {
            $this->team1 = $lobbyData['team1'];
            $this->team2 = $lobbyData['team2'];
            $this->team1_players = $team->getTeamMembers($lobbyData['team1']);
            $this->team2_players = $team->getTeamMembers($lobbyData['team2']);
            usort($this->team1_players, array('Lobby','sortPrivileges'));
            usort($this->team2_players, array('Lobby','sortPrivileges'));
        }
    }
    
    private function iniLobbyPicker(){
        $getLobbyData = $this->getLobbyData();
        $dbLobbyPicker = $getLobbyData['picker'];

        if (is_null($dbLobbyPicker)){
            if (is_null($this->team1_captain) || is_null($this->team2_captain)) $this->setLobbyCaptains();
            $setDbPicker = $this->pdo->prepare('UPDATE `lobby` SET `picker` = :iniPicker WHERE id = :lobbyId');
            $setDbPicker -> bindParam(':iniPicker',$this->team1_captain,PDO::PARAM_INT);
            $setDbPicker -> bindParam(':lobbyId',$this->id,PDO::PARAM_INT);
            $setDbPicker -> execute();
                        $getLobbyData = $this->getLobbyData();
                    $dbLobbyPicker = $getLobbyData['picker'];
            return $this->picker = $this->team1_captain;
        }
        return $this->picker = $dbLobbyPicker;
    }
    
    public function swapLobbyPicker(){
        $getLobbyData = $this->getLobbyData();
        $dbLobbyPicker = $getLobbyData['picker'];
        if ($dbLobbyPicker == $this->team1_captain) {
            $swapPicker = $this->team2_captain;
        } elseif ($dbLobbyPicker == $this->team2_captain) {
            $swapPicker = $this->team1_captain;
        }
        
        $setDbPicker = $this->pdo->prepare('UPDATE `lobby` SET `picker` = :swapPicker WHERE id = :lobbyId');
        $setDbPicker -> bindParam(':swapPicker',$swapPicker,PDO::PARAM_INT);
        $setDbPicker -> bindParam(':lobbyId',$this->id,PDO::PARAM_INT);
        return $setDbPicker -> execute();
    }
    
    public function setLobbyCaptains(){
        $joinedArr = $this->getJoinedPlayers();
        foreach($joinedArr as $joinedPlayer){
            foreach($this->team1_players as $teamPlayers){
                if (!isset($this->team1_captain)) {
                    if ($teamPlayers['id'] == $joinedPlayer && $teamPlayers['privilege'] == 3) {
                        $this->team1_captain = $teamPlayers['id'];
                    } 
                    /* elseif ($teamPlayers['id'] == $joinedPlayer) {
                        $this->team1_captain = $teamPlayers['id'];
                    } */
                }
            }
        }
        foreach($joinedArr as $joinedPlayer){
            foreach($this->team2_players as $teamPlayers){
                if (!isset($this->team2_captain)){
                    if ($teamPlayers['id'] == $joinedPlayer && $teamPlayers['privilege'] == 3) {
                        $this->team2_captain = $teamPlayers['id'];
                    } 
                    /* elseif ($teamPlayers['id'] == $joinedPlayer) {
                        $this->team2_captain = $teamPlayers['id'];
                    } */
                }
            }
        }
    }
    
    public function toDbLobbyPlayers(){
        $already_joined_player = null;
        $currentPlayers = $this->pdo->prepare('SELECT joined FROM lobby WHERE id = :lobbyId');
            $currentPlayers -> bindParam(':lobbyId',$this->id,PDO::PARAM_INT);
        $currentPlayers -> execute();
        $PlayersSerialArray = $currentPlayers->fetch(PDO::FETCH_ASSOC);
        
        $playersArray = unserialize($PlayersSerialArray['joined']);
        foreach ($playersArray as $player){
            if ( !isset($already_joined_player) ) {
                if ( $player == $this->userData['id'] ) { 
                    $already_joined_player = true; 
                }
            }
        }
        if ( $already_joined_player ) { return false; }
        else {
            $playersArray = array_merge( $playersArray, array($this->userData['id']) );
            $playersArray = serialize($playersArray);

            $insertPlayersSerialArr = $this->pdo->prepare('UPDATE lobby SET `joined` = :playersArr WHERE id = :lobbyId');
            $insertPlayersSerialArr -> bindParam(':playersArr', $playersArray, PDO::PARAM_STR);
            $insertPlayersSerialArr -> bindParam(':lobbyId', $this->id, PDO::PARAM_INT);
            return $insertPlayersSerialArr -> execute();
        }
    }
    
    public function getJoinedPlayers(){
        $jPlayersSerialArr = $this->pdo->prepare('SELECT `joined` FROM lobby WHERE id = :lobbyId');
        $jPlayersSerialArr -> bindParam(':lobbyId', $this->id, PDO::PARAM_INT);
        $jPlayersSerialArr -> execute();
        $jPlayersSArray = $jPlayersSerialArr -> fetch(PDO::FETCH_ASSOC);
        return unserialize($jPlayersSArray['joined']);
    }
    
    public function dbSerialArr($db_column, $src_array = null, $lobbyId = null){        
        if ( isset($src_array) ) { //set new array
            if (  !is_string($db_column) || !is_array($src_array) ) { return false; }

            $serial_arr = serialize($src_array);
            
            $updateDbArray = $this->pdo->prepare('UPDATE lobby SET '.$db_column.' = :serMerArray WHERE id = :lobbyId');
                         $updateDbArray->bindParam(':serMerArray', $serial_arr, PDO::PARAM_STR);
                         (isset($lobbyId)) ? $updateDbArray->bindParam(':lobbyId',$lobbyId, PDO::PARAM_INT) : $updateDbArray->bindParam(':lobbyId',$this->id, PDO::PARAM_INT);
            return $updateDbArray->execute();
        } else { //get read array
            $getDbArray = $this->pdo->prepare('SELECT '.$db_column.' FROM lobby WHERE id = :lobbyId');
                     (isset($lobbyId)) ? $getDbArray->bindParam(':lobbyId',$lobbyId, PDO::PARAM_INT) : $getDbArray->bindParam(':lobbyId',$this->id, PDO::PARAM_INT);
                     $getDbArray->execute();
            $dbArray = $getDbArray -> fetch(PDO::FETCH_ASSOC);
            
            if (  !is_string($db_column) || !is_null($src_array) ) { return false; }
            return unserialize($dbArray[$db_column]);
        }
    }
    
    public function toDbLobbyReport($info, $scrsht_path = null){
        $setReport = $this->pdo->prepare('INSERT INTO lobbyreports (`lobby`,`user`,`info`,`screenshot`) VALUES (:lobbyId,:userId,:info,:screenshot)');
            $setReport->bindParam(':lobbyId',$this->id,PDO::PARAM_INT);
            $setReport->bindParam(':userId',$_SESSION['id'],PDO::PARAM_INT);
            $setReport->bindParam(':info',$info,PDO::PARAM_STR);
            is_null($scrsht_path) ? $setReport->bindValue(':screenshot',null) : $setReport->bindParam(':screenshot',$scrsht_path,PDO::PARAM_STR);
        return $setReport->execute();
    }
    
    public function dbLobbyReport($lobby){
        $getReport = $this->pdo->prepare('SELECT * FROM lobbyreports WHERE lobby = :lobbyId');
            $getReport->bindParam(':lobbyId',$lobby,PDO::PARAM_INT);
        $getReport->execute();
        return $getReport -> fetchAll(PDO::FETCH_ASSOC);
    }
}




class Csgo {
    public $maps = array("de_dust2" => "0", "de_inferno" => "0", "de_nuke" => "0", "de_cache" => "0", "de_mirage" => "0", "de_cobblestone" => "0", "de_overpass" => "0", "de_train" => "0");
}

class Overwatch {
    
    public $lobby;
    
    public $modes = array("Control" => "0", "Escort" => "0", "Assault" => "0", "Hybrid" => "0");
    public $Control_maps = array("Ilios" => "0", "Lijiang Tower" => "0", "Nepal" => "0", "Oasis" => "0");
    public $Escort_maps = array("Dorado" => "0", "Junkertown" => "0", "Route 66" => "0", "Watchpoint: Gibraltar" => "0");
    public $Assault_maps = array("Hanamura" => "0", "Horizon Lunar Colony" => "0", "Temple of Anubis" => "0", "Volskaya Industries" => "0");
    public $Hybrid_maps = array("Eichenwalde" => "0", "Hollywood" => "0", "Kings Row" => "0", "Numbani" => "0");
    
    public function __construct($lobby){
        if (is_object($lobby)) {
			$this->lobby = $lobby;
		}
        
        $this->iniGameModes();
    }
    
    public function iniGameModes(){
        $isIniModes = $this->lobby->dbSerialArr("banned");
        if(count($isIniModes) == 0) {
            $this->lobby->dbSerialArr("banned",$this->modes);
            return $this->lobby->dbSerialArr("banned");
        }
        return $isIniModes;
    }
    
    public function iniGameMaps(){
        $currentBanArray = $this->lobby->dbSerialArr("banned");
        if (array_keys($currentBanArray) == array_keys($this->modes)){
            $modeAndMapsArr = array($this->lobby->dbSerialArr("banned"));
            foreach($modeAndMapsArr[0] as $modes => $bannedValue){
                if($modes == "Control" && $bannedValue == "0") {
                    array_push($modeAndMapsArr,$this->Control_maps);
                }
                if($modes == "Escort" && $bannedValue == "0") {
                    array_push($modeAndMapsArr,$this->Escort_maps);
                }
                if($modes == "Assault" && $bannedValue == "0") {
                    array_push($modeAndMapsArr,$this->Assault_maps);
                }
                if($modes == "Hybrid" && $bannedValue == "0") {
                    array_push($modeAndMapsArr,$this->Hybrid_maps);
                }
                $this->lobby->dbSerialArr("banned",array($modeAndMapsArr));
            }
            return $this->lobby->dbSerialArr("banned");
        }
        return $currentBanArray;
    }
    
    
}

class Lol {
    public $lobby;
    public $lobby_pw = null;
    
    public function __construct($lobby){
        if (is_object($lobby)) {
			$this->lobby = $lobby;
		}
        
        $this->setLocalPassword();
        $this->setLobbyPassword();
    }
    
    private function setLobbyPassword(){
        $isIniPw = $this->lobby->dbSerialArr("server");
        if(count($isIniPw) == 0) {
            $this->lobby->dbSerialArr("server",array("lol-pw" => $this->lobby_pw));
            return $this->lobby->dbSerialArr("server");
        }
        return $isIniPw;
    }
    
    public function getLobbyPassword(){
        $lobby_server = $this->lobby->dbSerialArr("server");
        return $lobby_server['lol-pw'];
    }
    
    private function setLocalPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $this->lobby_pw = implode($pass); //turn the array into a string
    }
}

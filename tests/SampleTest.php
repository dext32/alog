<?php

namespace tests;

use PHPUnit\Framework\TestCase;
use tests\Factories\ModelFactory;


class SampleTest extends TestCase
{

    public function test_TrueEqualsTrue()
    {
        // echo 'Runing test one';
        // You can write echo there but please remove it after debugging for tests cleanliness.

        $this->assertEquals(true, true);
    }

    public function test_VariableTrueIsTrue()
    {
        $variable = true;
        $this->assertTrue($variable);
    }

    public function test_ModelFactoryReturnsModelObject()
    {
        $modelFactory = new ModelFactory();
        $model = $modelFactory->createModel();

        $this->assertEquals('Model object', $model);
    }

    public function test_CanCreateControllerFromTestsNamespace()
    {
        $controller = new \IndexController();

        $this->assertTrue(
            $controller instanceof \Controller
        );
    }
}

<?php

namespace tests\Factories;

class ModelFactory
{
    public function createModel()
    {
        return 'Model object';
    }
}
<?php

// Flush the output buffer
ob_start();

// Start session
session_start();

// Include classes
require_once 'autoload.php';

// Create new object of core class
$core = new Core;

// Set handler for all Exceptions
set_exception_handler ( 'exception_handler' );
function exception_handler(Throwable $e){
    Functions::showException( $e );
}

// Set new variable with object of PDO
$pdo = $core -> DBconnection();

// Create new object of session class
$session = new Session;

// Build instance of Dependency Injection Container
$core->buildDI($pdo, $session);

// Run the application core
$core->runApplication($pdo, $session);


// End buffer
ob_end_flush();

-- phpMyAdmin SQL Dump
-- version home.pl
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 31 Sty 2018, 16:45
-- Wersja serwera: 5.7.19-17-log
-- Wersja PHP: 7.1.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `00059448_liga`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `university`
--

CREATE TABLE IF NOT EXISTS `university` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `university`
--

INSERT INTO `university` (`id`, `name`) VALUES
(1, 'Akademia FinansĂłw i Biznesu Vistula'),
(2, 'Akademia GĂłrniczo-Hutnicza im. StanisĹawa Staszica w Krakowie'),
(3, 'Akademia Humanistyczna im. Aleksandra Gieysztora w PuĹtusku'),
(4, 'Akademia Humanistyczno-Ekonomiczna w Ĺodzi'),
(5, 'Akademia Ignatianum w Krakowie'),
(6, 'Akademia im. Jakuba z ParadyĹźa'),
(7, 'Akademia im. Jana DĹugosza w CzÄstochowie'),
(8, 'Akademia Leona KoĹşmiĹskiego w Warszawie'),
(9, 'Akademia Marynarki Wojennej im. BohaterĂłw Westerplatte'),
(10, 'Akademia Morska w Gdyni'),
(11, 'Akademia Morska w Szczecinie'),
(12, 'Akademia Muzyczna im. Feliksa Nowowiejskiego w Bydgoszczy'),
(13, 'Akademia Muzyczna im. GraĹźyny i Kiejstuta BacewiczĂłw w Ĺodzi'),
(14, 'Akademia Muzyczna im. Ignacego Jana Paderewskiego w Poznaniu'),
(15, 'Akademia Muzyczna im. Karola LipiĹskiego we WrocĹawiu'),
(16, 'Akademia Muzyczna im. Karola Szymanowskiego w Katowicach'),
(17, 'Akademia Muzyczna im. StanisĹawa Moniuszki w GdaĹsku'),
(18, 'Akademia Muzyczna w Krakowie'),
(19, 'Akademia Pedagogiki Specjalnej im. Marii Grzegorzewskiej w Warszawie'),
(20, 'Akademia Polonijna w CzÄstochowie'),
(21, 'Akademia Pomorska w SĹupsku'),
(22, 'Akademia Sztuk PiÄknych im. Eugeniusza Gepperta we WrocĹawiu'),
(23, 'Akademia Sztuk PiÄknych im. Jana Matejki w Krakowie'),
(24, 'Akademia Sztuk PiÄknych im. WĹadysĹawa StrzemiĹskiego w Ĺodzi'),
(25, 'Akademia Sztuk PiÄknych w GdaĹsku'),
(26, 'Akademia Sztuk PiÄknych w Katowicach'),
(27, 'Akademia Sztuk PiÄknych w Warszawie'),
(28, 'Akademia Sztuki w Szczecinie'),
(29, 'Akademia Sztuki Wojennej'),
(30, 'Akademia Teatralna im. Aleksandra Zelwerowicza w Warszawie'),
(31, 'Akademia Techniczno-Humanistyczna w Bielsku-BiaĹej'),
(32, 'Akademia Wychowania Fizycznego i Sportu im. JÄdrzeja Ĺniadeckiego w GdaĹsku'),
(33, 'Akademia Wychowania Fizycznego im. BronisĹawa Czecha w Krakowie'),
(34, 'Akademia Wychowania Fizycznego im. Eugeniusza Piaseckiego w Poznaniu'),
(35, 'Akademia Wychowania Fizycznego im. Jerzego Kukuczki w Katowicach'),
(36, 'Akademia Wychowania Fizycznego JĂłzefa PiĹsudskiego w Warszawie'),
(37, 'Akademia Wychowania Fizycznego we WrocĹawiu'),
(38, 'ALMAMER SzkoĹa WyĹźsza z siedzibÄ w Warszawie'),
(39, 'Ateneum - SzkoĹa WyĹźsza w GdaĹsku'),
(42, 'Beskidzka WyĹźsza SzkoĹa UmiejÄtnoĹci w Ĺźywcu'),
(44, 'Bielska WyĹźsza SzkoĹa im. JĂłzefa Tyszkiewicza w Bielsku-BiaĹej'),
(45, 'Bydgoska SzkoĹa WyĹźsza w Bydgoszczy'),
(47, 'Centrum Medyczne KsztaĹcenia Podyplomowego w Warszawie'),
(48, 'ChrzeĹcijaĹska Akademia Teologiczna w Warszawie'),
(49, 'Collegium Civitas w Warszawie'),
(50, 'Collegium Da Vinci z siedzibÄ w Poznaniu'),
(52, 'Collegium Masoviense - WyĹźsza SzkoĹa Nauk o Zdrowiu w Ĺźyrardowie'),
(53, 'Collegium Mazovia Innowacyjna SzkoĹa WyĹźsza w Siedlcach'),
(54, 'Collegium Medicum WyĹźsza SzkoĹa SĹuĹźb Medycznych z siedzibÄ w Warszawie'),
(55, 'Collegium Varsoviense'),
(56, 'DolnoĹlÄska SzkoĹa WyĹźsza z siedzibÄ we WrocĹawiu'),
(58, 'ElblÄska Uczelnia Humanistyczno-Ekonomiczna w ElblÄgu'),
(62, 'Europejska SzkoĹa WyĹźsza w Sopocie z siedzibÄ w Sopocie'),
(63, 'Europejska Uczelnia SpoĹeczno - Techniczna w Radomiu'),
(64, 'Europejska Uczelnia w Warszawie'),
(66, 'Europejska WyĹźsza SzkoĹa Biznesu w Poznaniu'),
(67, 'Europejska WyĹźsza SzkoĹa Prawa i Administracji w Warszawie'),
(68, 'Ewangelikalna WyĹźsza SzkoĹa Teologiczna we WrocĹawiu'),
(70, 'GdaĹska SzkoĹa WyĹźsza z siedzibÄ w GdaĹsku'),
(71, 'GdaĹska WyĹźsza SzkoĹa Humanistyczna'),
(73, 'GdaĹski Uniwersytet Medyczny'),
(74, 'Gliwicka WyĹźsza SzkoĹa PrzedsiÄbiorczoĹci w Gliwicach'),
(75, 'GnieĹşnieĹska SzkoĹa WyĹźsza Milenium z siedzibÄ w GnieĹşnie'),
(76, 'GĂłrnoĹlÄska WyĹźsza SzkoĹa Handlowa im. Wojciecha Korfantego w Katowicach'),
(78, 'GĂłrnoĹlÄska WyĹźsza SzkoĹa PrzedsiÄbiorczoĹci im. Karola Goduli w Chorzowie'),
(79, 'Instytut Teologiczny im. bĹ. Wincentego KadĹubka w Sandomierzu'),
(80, 'Instytut Teologiczny im. Ĺw. Jana Kantego w Bielsku-BiaĹej'),
(81, 'Karkonoska PaĹstwowa SzkoĹa WyĹźsza w Jeleniej GĂłrze'),
(82, 'Kaszubsko-Pomorska SzkoĹa WyĹźsza w Wejherowie'),
(83, 'Katolicki Uniwersytet Lubelski Jana PawĹa II w Lublinie'),
(84, 'Kolegium JagielloĹskie ? ToruĹska SzkoĹa WyĹźsza'),
(85, 'KoszaliĹska WyĹźsza SzkoĹa Nauk Humanistycznych w Koszalinie'),
(86, 'Krakowska Akademia im. Andrzeja Frycza Modrzewskiego w Krakowie'),
(88, 'Krakowska WyĹźsza SzkoĹa Promocji Zdrowia w Krakowie'),
(89, 'Kujawska SzkoĹa WyĹźsza we WĹocĹawku'),
(90, 'Kujawsko-Pomorska SzkoĹa WyĹźsza w Bydgoszczy'),
(91, 'Lingwistyczna SzkoĹa WyĹźsza w Warszawie'),
(93, 'Lubelska SzkoĹa WyĹźsza w Rykach'),
(94, 'Lubuska WyĹźsza SzkoĹa Zdrowia Publicznego w Zielonej GĂłrze'),
(95, 'ĹuĹźycka SzkoĹa WyĹźsza im. Jana Benedykta Solfy z siedzibÄ w Ĺźarach'),
(96, 'MaĹopolska WyĹźsza SzkoĹa Ekonomiczna z siedzibÄ w Tarnowie'),
(97, 'MaĹopolska WyĹźsza SzkoĹa im. JĂłzefa Dietla w Krakowie'),
(101, 'MiÄdzynarodowa WyĹźsza SzkoĹa Logistyki i Transportu we WrocĹawiu'),
(102, 'NadbuĹźaĹska SzkoĹa WyĹźsza w Siemiatyczach z siedzibÄ w Siemiatyczach'),
(103, 'NiepaĹstwowa WyĹźsza SzkoĹa Pedagogiczna w BiaĹymstoku'),
(104, 'Niepubliczna WyĹźsza SzkoĹa Medyczna we WrocĹawiu'),
(105, 'OlsztyĹska SzkoĹa WyĹźsza im. JĂłzefa Rusieckiego'),
(107, 'PaĹstwowa Medyczna WyĹźsza SzkoĹa Zawodowa w Opolu'),
(108, 'PaĹstwowa SzkoĹa WyĹźsza im. PapieĹźa Jana PawĹa II w BiaĹej Podlaskiej'),
(109, 'PaĹstwowa WyĹźsza SzkoĹa Filmowa, Telewizyjna i Teatralna im. Leona Schillera w Ĺodzi'),
(110, 'PaĹstwowa WyĹźsza SzkoĹa Informatyki i PrzedsiÄbiorczoĹci w ĹomĹźy'),
(111, 'PaĹstwowa WyĹźsza SzkoĹa Teatralna im. Ludwika Solskiego w Krakowie'),
(112, 'PaĹstwowa WyĹźsza SzkoĹa Techniczno-Ekonomiczna im. ks. BronisĹawa Markiewicza w JarosĹawiu'),
(113, 'PaĹstwowa WyĹźsza SzkoĹa Wschodnioeuropejska w PrzemyĹlu'),
(114, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. Angelusa Silesiusa w WaĹbrzychu'),
(115, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. Hipolita Cegielskiego w GnieĹşnie'),
(116, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. Jana Amosa KomeĹskiego w Lesznie'),
(117, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. Jana Grodka w Sanoku'),
(118, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. Prezydenta StanisĹawa Wojciechowskiego w Kaliszu'),
(119, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. prof. E. Szczepanika w SuwaĹkach'),
(120, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. prof. StanisĹawa Tarnowskiego w Tarnobrzegu'),
(121, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. rotmistrza Witolda Pileckiego w OĹwiÄcimiu'),
(122, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. StanisĹawa Pigonia w KroĹnie'),
(123, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. StanisĹawa Staszica w Pile'),
(124, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. Szymona Szymonowica w ZamoĹciu'),
(125, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa im. Witelona w Legnicy'),
(126, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w CheĹmie'),
(127, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Ciechanowie'),
(128, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w ElblÄgu'),
(129, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w GĹogowie'),
(130, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Koninie'),
(131, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Koszalinie'),
(132, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Nowym SÄczu'),
(133, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Nysie'),
(134, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w PĹocku'),
(135, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Raciborzu'),
(137, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Skierniewicach'),
(138, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Sulechowie'),
(139, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Tarnowie'),
(140, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa w WaĹczu'),
(141, 'PaĹstwowa WyĹźsza SzkoĹa Zawodowa we WĹocĹawku'),
(142, 'Papieski WydziaĹ Teologiczny w Warszawie'),
(143, 'Papieski WydziaĹ Teologiczny we WrocĹawiu'),
(145, 'Pedagogium WyĹźsza SzkoĹa Nauk SpoĹecznych w Warszawie'),
(146, 'PodhalaĹska PaĹstwowa WyĹźsza SzkoĹa Zawodowa w Nowym Targu'),
(147, 'Podkarpacka SzkoĹa WyĹźsza im. bĹ. ks. WĹadysĹawa Findysza w JaĹle'),
(148, 'PodkowiaĹska WyĹźsza SzkoĹa Medyczna im. Zofii i Jonasza Ĺyko w Podkowie LeĹnej'),
(149, 'Politechnika BiaĹostocka'),
(150, 'Politechnika CzÄstochowska'),
(151, 'Politechnika GdaĹska'),
(152, 'Politechnika KoszaliĹska'),
(153, 'Politechnika Krakowska im. Tadeusza KoĹciuszki'),
(154, 'Politechnika Lubelska'),
(155, 'Politechnika ĹĂłdzka'),
(156, 'Politechnika Opolska'),
(157, 'Politechnika PoznaĹska'),
(158, 'Politechnika Rzeszowska im. Ignacego Ĺukasiewicza'),
(159, 'Politechnika ĹlÄska'),
(160, 'Politechnika ĹwiÄtokrzyska'),
(161, 'Politechnika Warszawska'),
(162, 'Politechnika WrocĹawska'),
(163, 'Polsko-Czeska WyĹźsza SzkoĹa Biznesu i Sportu "Collegium Glacense" z siedzibÄ w Stalowej Woli'),
(164, 'Polsko-JapoĹska Akademia Technik Komputerowych'),
(165, 'Pomorska SzkoĹa WyĹźsza w Starogardzie GdaĹskim'),
(166, 'Pomorska WyĹźsza SzkoĹa Nauk Stosowanych w Gdyni'),
(167, 'Pomorski Uniwersytet Medyczny w Szczecinie'),
(168, 'PowiĹlaĹska SzkoĹa WyĹźsza w Kwidzynie'),
(169, 'Powszechna WyĹźsza SzkoĹa Humanistyczna "Pomerania" w Chojnicach'),
(170, 'PrawosĹawne Seminarium Duchowne w Warszawie'),
(173, 'Prywatna WyĹźsza SzkoĹa Nauk SpoĹecznych, Komputerowych i Medycznych z siedzibÄ w Warszawie'),
(174, 'Prywatna WyĹźsza SzkoĹa Ochrony Ĺrodowiska w Radomiu'),
(176, 'Radomska SzkoĹa WyĹźsza'),
(178, 'Sopocka SzkoĹa WyĹźsza z siedzibÄ w Sopocie'),
(179, 'SpoĹeczna Akademia Nauk z siedzibÄ w Ĺodzi'),
(181, 'Staropolska SzkoĹa WyĹźsza w Kielcach'),
(183, 'SWPS Uniwersytet HumanistycznospoĹeczny z siedzibÄ w Warszawie'),
(184, 'SzczeciĹska SzkoĹa WyĹźsza Collegium Balticum w Szczecinie'),
(185, 'SzkoĹa GĹĂłwna Gospodarstwa Wiejskiego w Warszawie'),
(186, 'SzkoĹa GĹĂłwna Handlowa w Warszawie'),
(187, 'SzkoĹa GĹĂłwna Politechniczna z siedzibÄ w Nowym SÄczu'),
(188, 'SzkoĹa GĹĂłwna SĹuĹźby PoĹźarniczej'),
(189, 'SzkoĹa GĹĂłwna Turystyki i Rekreacji'),
(191, 'SzkoĹa WyĹźsza Ekonomii i ZarzÄdzania w Ĺodzi'),
(192, 'SzkoĹa WyĹźsza im. Bogdana JaĹskiego w Warszawie'),
(193, 'SzkoĹa WyĹźsza im. PawĹa WĹodkowica w PĹocku'),
(195, 'SzkoĹa WyĹźsza Przymierza Rodzin w Warszawie'),
(196, 'SzkoĹa WyĹźsza RzemiosĹ Artystycznych i ZarzÄdzania we WrocĹawiu'),
(198, 'ĹlÄska WyĹźsza SzkoĹa Informatyczno-Medyczna w Chorzowie'),
(199, 'ĹlÄska WyĹźsza SzkoĹa Medyczna w Katowicach'),
(201, 'ĹlÄski Uniwersytet Medyczny w Katowicach'),
(202, 'ĹwiÄtokrzyska SzkoĹa WyĹźsza w Kielcach'),
(203, 'Tarnowska SzkoĹa WyĹźsza z siedzibÄ w Tarnowie'),
(204, 'Uczelnia im. Edwarda Herzberga z siedzibÄ w GrudziÄdzu'),
(205, 'Uczelnia Jana WyĹźykowskiego'),
(206, 'Uczelnia JaĹskiego z siedzibÄ w ĹomĹźy'),
(207, 'Uczelnia Lingwistyczno-Techniczna w Ĺwieciu'),
(208, 'Uczelnia Ĺazarskiego w Warszawie'),
(209, 'Uczelnia Nauk SpoĹecznych'),
(210, 'Uczelnia Techniczno-Handlowa im. Heleny Chodkowskiej'),
(212, 'Uczelnia Warszawska im. Marii SkĹodowskiej-Curie w Warszawie'),
(214, 'Uniwersytet Artystyczny w Poznaniu'),
(215, 'Uniwersytet Ekonomiczny w Katowicach'),
(216, 'Uniwersytet Ekonomiczny w Krakowie'),
(217, 'Uniwersytet Ekonomiczny w Poznaniu'),
(218, 'Uniwersytet Ekonomiczny we WrocĹawiu'),
(219, 'Uniwersytet GdaĹski'),
(220, 'Uniwersytet im. Adama Mickiewicza w Poznaniu'),
(221, 'Uniwersytet JagielloĹski w Krakowie'),
(222, 'Uniwersytet Jana Kochanowskiego w Kielcach'),
(223, 'Uniwersytet KardynaĹa Stefana WyszyĹskiego w Warszawie'),
(224, 'Uniwersytet Kazimierza Wielkiego w Bydgoszczy'),
(225, 'Uniwersytet ĹĂłdzki'),
(226, 'Uniwersytet Marii Curie-SkĹodowskiej w Lublinie'),
(227, 'Uniwersytet Medyczny im. Karola Marcinkowskiego w Poznaniu'),
(228, 'Uniwersytet Medyczny im. PiastĂłw ĹlÄskich we WrocĹawiu'),
(229, 'Uniwersytet Medyczny w BiaĹymstoku'),
(230, 'Uniwersytet Medyczny w Lublinie'),
(231, 'Uniwersytet Medyczny w Ĺodzi'),
(232, 'Uniwersytet MikoĹaja Kopernika w Toruniu'),
(233, 'Uniwersytet Muzyczny Fryderyka Chopina w Warszawie'),
(234, 'Uniwersytet Opolski'),
(235, 'Uniwersytet Papieski Jana PawĹa II w Krakowie'),
(236, 'Uniwersytet Pedagogiczny im. Komisji Edukacji Narodowej w Krakowie'),
(237, 'Uniwersytet Przyrodniczo-Humanistyczny w Siedlcach'),
(238, 'Uniwersytet Przyrodniczy w Lublinie'),
(239, 'Uniwersytet Przyrodniczy w Poznaniu'),
(240, 'Uniwersytet Przyrodniczy we WrocĹawiu'),
(241, 'Uniwersytet Rolniczy im. Hugona KoĹĹÄtaja w Krakowie'),
(242, 'Uniwersytet Rzeszowski'),
(243, 'Uniwersytet SzczeciĹski'),
(244, 'Uniwersytet ĹlÄski w Katowicach'),
(245, 'Uniwersytet Technologiczno-Humanistyczny im. Kazimierza PuĹaskiego w Radomiu'),
(246, 'Uniwersytet Technologiczno-Przyrodniczy im. Jana i JÄdrzeja Ĺniadeckich w Bydgoszczy'),
(247, 'Uniwersytet w BiaĹymstoku'),
(248, 'Uniwersytet WarmiĹsko-Mazurski w Olsztynie'),
(249, 'Uniwersytet Warszawski'),
(250, 'Uniwersytet WrocĹawski'),
(251, 'Uniwersytet ZielonogĂłrski'),
(252, 'VIAMODA SzkoĹa WyĹźsza z siedzibÄ w Warszawie'),
(254, 'Warszawska SzkoĹa Filmowa z siedzibÄ w Warszawie'),
(255, 'Warszawska SzkoĹa WyĹźsza z siedzibÄ w Otwocku'),
(256, 'Warszawska SzkoĹa ZarzÄdzania - SzkoĹa WyĹźsza w Warszawie'),
(257, 'Warszawska WyĹźsza SzkoĹa Ekonomiczna im. Edwarda Wiszniewskiego w Warszawie'),
(258, 'Warszawska WyĹźsza SzkoĹa Humanistyczna im. BolesĹawa Prusa'),
(259, 'Warszawska WyĹźsza SzkoĹa Informatyki w Warszawie'),
(260, 'Warszawski Uniwersytet Medyczny'),
(262, 'Wielkopolska WyĹźsza SzkoĹa SpoĹeczno-Ekonomiczna w Ĺrodzie Wielkopolskiej'),
(264, 'Wojskowa Akademia Techniczna im. JarosĹawa DÄbrowskiego'),
(265, 'WrocĹawska WyĹźsza SzkoĹa Informatyki Stosowanej we WrocĹawiu'),
(266, 'WschĂłd-ZachĂłd SzkoĹa WyĹźsza im. Henryka JĂłĹşwiaka'),
(268, 'Wszechnica Polska SzkoĹa WyĹźsza w Warszawie'),
(269, 'Wszechnica ĹwiÄtokrzyska w Kielcach'),
(272, 'WyĹźsza InĹźynierska SzkoĹa BezpieczeĹstwa i Organizacji Pracy w Radomiu'),
(274, 'WyĹźsza MiÄdzynarodowa SzkoĹa Biznesu w Warszawie'),
(280, 'WyĹźsza SzkoĹa - Edukacja w Sporcie w Warszawie'),
(281, 'WyĹźsza SzkoĹa Administracji i Biznesu im. Eugeniusza Kwiatkowskiego w Gdyni'),
(283, 'WyĹźsza SzkoĹa Administracji Publicznej imienia StanisĹawa Staszica w BiaĹymstoku'),
(284, 'WyĹźsza SzkoĹa Administracji Publicznej w Kielcach'),
(286, 'WyĹźsza SzkoĹa Administracji Publicznej w OstroĹÄce'),
(287, 'WyĹźsza SzkoĹa Administracji Publicznej w Szczecinie'),
(288, 'WyĹźsza SzkoĹa Administracji w Bielsku-BiaĹej'),
(289, 'WyĹźsza SzkoĹa Administracyjno-SpoĹeczna w Warszawie'),
(290, 'WyĹźsza SzkoĹa Agrobiznesu w ĹomĹźy'),
(291, 'WyĹźsza SzkoĹa Artystyczna w Warszawie'),
(293, 'WyĹźsza SzkoĹa Bankowa w Poznaniu'),
(294, 'WyĹźsza SzkoĹa Bankowa w Toruniu'),
(295, 'WyĹźsza SzkoĹa Bankowa we WrocĹawiu'),
(296, 'WyĹźsza SzkoĹa Bankowa z siedzibÄ w GdaĹsku'),
(297, 'WyĹźsza SzkoĹa BankowoĹci i FinansĂłw z siedzibÄ w Katowicach'),
(298, 'WyĹźsza SzkoĹa BezpieczeĹstwa i Ochrony im. MarszaĹka JĂłzefa PiĹsudskiego w Warszawie'),
(299, 'WyĹźsza SzkoĹa BezpieczeĹstwa Publicznego i Indywidualnego "Apeiron" w Krakowie'),
(300, 'WyĹźsza SzkoĹa BezpieczeĹstwa w Poznaniu'),
(301, 'WyĹźsza SzkoĹa BezpieczeĹstwa w PrzemyĹlu'),
(302, 'WyĹźsza SzkoĹa Biznesu - National Louis University z siedzibÄ w Nowym SÄczu'),
(304, 'WyĹźsza SzkoĹa Biznesu i Administracji w Ĺukowie'),
(305, 'WyĹźsza SzkoĹa Biznesu i Nauk o Zdrowiu w Ĺodzi'),
(306, 'WyĹźsza SzkoĹa Biznesu i PrzedsiÄbiorczoĹci w Ostrowcu ĹwiÄtokrzyskim'),
(307, 'WyĹźsza SzkoĹa Biznesu i ZarzÄdzania w Ciechanowie'),
(308, 'WyĹźsza SzkoĹa Biznesu im. Biskupa Jana Chrapka w Radomiu'),
(309, 'WyĹźsza SzkoĹa Biznesu w DÄbrowie GĂłrniczej'),
(310, 'WyĹźsza SzkoĹa Biznesu w Gorzowie Wlkp.'),
(311, 'WyĹźsza SzkoĹa Biznesu z siedzibÄ w Pile'),
(314, 'WyĹźsza SzkoĹa COSINUS w Ĺodzi'),
(315, 'WyĹźsza SzkoĹa Demokracji im. ks. Jerzego PopieĹuszki w GrudziÄdzu'),
(317, 'WyĹźsza SzkoĹa Edukacji i Terapii im. prof. Kazimiery Milanowskiej z siedzibÄ w Poznaniu'),
(318, 'WyĹźsza SzkoĹa Edukacji Zdrowotnej i Nauk SpoĹecznych w Ĺodzi'),
(319, 'WyĹźsza SzkoĹa Ekologii i ZarzÄdzania w Warszawie'),
(320, 'WyĹźsza SzkoĹa Ekonomiczna w BiaĹymstoku'),
(322, 'WyĹźsza SzkoĹa Ekonomiczna w Stalowej Woli'),
(323, 'WyĹźsza SzkoĹa Ekonomiczno-Humanistyczna im. prof. Szczepana A. PieniÄĹźka w Skierniewicach'),
(324, 'WyĹźsza SzkoĹa Ekonomiczno-Humanistyczna w Bielsku-BiaĹej'),
(326, 'WyĹźsza SzkoĹa Ekonomiczno-SpoĹeczna w OstroĹÄce'),
(330, 'WyĹźsza SzkoĹa Ekonomii i Informatyki w Krakowie'),
(331, 'WyĹźsza SzkoĹa Ekonomii i Innowacji w Lublinie'),
(332, 'WyĹźsza SzkoĹa Ekonomii, Prawa i Nauk Medycznych im. prof. Edwarda LipiĹskiego w Kielcach'),
(333, 'WyĹźsza SzkoĹa Europejska im. Ks. JĂłzefa Tischnera w Krakowie'),
(334, 'WyĹźsza SzkoĹa Filologiczna we WrocĹawiu'),
(335, 'WyĹźsza SzkoĹa Filologii Hebrajskiej w Toruniu'),
(337, 'WyĹźsza SzkoĹa FinansĂłw i Informatyki im. prof. Janusza ChechliĹskiego w Ĺodzi'),
(338, 'WyĹźsza SzkoĹa FinansĂłw i Prawa w Bielsku-BiaĹej'),
(339, 'WyĹźsza SzkoĹa FinansĂłw i ZarzÄdzania w BiaĹymstoku'),
(340, 'WyĹźsza SzkoĹa FinansĂłw i ZarzÄdzania w Warszawie'),
(342, 'WyĹźsza SzkoĹa Fizjoterapii we WrocĹawiu'),
(344, 'WyĹźsza SzkoĹa Gospodarki Euroregionalnej im. Alcide De Gasperi w JĂłzefowie'),
(345, 'WyĹźsza SzkoĹa Gospodarki i ZarzÄdzania w Krakowie'),
(346, 'WyĹźsza SzkoĹa Gospodarki Krajowej w Kutnie'),
(348, 'WyĹźsza SzkoĹa Gospodarki w Bydgoszczy'),
(349, 'WyĹźsza SzkoĹa Gospodarowania NieruchomoĹciami w Warszawie'),
(351, 'WyĹźsza SzkoĹa Handlowa im. KrĂłla Stefana Batorego w Piotrkowie Trybunalskim'),
(353, 'WyĹźsza SzkoĹa Handlowa w Radomiu'),
(354, 'WyĹźsza SzkoĹa Handlowa we WrocĹawiu'),
(357, 'WyĹźsza SzkoĹa Handlu i UsĹug w Poznaniu'),
(358, 'WyĹźsza SzkoĹa Hotelarstwa i Gastronomii w Poznaniu'),
(359, 'WyĹźsza SzkoĹa Hotelarstwa i Turystyki w CzÄstochowie'),
(361, 'WyĹźsza SzkoĹa Humanistyczna im. KrĂłla StanisĹawa LeszczyĹskiego w Lesznie'),
(362, 'WyĹźsza SzkoĹa Humanistyczna TWP w Szczecinie'),
(365, 'WyĹźsza SzkoĹa Humanistyczna we WrocĹawiu'),
(366, 'WyĹźsza SzkoĹa Humanistyczno-Ekonomiczna im. J. Zamoyskiego w ZamoĹciu'),
(367, 'WyĹźsza SzkoĹa Humanistyczno-Ekonomiczna w Brzegu'),
(370, 'WyĹźsza SzkoĹa Humanistyczno-Przyrodnicza Studium Generale Sandomiriense w Sandomierzu'),
(371, 'WyĹźsza SzkoĹa Humanitas w Sosnowcu'),
(372, 'WyĹźsza SzkoĹa Informatyki i Ekonomii TWP w Olsztynie'),
(373, 'WyĹźsza SzkoĹa Informatyki i UmiejÄtnoĹci z siedzibÄ w Ĺodzi'),
(374, 'WyĹźsza SzkoĹa Informatyki i ZarzÄdzania "COPERNICUS" we WrocĹawiu'),
(375, 'WyĹźsza SzkoĹa Informatyki i ZarzÄdzania im. Prof. Tadeusza KotarbiĹskiego z siedzibÄ w Olsztynie'),
(376, 'WyĹźsza SzkoĹa Informatyki i ZarzÄdzania w Bielsku-BiaĹej'),
(377, 'WyĹźsza SzkoĹa Informatyki i ZarzÄdzania w PrzemyĹlu'),
(378, 'WyĹźsza SzkoĹa Informatyki i ZarzÄdzania z siedzibÄ w Rzeszowie'),
(379, 'WyĹźsza SzkoĹa Informatyki Stosowanej i ZarzÄdzania w Warszawie'),
(380, 'WyĹźsza SzkoĹa Informatyki, ZarzÄdzania i Administracji w Warszawie'),
(382, 'WyĹźsza SzkoĹa Integracji Europejskiej w Szczecinie'),
(383, 'WyĹźsza SzkoĹa InĹźynierii BezpieczeĹstwa i Ekologii w Sosnowcu'),
(385, 'WyĹźsza SzkoĹa InĹźynierii Gospodarki w SĹupsku'),
(386, 'WyĹźsza SzkoĹa InĹźynierii i Zdrowia w Warszawie'),
(388, 'WyĹźsza SzkoĹa InĹźynieryjno-Ekonomiczna w Rzeszowie'),
(390, 'WyĹźsza SzkoĹa JÄzykĂłw Obcych im. Samuela BogumiĹa Lindego z siedzibÄ w Poznaniu'),
(392, 'WyĹźsza SzkoĹa Kadr MenedĹźerskich w Koninie z siedzibÄ w Koninie'),
(393, 'WyĹźsza SzkoĹa Komunikacji i ZarzÄdzania w Poznaniu'),
(394, 'WyĹźsza SzkoĹa Komunikacji SpoĹecznej w Gdyni'),
(396, 'WyĹźsza SzkoĹa Komunikowania, Politologii i StosunkĂłw MiÄdzynarodowych w Warszawie'),
(398, 'WyĹźsza SzkoĹa Kosmetyki i Nauk o Zdrowiu w Ĺodzi'),
(399, 'WyĹźsza SzkoĹa Kultury Fizycznej i Turystyki im. Haliny Konopackiej w Pruszkowie'),
(400, 'WyĹźsza SzkoĹa Kultury SpoĹecznej i Medialnej w Toruniu'),
(402, 'WyĹźsza SzkoĹa Lingwistyczna w CzÄstochowie'),
(403, 'WyĹźsza SzkoĹa Logistyki w Poznaniu'),
(406, 'WyĹźsza SzkoĹa Marketingu i ZarzÄdzania w Lesznie'),
(408, 'WyĹźsza SzkoĹa Mazowiecka w Warszawie'),
(409, 'WyĹźsza SzkoĹa Mechatroniki w Katowicach'),
(410, 'WyĹźsza SzkoĹa Medyczna w KĹodzku'),
(411, 'WyĹźsza SzkoĹa Medyczna w Legnicy z siedzibÄ w Legnicy'),
(412, 'WyĹźsza SzkoĹa Medyczna w Sosnowcu'),
(413, 'WyĹźsza SzkoĹa Medyczna z siedzibÄ w BiaĹymstoku'),
(414, 'WyĹźsza SzkoĹa MenedĹźerska w BiaĹymstoku'),
(417, 'WyĹźsza SzkoĹa MenedĹźerska w Warszawie'),
(420, 'WyĹźsza SzkoĹa Nauk o Zdrowiu w Bydgoszczy'),
(421, 'WyĹźsza SzkoĹa Nauk Prawnych i Administracji im. Leona PetraĹźyckiego z siedzibÄ w WoĹominie'),
(423, 'WyĹźsza SzkoĹa Nauk SpoĹecznych z siedzibÄ w Lublinie'),
(424, 'WyĹźsza SzkoĹa Nauk Stosowanych w Rudzie ĹlÄskiej'),
(427, 'WyĹźsza SzkoĹa Oficerska SiĹ Powietrznych'),
(428, 'WyĹźsza SzkoĹa Oficerska Wojsk LÄdowych imienia generaĹa Tadeusza KoĹciuszki'),
(430, 'WyĹźsza SzkoĹa Pedagogiczna im. Janusza Korczaka w Warszawie'),
(431, 'WyĹźsza SzkoĹa Pedagogiczna w Ĺodzi'),
(433, 'WyĹźsza SzkoĹa Pedagogiczno-Techniczna w Koninie'),
(434, 'WyĹźsza SzkoĹa Pedagogiki i Administracji im. Mieszka I w Poznaniu'),
(435, 'WyĹźsza SzkoĹa Planowania Strategicznego w DÄbrowie GĂłrniczej'),
(436, 'WyĹźsza SzkoĹa Policji w Szczytnie'),
(437, 'WyĹźsza SzkoĹa Prawa i Administracji Rzeszowska SzkoĹa WyĹźsza z siedzibÄ w Rzeszowie'),
(438, 'WyĹźsza SzkoĹa Prawa im. Heleny Chodkowskiej'),
(439, 'WyĹźsza SzkoĹa Promocji, MediĂłw i Show Businessu z siedzibÄ w Warszawie'),
(440, 'WyĹźsza SzkoĹa PrzedsiÄbiorczoĹci i Administracji w Lublinie'),
(443, 'WyĹźsza SzkoĹa PrzedsiÄbiorczoĹci imienia KsiÄcia Kazimierza Kujawskiego z siedzibÄ w InowrocĹawiu'),
(444, 'WyĹźsza SzkoĹa PrzedsiÄbiorczoĹci w Warszawie z siedzibÄ w Warszawie'),
(445, 'WyĹźsza SzkoĹa Rehabilitacji w Warszawie'),
(448, 'WyĹźsza SzkoĹa SĹuĹźb Lotniczych z siedzibÄ w Bydgoszczy'),
(450, 'WyĹźsza SzkoĹa SpoĹeczno-Ekonomiczna w GdaĹsku z siedzibÄ w GdaĹsku'),
(451, 'WyĹźsza SzkoĹa SpoĹeczno-Ekonomiczna w Warszawie'),
(453, 'WyĹźsza SzkoĹa SpoĹeczno-Gospodarcza z siedzibÄ w Przeworsku'),
(454, 'WyĹźsza SzkoĹa SpoĹeczno-Przyrodnicza im. Wincentego Pola w Lublinie'),
(455, 'WyĹźsza SzkoĹa Sportowa im. Kazimierza GĂłrskiego w Ĺodzi'),
(456, 'WyĹźsza SzkoĹa StosunkĂłw MiÄdzynarodowych i Amerykanistyki w Warszawie'),
(457, 'WyĹźsza SzkoĹa StosunkĂłw MiÄdzynarodowych i Komunikacji SpoĹecznej w CheĹmie'),
(458, 'WyĹźsza SzkoĹa StudiĂłw MiÄdzynarodowych w Ĺodzi'),
(460, 'WyĹźsza SzkoĹa Sztuk Filmowych i Teatralnych z siedzibÄ w Warszawie'),
(462, 'WyĹźsza SzkoĹa Sztuki i Projektowania w Ĺodzi'),
(465, 'WyĹźsza SzkoĹa Techniczna w Katowicach'),
(468, 'WyĹźsza SzkoĹa Techniczno-Ekonomiczna w Szczecinie'),
(470, 'WyĹźsza SzkoĹa Techniczno-Ekonomiczna w Warszawie z siedzibÄ w Warszawie'),
(471, 'WyĹźsza SzkoĹa Techniczno-Humanistyczna - Kadry dla Europy z siedzibÄ w Poznaniu'),
(473, 'WyĹźsza SzkoĹa Technik Komputerowych i Telekomunikacji w Kielcach'),
(474, 'WyĹźsza SzkoĹa Technologii Informatycznych w Katowicach'),
(475, 'WyĹźsza SzkoĹa Technologii Informatycznych w Warszawie'),
(478, 'WyĹźsza SzkoĹa Turystyki i Ekologii w Suchej Beskidzkiej'),
(479, 'WyĹźsza SzkoĹa Turystyki i Hotelarstwa w GdaĹsku'),
(482, 'WyĹźsza SzkoĹa Turystyki i JÄzykĂłw Obcych w Warszawie'),
(484, 'WyĹźsza SzkoĹa UbezpieczeĹ w Krakowie'),
(486, 'WyĹźsza SzkoĹa UmiejÄtnoĹci SpoĹecznych w Poznaniu'),
(487, 'WyĹźsza SzkoĹa UmiejÄtnoĹci Zawodowych w PiĹczowie'),
(488, 'WyĹźsza SzkoĹa Uni-Terra z siedzibÄ w Poznaniu'),
(489, 'WyĹźsza SzkoĹa WspĂłĹpracy MiÄdzynarodowej i Regionalnej im. Zygmunta Glogera z siedzibÄ w WoĹominie'),
(490, 'WyĹźsza SzkoĹa Wychowania Fizycznego i Turystyki w BiaĹymstoku'),
(491, 'WyĹźsza SzkoĹa ZarzÄdzania "Edukacja" we WrocĹawiu'),
(492, 'WyĹźsza SzkoĹa ZarzÄdzania i Administracji w Opolu'),
(493, 'WyĹźsza SzkoĹa ZarzÄdzania i Administracji w ZamoĹciu'),
(494, 'WyĹźsza SzkoĹa ZarzÄdzania i BankowoĹci w Krakowie'),
(495, 'WyĹźsza SzkoĹa ZarzÄdzania i BankowoĹci w Poznaniu'),
(496, 'WyĹźsza SzkoĹa ZarzÄdzania i Coachingu we WrocĹawiu'),
(499, 'WyĹźsza SzkoĹa ZarzÄdzania i Marketingu w Sochaczewie'),
(502, 'WyĹźsza SzkoĹa ZarzÄdzania i PrzedsiÄbiorczoĹci z siedzibÄ w WaĹbrzychu'),
(504, 'WyĹźsza SzkoĹa ZarzÄdzania OchronÄ Pracy w Katowicach'),
(505, 'WyĹźsza SzkoĹa ZarzÄdzania Personelem w Warszawie'),
(506, 'WyĹźsza SzkoĹa ZarzÄdzania Ĺrodowiskiem w Tucholi'),
(507, 'WyĹźsza SzkoĹa ZarzÄdzania w CzÄstochowie'),
(508, 'WyĹźsza SzkoĹa ZarzÄdzania w GdaĹsku'),
(512, 'WyĹźsza SzkoĹa Zawodowa Kosmetyki i PielÄgnacji Zdrowia w Warszawie'),
(513, 'WyĹźsza SzkoĹa Zawodowa ĹĂłdzkiej Korporacji OĹwiatowej w Ĺodzi'),
(514, 'WyĹźsza SzkoĹa Zawodowa Ochrony Zdrowia TWP w ĹomĹźy z siedzibÄ w ĹomĹźy'),
(515, 'WyĹźsza SzkoĹa Zawodowa ?Oeconomicus? Polskiego Towarzystwa Ekonomicznego z siedzibÄ w Szczecinie'),
(516, 'WyĹźsza SzkoĹa Zawodowa z siedzibÄ w Kostrzynie nad OdrÄ'),
(518, 'WyĹźsza SzkoĹa Zdrowia, Urody i Edukacji w Poznaniu'),
(520, 'Zachodniopomorska SzkoĹa Biznesu w Szczecinie'),
(521, 'Zachodniopomorski Uniwersytet Technologiczny w Szczecinie'),
(522, 'WyĹźsza SzkoĹa Bankowa w Bydgoszczy');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

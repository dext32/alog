<?php
if(isset($_SERVER['HTTP_REFERER'])) {
    $back = '<p><a href="' . $_SERVER['HTTP_REFERER'] . '" class="back">Powrót</a></p>';
} else {
    $back = null;
}

$lang['team'][0] = 'Wypełnij wszystkie pola formularza.'.$back;
$lang['team'][1] = 'Zalogowałeś się pomyślnie. Za chwile nastąpi przekierowanie.';
$lang['team'][2] = 'Pomyślnie utworzono drużynę'.$back;
$lang['team'][3] = 'Niepoprawny adres E-mail.'.$back;
$lang['team'][4] = 'Jesteś już w drużynie.'.$back;
$lang['team'][5] = 'Podany E-mail jest już zajęty.'.$back;
$lang['team'][6] = 'Zarejestrowano pomyślnie. Teraz możesz się zalogować.'.$back;
$lang['team'][7] = 'Zaprosiłeś gracza do twojej drużyny.'.$back;
$lang['team'][8] = '';
$lang['team'][9] = '';
$lang['team'][10] = 'nie można zapraszać spoza uczelni';
$lang['team'][11] = '';
$lang['team'][12] = '';
$lang['team'][13] = '';
$lang['team'][14] = '';
$lang['team'][15] = '';
$lang['team'][16] = '';
$lang['team'][17] = '';
$lang['team'][18] = 'udało się dodać użytkownika do teamu';
$lang['team'][19] = 'nie można dodać użytkownika ponieważ jest już w drużynie';
$lang['team'][20] = 'takie zaproszenie nie istnieje';
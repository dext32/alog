<?php
if(isset($_SERVER['HTTP_REFERER'])) {
    $back = '<p><a href="' . $_SERVER['HTTP_REFERER'] . '" class="back">Powrót</a></p>';
} else {
    $back = null;
}

$lang['auth'][0] = 'Wypełnij wszystkie pola formularza.';
$lang['auth'][1] = 'Zalogowałeś się pomyślnie. Za chwile nastąpi przekierowanie.';
$lang['auth'][2] = 'Brak podanego użytkownika w bazie danych.';
$lang['auth'][3] = 'Niepoprawny adres E-mail.';
$lang['auth'][4] = 'Podany login jest już zajety.';
$lang['auth'][5] = 'Podany E-mail jest już zajęty.';
$lang['auth'][6] = 'Zarejestrowano pomyślnie. Teraz możesz się zalogować.';
$lang['auth'][7] = 'Rejestracja zakończyła się niepomyślnie. Spróbuj ponownie za jakiś czas.';
$lang['auth'][8] = ''.$back;
$lang['auth'][9] = ''.$back;
$lang['auth'][10] = ''.$back;
$lang['auth'][11] = ''.$back;
$lang['auth'][12] = ''.$back;
$lang['auth'][13] = 'Rejestracja zakończyła się pomyślnie.'.$back;
$lang['auth'][14] = ''.$back;
$lang['auth'][15] = 'email, haslo i email są wymagane';
$lang['auth'][16] = 'Powtórz hasło poprawnie.';
$lang['auth'][17] = 'Profil zaktualizowano pomyślnie.';
$lang['auth'][18] = 'Operacja resetowania hasła przebiegła pomyślnie. Sprawdź swoją pocztę email.';
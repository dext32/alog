<?php
if(!$session -> exists('id') || !$session -> exists('login') || !($_SESSION['privilege'] === '2') )
{
 return 'Brak uprawnień.';
}

echo '
<div class="container boxes">
<div class="row section-2">
<div class="col-lg-6 col-lg-offset-1">
<h1>Team Manager</h1>
';
$i = '1';

if (isset($leader_kick_member_result) && $leader_kick_member_result == "true") {
    echo '
        <div class="alert alert-success" role="alert">
          Pomyślnie usunięto gracza <b>'.$user->getUserName($_POST['member_id']).'</b> z drużyny <b>'.$team->getTeamName($_POST['team_id']).'</b>
        </div>
    '; } 

foreach($leaderTeams as $teamArray) {
    $j = '1';
	$invList = $team->listInvites($teamArray['id'],$user);
    echo "<table class=\"table\">
			<thead>
				<tr>
					<th scope=\"col\">#</th>
					<th scope=\"col\">Tag</th>
					<th scope=\"col\">Nazwa</th>
					<th scope=\"col\">Gra</th>
					<th scope=\"col\" colspan=\"2\">Liga</th>
				</tr>
			</thead>
            <tbody>";
	echo "<tr>
		<th scope=\"row\">".$i++."</th>
		<td>".$teamArray['tag']."</td>
		<td>".$teamArray['name']."</td>
		<td>".$games->getGameName($teamArray['game'])."</td>
		<td colspan=\"2\">"; 
		if(empty($teamArray['league']))
		{ 
			echo '<form action="index.php" method="GET">
				<input type="text" name="page" value="setTeamLeague" style="display:none" readonly>
				<input type="text" name="team" value="'.$teamArray['id'].'" style="display:none" readonly>
				  <select name="league" class="form-control input-sm" placeholder="Wybierz ligę" >';
					$leagueList = $team->getAvaliableTeamLeagues(['game' => $teamArray['game']]);
					foreach($leagueList as $leagueArray) {
						echo "<option value=".$leagueArray['id'].">".$leagueArray['name']."</option>";
					}
			echo '</select>
				<input type="submit" value="Zapisz do ligii" class="btn btn-info btn-block" ></form>';
		} else {
			echo $league->getLeagueName($teamArray['league']);
		}
		echo "</td>
	</tr>";
    
    echo "<thead>
				<tr>
					<th scope=\"col\">#</th>
					<th scope=\"col\">Login</th>
					<th scope=\"col\">Imię</th>
					<th scope=\"col\">Nazwisko</th>
					<th scope=\"col\">Kontakt</th>
                    <th scope=\"col\">Usuń</th>
				</tr>
			</thead>";
    $teamMembers = $team->getTeamMembers($teamArray['id']);
    foreach($teamMembers as $teamMember){
        echo "<tr>
            <th scope=\"row\">".$j++."</th>
            <td>".$teamMember['login']."</td>
            <td>".$teamMember['name']."</td>
            <td>".$teamMember['surname']."</td>
            <td><a href=\"mailto:".$teamMember['email']."\"><img src=\"http://www.clker.com/cliparts/6/k/H/g/d/8/email-icon-md.png\" style='width:25px;height:18px;'></a></td>
            <td><form action=\"index.php?page=leader&a=delete_member\" method=\"post\"><input type=\"text\" value=\"".$teamMember['id']."\" name=\"member_id\" style=\"display:none;\" readonly><input type=\"text\" value=\"".$teamArray['id']."\" name=\"team_id\" style=\"display:none;\" readonly><input type=\"image\" name=\"submit\" src=\"images/icons/delete.png\" border=\"0\" alt=\"Usuń\" width=\"15\"/></form></td>
        </tr>";
    }
	echo "<tr><th colspan='6' scope=\"row\">Prośby o dodanie</th></tr>";
		foreach($invList as $invite){
			if($invite['type'] == '2'){
                $userDataTemp = $user->getUserAllData($invite['from'],$user->getUserName($invite['from']));
				echo "<td colspan='6'><span style=\"float:left;\">".$user->getUserName($invite['from']).", ".$userDataTemp["name"]." ".$userDataTemp["surname"]."</span>";
                echo '<form action="index.php?page=leader" method="POST">
                    <input type="text" name="addUserToTeam" value="true" style="display:none" readonly>
                    <input type="text" name="user" value="'.$invite['from'].'" style="display:none" readonly>
                    <input type="text" name="team" value="'.$invite['teamid'].'" style="display:none" readonly>
                    <input type="text" name="type" value="'.$invite['type'].'" style="display:none" readonly>
                <input type="submit" value="&#10003;" class="btn btn-info" style="float:left; padding: 0px 5px !important; margin-left:5px;"></form>';
                echo '<form action="index.php?page=leader" method="POST">
                    <input type="text" name="removeInvite" value="true" style="display:none" readonly>
                    <input type="text" name="user" value="'.$invite['from'].'" style="display:none" readonly>
                    <input type="text" name="team" value="'.$invite['teamid'].'" style="display:none" readonly>
                    <input type="text" name="type" value="'.$invite['type'].'" style="display:none" readonly>
                <input type="submit" value="&#10005;" class="btn btn-info" style="float:left; padding: 0px 5px !important; margin-left:5px;"></form>';
                echo "</td></tr>";
			}
		}
	echo "<tr><th colspan='6' scope=\"row\">Propozycje dodania</th></tr>";
		foreach($invList as $invite){
			if($invite['type'] == '1'){
                $userDataTemp = $user->getUserAllData($invite['to'],$user->getUserName($invite['to']));
				echo "<td colspan='6'><span style=\"float:left;\">".$user->getUserName($invite['to']).", ".$userDataTemp["name"]." ".$userDataTemp["surname"]."</span>";
                echo '<form action="index.php?page=leader" method="POST">
                    <input type="text" name="addUserToTeam" value="true" style="display:none" readonly>
                    <input type="text" name="user" value="'.$invite['to'].'" style="display:none" readonly>
                    <input type="text" name="team" value="'.$invite['teamid'].'" style="display:none" readonly>
                    <input type="text" name="type" value="'.$invite['type'].'" style="display:none" readonly>
                <input type="submit" value="&#10003;" class="btn btn-info" style="float:left; padding: 0px 5px !important; margin-left:5px;"></form>';
                echo '<form action="index.php?page=leader" method="POST">
                    <input type="text" name="removeInvite" value="true" style="display:none" readonly>
                    <input type="text" name="user" value="'.$invite['to'].'" style="display:none" readonly>
                    <input type="text" name="team" value="'.$invite['teamid'].'" style="display:none" readonly>
                    <input type="text" name="type" value="'.$invite['type'].'" style="display:none" readonly>
                <input type="submit" value="&#10005;" class="btn btn-info" style="float:left; padding: 0px 5px !important; margin-left:5px;"></form>';
                echo "</td></tr>";
			}
		}
	echo "</td></tbody></table>";
}
echo '
</div>
'; ?>
<div class="col-lg-4">
<h3>Nowa drużyna</h3>
	<div id="player">
		<div class="teamname">
		<form action="index.php" method="GET">
			<input type="text" name="page" value="add_team" style="display:none" readonly>
			<div class="form-group">
				<input type="text" name="tag" class="form-control input-sm" placeholder="Tag drużyny">
			</div>
			<div class="form-group">
				<input type="text" name="name" class="form-control input-sm" placeholder="Nazwa drużyny">
			</div>
			<div class="form-group">
				<select name="game" class="form-control input-sm">
                <?php 
					foreach($gameList as $gameArray) {
						echo "<option value=".$gameArray['id'].">".$gameArray['name']."</option>";
					}
                ?>
			</select>
			</div>
			<input type="submit" value="Dodaj drużynę" class="btn btn-info btn-block">
		</form>
		</div>
    <div style="clear:both"></div>
</div>

    <div>
        <h1>Zmiana avatarów</h1>
        <img src="<?php echo $team->getTeamAvatar($leaderTeams[0]['id']); ?>" id="avatar_team" style="margin-left:auto;margin-right:auto;display:block;" class="img-responsive">
        <?php if($avatarResult) {echo $avatarResult;} ?>
        <form action="index.php?page=leader" method="post" enctype="multipart/form-data">
            <select name="teamid" id="avatar_team_id" style="width:100%;margin-top:5px;" onchange="displayAvatar()" required>
            <?php 
                foreach($leaderTeams as $teamData) {
                    echo '<option value="'.$teamData['id'].'">'.$teamData['name'].' ('.$games->getGameName($teamData['game']).')</option>';
                }
            ?>
            </select>
            <input class="select-file" name="fileToUpload" id="fileToUpload" type="file" style="margin:5px 0px;">
            <input class="form-control" name="avatar_upload" value="Załaduj avatar" type="submit">
        </form> 
    </div>
</div>

</div>
</div>

<script>
    function ImageExists(ext){
        var http = new XMLHttpRequest();
        http.open('HEAD', "uploads/avatar-team-"+ext, false);
        http.send();
        if (http.status!=404) {
            return true;
        } else {
            return false;
        }
    }
    function displayAvatar(){
        var e = document.getElementById("avatar_team_id");
        var strUser = e.options[e.selectedIndex].value;
        var img_ext = [strUser+".jpg",strUser+".jpeg",strUser+".png"];
        for(var i = 0; i < img_ext.length; i++) {
            if ( ImageExists(img_ext[i]) ) {
                document.getElementById("avatar_team").src = "uploads/avatar-team-"+img_ext[i];
                break;
            } else {
                document.getElementById("avatar_team").src = "images/avatar-default.png";
            }
        }
    }
</script>
    
    


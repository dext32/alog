<main>
		<h1>Najbliższe spotkania</h1>

		<div class="games">

			<figure>
				<a href="?page=pick_game&id=1"><img src="img/cs.jpg" class="img-responsive" alt=""></a>
				<figcaption>Counter Strike: Global Offensive</figcaption>
			</figure>

		</div>

		<div class="games">

			<figure>
				<a href="?page=pick_game&id=2"><img src="img/sc2.jpg" class="img-responsive" alt=""></a>
				<figcaption>StarCraft II: Legacy of the Void</figcaption>
			</figure>

		</div>

		<div class="games">

			<figure>
				<a href="?page=pick_game&id=3"><img src="img/hs.jpg" class="img-responsive" alt=""></a>
				<figcaption>Hearthstone</figcaption>
			</figure>

		</div>

		<div class="games">

			<figure>
				<a href="?page=pick_game&id=4"><img src="img/ow.jpg" class="img-responsive" alt=""></a>
				<figcaption>Overwatch</figcaption>
			</figure>

		</div>

		<div class="games">

			<figure>
				<a href="?page=pick_game&id=5"><img src="img/gwint.jpg" class="img-responsive" alt=""></a>
				<figcaption>Gwint: Wiedźmińska Gra Karciana</figcaption>
			</figure>

		</div>

		<div class="games">

			<figure>
				<a href="?page=pick_game&id=6"><img src="img/lol.png" class="img-responsive" alt=""></a>
				<figcaption>Leauge of Legends</figcaption>
			</figure>

		</div>
</main>
</div>
</div>
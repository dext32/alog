<?php
/*if(!$session -> exists('id') || !$session -> exists('login'))
{
 echo 'Musisz być zalogowany żeby uruchomić kolejkę.';
 return false;
}*/
    $allLeagueList = $league->getLeagueList($_GET['id']);
?>	

<div class="container boxes">
		<div class="row section-1">
			<div class="col-lg-4 thumb-img">
				<img src="images/templates/0<?php echo $gameData['id']; ?>.jpg" alt="<?php echo $games->getGameName($gameData['id']); ?>" class="img-responsive">
			</div>
			<div class="col-lg-4">
				<h1>TEAM PLACE</h1>
				<div class="rangs clearfix">
					<div class="team-image left">
						<div class="sq"><h1><?php echo ($leagueScoresMyTeam["place"]==null)?$leagueScores[0]["place"]:$leagueScoresMyTeam["place"]; ?></h1></div>
					</div>
					<div class="team-rangs left">
						<h3><?php echo ($leagueScoresMyTeam['id']==null)?$leagueScores[0]["tag"]:$leagueScoresMyTeam["tag"]; ?></h3>
						<p>Reprezentacja uczelni - <?php echo ($leagueScoresMyTeam["id"]==null)?$team->getUniversityName($leagueScores[0]["id"]):$team->getUniversityName($leagueScoresMyTeam["id"]); ?></p>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="meeting">
					<div class="meeting-date left">
                        <?php
                            $closeMeet = null;
                            $firstCloseMeet = null;
                            foreach ($allLeagueList as $leagueKeys){
                                if( $leagueKeys['id'] == $teamData['league'] ){
                                    $closeMeet = $league->getLeagueMeets($teamData['league']);
                                }
                            }
                            if ( is_null($closeMeet) ) { $closeMeet = $league->getLeagueMeets($defaultLeague['id']); }                
                            if ($session->exists("id") && isset($userData['team']) ){
                                foreach ($closeMeet as $closestMeet){
                                    if ( $closestMeet['time_to_sort'] > 0 && $closestMeet['team1']==$teamData['id'] || $closestMeet['team2']==$teamData['id']) {
                                        if (empty($firstCloseMeet)) { $firstCloseMeet = $closestMeet; }
                                    }
                                }
                            } else {
                                foreach ($closeMeet as $closestMeet){
                                    if ( $closestMeet['time_to_sort'] > 0) {
                                        if (empty($firstCloseMeet)) { $firstCloseMeet = $closestMeet; }
                                    }
                                }
                            }
                        ?>
						<h2>NAJBLIŻSZE SPOTKANIE</h2>
						<p><?php echo (empty($firstCloseMeet))?"BRAK ZAPLANOWANEGO SPOTKANIA":date_format(date_create($firstCloseMeet["date"]),"d-m-Y H : i")." (".substr($team->getTeamName($firstCloseMeet["team1"]), 0, 40)." vs. ".substr($team->getTeamName($firstCloseMeet["team2"]), 0, 40).")"; ?></p>
					</div>
					<div class="start-game right">
                        <?php 
                            $queue = new Queue($pdo);
                            $queueData = $queue->findQueueData($teamData['id']);
                            $lobbyData = Lobby::getLobbyDataById($pdo, $queueData['id']);
                            if ($_GET['id'] == $teamData['game']) {
                                if ( (isset($userData['lobby']) || $queueData['createdBy'] == $userData['id']) && !empty($lobbyData) ) { 
                                    $session->set(["lobby" => $userData['lobby']]);
                                    if (is_null($userData['lobby'])) $session->set(["lobby" => $queueData['id']]);
                                    echo '<a href="?page=lobby">
                                        <button type="button">Powrót do lobby</button>
                                    </a>
                                    ';
                                } else {
                                    echo '<a href="#" data-toggle="modal" data-target="#runQueue" data-whatever="@queue">
                                        <button '; if($session -> exists('id')) { echo 'onclick="iniRequest(\'queue-process\',\'queue\')"'; } echo ' type="button">GRAJ</button>
                                    </a>
                                    ';
                                }
                            }
                        ?>

                    </div>
				</div>
			</div>
			<div class="col-lg-4">
				<h1>RANKING ( <?php $currentLeague = $league->getLeagueData($league->id); echo $currentLeague['name']; ?> )</h1>
				<table class="table extend-table link-black" style="margin-bottom: 0px !important;">
					<thead>
						<tr>
							<th>Poz.</th>
							<th>Drużyna</th>
							<th>Punkty</th>
						</tr>
						<tbody>
                            <?php 
                            if ($teamData['game'] != $_GET['id']){
                                $leagueScoresMini = $league->getLeagueScoresMini(null,5);
                                foreach($leagueScoresMini as $leagueTeam){
                                    if (!is_null($leagueTeam)) echo '
                                    <tr>
                                        <td style="width: 55px;">'.$leagueTeam["place"].'</td>
                                        <td><a href="'.$team->getTeamLink($leagueTeam['id']).'">'.$leagueTeam["tag"].'</a></td>
                                        <td>'.$leagueTeam["points"].'</td>
                                    </tr>'; 
                                }
                            } else {
                                echo '
                                <tr style="font-weight: bold;">
                                    <td style="width: 55px;">'.$leagueScoresMyTeam["place"].'</td>
                                    <td><a href="'.$team->getTeamLink($leagueScoresMyTeam["id"]).'">'.$leagueScoresMyTeam["tag"].'</a></td>
                                    <td>'.$leagueScoresMyTeam["points"].'</td>
                                </tr>
                                <tr>
                                    <td>...</td>
                                    <td>...</td>
                                    <td>...</td>
                                </tr>';
                                foreach($leagueScoresMini as $leagueTeam){
                                    echo '
                                    <tr>
                                        <td>'.$leagueTeam["place"].'</td>
                                        <td><a href="'.$team->getTeamLink($leagueTeam['id']).'">'.$leagueTeam["tag"].'</a></td>
                                        <td>'.$leagueTeam["points"].'</td>
                                    </tr>'; 
                                }
                            }
                            ?>
						</tbody>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div class="container boxes">
		<div class="row section-2">
			<div class="col-lg-6 col-lg-offset-1">
                <h1>TURNIEJE</h1>
				<table class="table table-tournaments">
					<tbody>
                        <?php
                            $leagueList = $league->getLeagueList($_GET['id']);
                            for ( $i = 0; $i < 5; $i++){
                                if ( empty($leagueList[$i]) ) { }
                                else {
                                    echo "
                                        <tr>
                                            <td>
                                                <h3><a href=\"index.php?page=league&ind=".$leagueList[$i]['id']."\" style=\"color:red;\">OTWÓRZ</a></h3>
                                                <p>NUMER: ".$leagueList[$i]['id']."</p>
                                            </td>
                                            <td>
                                                <p>".$leagueList[$i]['name']."</p>
                                                <p>Semester League</p>
                                            </td>
                                            <td>
                                                <p>5 Nagrody</p>
                                            </td>
                                            <td>
                                                <p>".$league->getLeagueTeamsNumber($leagueList[$i]['id'])."</p>
                                            </td>
                                        </tr>
                                    ";
                                }
                            }
                        
                        ?>
						<tr class="clickable" id="69">
							<td colspan="4">
								<h2>POKAŻ WSZYSTKIE TURNIEJE</h2>
								<img src="images/arrow-down.png" alt="">
							</td>
						</tr>
                        <?php 
                            $allLeagueList = $league->getLeagueList($_GET['id']);
                            foreach ($allLeagueList as $leagueKeys){
                                echo "<tr class=\"collapse out budgets 69collapsed\">
                                        <td>
                                            <h3><a href=\"index.php?page=league&ind=".$leagueKeys['id']."\" style=\"color:red;\">OTWÓRZ</a></h3>
                                            <p>NUMER: ".$leagueKeys['id']."</p>
                                        </td>
                                        <td>
                                            <p>".$leagueKeys['name']."</p>
                                            <p>Semester League</p>
                                        </td>
                                        <td>
                                            <p>5 Nagrody</p>
                                        </td>
                                        <td>
                                            <p>".$league->getLeagueTeamsNumber($leagueKeys['id'])."</p>
                                        </td>
                                    </tr>                      
                                ";
                            }
                        ?>
					</tbody>
				</table>
			</div>
			<div class="col-lg-4">
				<h1>PULA NAGRÓD</h1>
				<div class="awards">
					<a role="button" tabindex="0" data-container="body" data-toggle="popover" data-placement="left" title="Myszka Razer" data-content="Wartość 259zł"><img src="images/awards/01.jpg" alt="" class="img-responsive" ></a>
					<a role="button" tabindex="0" data-container="body" data-toggle="popover" data-placement="left" title="Klawiatura Razer" data-content="Wartość 200zł"><img src="images/awards/02.jpg" alt="" class="img-responsive"></a>
					<a role="button" tabindex="0" data-container="body" data-toggle="popover" data-placement="left" title="Słuchawki Razer" data-content="Wartość 159zł"><img src="images/awards/03.jpg" alt="" class="img-responsive"></a>
					<a role="button" tabindex="0" data-container="body" data-toggle="popover" data-placement="left" title="Opaska Razer" data-content="Wartość 59zł"><img src="images/awards/04.jpg" alt="" class="img-responsive"></a>
				</div>
			</div>
		</div>
	</div>

    <div class="container boxes">
		<div class="row section-2">
            <div class="col-lg-12">
                <h1 style="text-align:center;">NAJBLIŻSZE SPOTKANIA</h1>
                <table class="table table-tournaments">
                    <tbody>
                        <?php 
                            $closeMets = null;
                            $inc_top5_mets = 1;
                            foreach ($allLeagueList as $leagueDataKeys){
                                if( $leagueDataKeys['id'] == $teamData['league'] ){
                                    $closeMets = $league->getLeagueMeets($teamData['league']);
                                }
                            }
                            if ( is_null($closeMets) ) { $closeMets = $league->getLeagueMeets($defaultLeague['id']); }                
                            if ( !empty($closeMets) ) {
                                foreach($closeMets as $meetDataKeys) {
                                    if ( $meetDataKeys['time_to_sort'] > 0 && $inc_top5_mets <= 5) {
                                        echo '
                                            <tr style="text-align:center;">
                                                <td style="width:30%;">
                                                    <h3><a href="'.$team->getTeamLink($meetDataKeys['team1']).'" style="color:black;">'.$team->getTeamName($meetDataKeys['team1']).'</a></h3>
                                                </td>
                                                <td style="width:10%;">
                                                    <p><img src="'.$team->getTeamAvatar($meetDataKeys['team1']).'" class="img-responsive" style="max-height:60px;"></p>
                                                </td>
                                                <td style="width:20%;">
                                                    <p>'.$league->getLeagueName($meetDataKeys['league']).'</p>
                                                    <h1>vs.</h1>
                                                    <p>
                                                        Termin : '.date_format(date_create($meetDataKeys['date']),"d.m.Y H : i").'<br>
                                                        Pozostało : '.$meetDataKeys['time_left'].'
                                                    </p>
                                                </td>
                                                <td style="width:10%;">
                                                    <p><img src="'.$team->getTeamAvatar($meetDataKeys['team2']).'" class="img-responsive" style="max-height:60px;"></p>
                                                </td>
                                                <td style="width:30%;">
                                                    <h3><a href="'.$team->getTeamLink($meetDataKeys['team2']).'" style="color:black;">'.$team->getTeamName($meetDataKeys['team2']).'</a></h3>
                                                </td>
                                            </tr>
                                        ';
                                        $inc_top5_mets++;
                                    }
                                }
                            } else {
                                echo "<h1>BRAK ZAPLANOWANYCH SPOTKAŃ</h1>";
                            }
                        ?>
                        <tr class="clickable" id="67">
							<td colspan="5">
								<h2>WYŚWIETL WIĘCEJ</h2>
								<img src="images/arrow-down.png" alt="">
							</td>
						</tr>
                    <?php
                        if ( !empty($closeMets) ) {
                            foreach($closeMets as $meetDataKeys) {
                                if ( $meetDataKeys['time_to_sort'] > 0) {
                                    echo '
                                        <tr style="text-align:center;" class="collapse out budgets 67collapsed">
                                            <td style="width:30%;">
                                                <h3><a href="'.$team->getTeamLink($meetDataKeys['team1']).'" style="color:black;">'.$team->getTeamName($meetDataKeys['team1']).'</a></h3>
                                            </td>
                                            <td style="width:10%;">
                                                <p><img src="'.$team->getTeamAvatar($meetDataKeys['team1']).'" class="img-responsive" style="max-height:60px;"></p>
                                            </td>
                                            <td style="width:20%;">
                                                <p>'.$league->getLeagueName($meetDataKeys['league']).'</p>
                                                <h1>vs.</h1>
                                                <p>
                                                    Termin : '.date_format(date_create($meetDataKeys['date']),"d.m.Y H : i").'<br>
                                                    Pozostało : '.$meetDataKeys['time_left'].'
                                                </p>
                                            </td>
                                            <td style="width:10%;">
                                                <p><img src="'.$team->getTeamAvatar($meetDataKeys['team2']).'" class="img-responsive" style="max-height:60px;"></p>
                                            </td>
                                            <td style="width:30%;">
                                                <h3><a href="'.$team->getTeamLink($meetDataKeys['team2']).'" style="color:black;">'.$team->getTeamName($meetDataKeys['team2']).'</a></h3>
                                            </td>
                                        </tr>
                                    ';
                                }
                            }
                        } else {
                            echo '<tr><td colspan="5" class="collapse out budgets 67collapsed"><h1>BRAK ZAPLANOWANYCH SPOTKAŃ</h1></td></tr>';
                        }  
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

	<div class="container boxes">
		<div class="row section-3">
            <?php
                $closeMeet = null;
                $firstCloseMeet = null;
                foreach ($allLeagueList as $leagueKeys){
                    if( $leagueKeys['id'] == $teamData['league'] ){
                        $closeMeet = $league->getLeagueMeets($teamData['league']);
                    }
                }
                if ( is_null($closeMeet) ) { $closeMeet = $league->getLeagueMeets($defaultLeague['id']); }                
                foreach ($closeMeet as $closestMeet){
                    if ( $closestMeet['time_to_sort'] > 0) {
                        if (empty($firstCloseMeet)) { $firstCloseMeet = $closestMeet; }
                    }
                }
                $meet_team1 = $team->getTeamName($firstCloseMeet['team1']);
                $meet_team2 = $team->getTeamName($firstCloseMeet['team2']);
                echo '
                    <div class="col-lg-3 meeting-info">
                        <div>
                            <h1>NASTĘPNE SPOTKANIE</h1>
                            <h2>'.$meet_team1.' VS '.$meet_team2.'</h2>
                        </div>
                    </div>
                    <div class="col-lg-2 logo-meeting-1">
                        <img src="'.$team->getTeamAvatar($firstCloseMeet['team1']).'" alt="" class="img-responsive" style="max-height:95%;">
                    </div>
                    <div class="col-lg-2 versus">
                        <h1>VS</h1>
                    </div>
                    <div class="col-lg-2 logo-meeting-2">
                        <img src="'.$team->getTeamAvatar($firstCloseMeet['team2']).'" alt="" class="img-responsive" style="max-height:95%;">
                    </div>
                    <a href="https://www.twitch.tv/alogamespl" target="_blank"><div class="col-lg-3 match-saw">
                        <h1>OBEJRZYJ MECZ</h1>
                        <img src="images/arrow.png" alt="" class="img-responsive">
                    </div></a>
                ';
            ?>
			
		</div>
	</div>
<div class="slider">
		<div id="carousel-games" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-games" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-games" data-slide-to="1"></li>
				<li data-target="#carousel-games" data-slide-to="2"></li>
				<li data-target="#carousel-games" data-slide-to="3"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="images/slider/slide_1.jpg" alt="Counter-Strike Global Offensive">
					<div class="carousel-caption">
						<h3>Counter-Strike: Global Offensive</h3>
					</div>
				</div>
				<div class="item">
					<img src="images/slider/slide_2.jpg" alt="Counter-Strike Global Offensive">
					<div class="carousel-caption">
						<h3>Leauge of Legends</h3>
					</div>
				</div>
				<div class="item">
					<img src="images/slider/slide_3.jpg" alt="Counter-Strike Global Offensive">
					<div class="carousel-caption">
						<h3>HearthStone</h3>
					</div>
				</div>
				<div class="item">
					<img src="images/slider/slide_4.jpg" alt="Counter-Strike Global Offensive">
					<div class="carousel-caption">
						<h3>Overwatch</h3>
					</div>
				</div>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-games" role="button" data-slide="prev">
				<img src="images/arrow-left.png">
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-games" role="button" data-slide="next">
				<img src="images/arrow-right.png">
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<div class="boxes main-page">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-lg-offset-4 panters-logo"><img src="images/panters.png" alt="ACADEMIC LEAGUE OF GAMES LOGO" class="img-responsive"></div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-lg-offset-4">
					<h1>ACADEMIC LEAGUE OF GAMES</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-lg-offset-4">
					<h2>ZDOBYWAJ Z NAMI CENNE NAGRODY</h2>
				</div>
			</div>
			<div class="log-button-main">
				<a href="?page=register" class="left"><button type="button" class="btn btn-log" data-dismiss="modal">REJESTRACJA</button></a>
				<p>LUB</p>
				<a href="#" class="right"><button type="button" class="btn btn-back">LOGOWANIE</button></a>
			</div>
		</div>
	</div>
	<div class="boxes image-main">
		<div class="container">
			<div class="row image-margin">
				<div class="col-lg-3 image-margin-hover">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4"><img src="images/icons/01.png" alt="" class="img-responsive"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3>Zaawansowane statystyki</h3>
							<p>Dzięki nim otrzymujesz możliwość śledzenia swoich postępów w grach</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 image-margin-hover">
					<div class="row image-margin-hover">
						<div class="col-lg-4 col-lg-offset-4"><img src="images/icons/02.png" alt="" class="img-responsive"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3>Bezpieczeństwo</h3>
							<p>Korzystając z naszej platformy otrzymujesz dedykowane rozwiązania dla bezpieczeństwa</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 image-margin-hover">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4"><img src="images/icons/03.png" alt="" class="img-responsive"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3>Najciekawsze gry</h3>
							<p>Zapewniamy platformę przeznaczoną dla kilkunastu gier online</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 image-margin-hover">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4"><img src="images/icons/04.png" alt="" class="img-responsive"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3>Cenne nagrody</h3>
							<p>Graj w ulubione gry i zdobywaj cenne nagrody</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
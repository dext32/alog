<?php if($session -> exists('id')) { header("Location: index.php"); } ?>
	<div class="slider">
		<div id="carousel-games" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-games" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-games" data-slide-to="1"></li>
				<li data-target="#carousel-games" data-slide-to="2"></li>
				<li data-target="#carousel-games" data-slide-to="3"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="images/slider/slide_1.jpg" alt="Counter-Strike Global Offensive">
					<div class="carousel-caption">
						<h3>Counter-Strike: Global Offensive</h3>
					</div>
				</div>
				<div class="item">
					<img src="images/slider/slide_2.jpg" alt="Counter-Strike Global Offensive">
					<div class="carousel-caption">
						<h3>Leauge of Legends</h3>
					</div>
				</div>
				<div class="item">
					<img src="images/slider/slide_3.jpg" alt="Counter-Strike Global Offensive">
					<div class="carousel-caption">
						<h3>Hearthstone</h3>
					</div>
				</div>
				<div class="item">
					<img src="images/slider/slide_4.jpg" alt="Counter-Strike Global Offensive">
					<div class="carousel-caption">
						<h3>Overwatch</h3>
					</div>
				</div>
			</div>

			<!-- Controls -->
			<a class="left carousel-control" href="#carousel-games" role="button" data-slide="prev">
				<img src="images/arrow-left.png">
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#carousel-games" role="button" data-slide="next">
				<img src="images/arrow-right.png">
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<div class="boxes">
		<form action="?page=register_action" method="POST">
			<div class="container">
				<div class="row">
                    <?php if(isset($_GET['done'])){
                                if ($_GET['done'] == 'true') {
                                    echo '<div class="col-lg-8 register-process">
                                        <h1>Dziękujemy za rejestrację.</h1>
                                    </div>';
                                } elseif (isset($_GET['alert'])) {
                                    echo '<div class="col-lg-8 register-process">
                                        <h1>'.$_GET['alert'].'</h1>
                                    </div>';
                                }
                            } ?>
					<div class="col-lg-4">
						<div class="register-box-1">
							<h1>REJESTRACJA</h1>
							<input type="text" name="login" pattern="[a-zA-Z0-9]+" placeholder="Login" class="form-control" required>
							<input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Ades e-mail" class="form-control" required>
							<input type="password" name="password" placeholder="Hasło" class="form-control" required>
							<input type="password" name="password_confirmation" placeholder="Powtórz hasło" class="form-control" required>
							<div class="checkbox">
								<label>
									<input type="checkbox" required> Akceptuję <a href="#">regulamin</a>
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" required> Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb niezbędnych do realizacji procesu rejestracji (zgodnie z Ustawą z dnia 29.08.1997 roku o Ochronie Danych Osobowych; tekst jednolity: Dz. U. 2016 r. poz. 922). 
								</label>
							</div>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="register-box-2">
							<div><h1>JESZCZE TYLKO...</h1>
							<input type="text" name="name" pattern="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]+" placeholder="Imię" class="form-control" required>
							<input type="text" name="surname" pattern="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+" placeholder="Nazwisko" class="form-control" required>
							<input type="text" name="adres1" pattern="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]+" placeholder="Ulica" class="form-control" required>
							<input type="text" name="adres2" pattern="[a-zA-Z0-9 /]+" placeholder="Numer budynku / Lokalu" class="form-control" required>
							<input type="text" name="city" pattern="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+" placeholder="Miejscowość" class="form-control" required>
							<input type="text" name="postal" pattern="[0-9-]+" placeholder="Kod pocztowy" class="form-control" required></div>
                            <select name="university" class="form-control">
									<?php
									foreach($universityList as $universityArray) {
										echo "<option value=".$universityArray['id'].">".$universityArray['name']."</option>";
									}
									?>
				            </select>
						</div>
					</div>
					<div class="col-lg-4 image-center">
						<img src="images/panters.png" alt="ACADEMIC LEAGUE OF GAMES LOGO" class="img-responsive">
					</div>
				</div>
				<div class="row register-form">
					<div class="col-lg-8 send-input"><input type="submit" class="form-control" name="send" value="DOŁĄCZAM DO SPOŁECZNOŚCI!"></div>
					<div class="col-lg-4">
						<h1>ACADEMIC LEAGUE OF GAMES</h1>
						<h2>Międzyakademicka Liga Esportowa!</h2>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="boxes image-main">
		<div class="container">
			<div class="row image-margin">
				<div class="col-lg-3 image-margin-hover">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4"><img src="images/icons/01.png" alt="" class="img-responsive"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3>Oficjalne reprezentacje</h3>
							<p>W rozgrywkach biorą udział oficjalne reprezentacje swoich uczelni</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 image-margin-hover">
					<div class="row image-margin-hover">
						<div class="col-lg-4 col-lg-offset-4"><img src="images/icons/02.png" alt="" class="img-responsive"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3>Bezpieczeństwo</h3>
							<p>Dobamy o najwyższy standard klarowności, stabilności i uczciwości w rozgrywkach</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 image-margin-hover">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4"><img src="images/icons/03.png" alt="" class="img-responsive"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3>Najciekawsze gry</h3>
							<p>W Academic League of Games gramy w przewodnie tytuły esportowe!</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 image-margin-hover">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4"><img src="images/icons/04.png" alt="" class="img-responsive"></div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<h3>Cenne nagrody</h3>
							<p>Uczestnicy mają szanse zdobyć cenne nagrody i wyróżnienia!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-arrow"></div>
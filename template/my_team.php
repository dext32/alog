<div class="container boxes" style="margin-bottom:50px;">
    <div class="row section-2">
        <div class="col-lg-11 col-lg-offset-1">
            <h2><?php echo $teamData['name']; ?></h2>
            <div class="col-lg-8">
                <h5><?php echo $teamData['tag']; ?></h5>
                <div id="team_viewer">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Status</th>
                                <th>Nazwa Gracza</th>
                                <th>Imię</th>
                                <th>Nazwisko</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                            $i = '1';
                            foreach($teamMembers as $row) {
                                echo "<tr>
                                    <td>".$i++."</td>
                                    <td><div id=\"isOnline_".$row['id']."\"></td>
                                    <td>".$row['login']."</td>
                                    <td>".$row['name']."</td>
                                    <td>".$row['surname']."</td>
                                </tr>";
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <img src="<?php echo $team->getTeamAvatar($teamData['id']); ?>" style="margin-left:auto;margin-right:auto;display:block;" class="img-responsive">
                <h5>Zaproś do drużyny</h5>
                <form action="index.php" method="GET">
                <div class="register-box-2" style="min-height: 30px; padding:0; padding-left:20px; padding-right:20px; padding-bottom: 5px;">
                    <input type="text" name="page" value="team_invite" style="display:none" readonly>
                    <input type="text" placeholder="Login użytkownika" class="form-control" id="search_user_input" required>
                    <input type="text" name="inviteTo" style="display:none" id="search_user_id" readonly>
                    <div id="search_user_box"></div>
                </div>
                <div class="register-form"><input class="form-control" name="send" value="WYŚLIJ ZAPROSZENIE" type="submit" style="height: 40px;"></div>
                </form>
                <br><br>
                <h4>Otrzymane zaproszenia</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Zaproszony przez</th>
                            <th>Drużyna</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                            foreach($invList as $row) {
                                echo "<tr>
                                    <td>".$user->getUserName($row['from'])."</td>
                                    <td>".$team->getTeamName($row['teamid'])."</td>
                                </tr>";
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>	
    </div>
</div>

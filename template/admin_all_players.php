<?php
if(!$session -> exists('id') || !$session -> exists('login') || !($_SESSION['privilege'] === '0') ) return 'Brak uprawnień.';
?>

<?php
    $allPlayersForAdmin     = $admin -> getAllPlayersArray();
    $allPlayersGameLogins   = $admin -> getAllGameLogins();

?>

<div class="container boxes">
    <div class="row section-1">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col" style="width:15%">Imie</th>
                <th scope="col" style="width:15%">Nickname</th>
              <th scope="col" style="width:15%">Nazwisko</th>
                <th scope="col">Drużyna</th>
            </tr>
          </thead>
          <tbody>
            <tr>
            <?php
                foreach($allPlayersForAdmin as $playerListKeys){
                    $playerTeamData = $team->getDataTeam($playerListKeys['team']);
                    if ($playerTeamData['game'] == core::$config['game']['lol_id']) {
                        echo '
                            <tr>
                              <th scope="row">'.$playerListKeys['name'].'</th>
                              <td>';
                        foreach($allPlayersGameLogins as $playerGameLogin) {
                            if ($playerListKeys['id'] == $playerGameLogin['user'] && $playerGameLogin['game'] == core::$config['game']['lol_id']) echo $playerGameLogin['login']; 
                        }
                        echo '</td>
                              <td>'.$playerListKeys['surname'].'</td>
                              <td>'.$team->getTeamName($playerListKeys['team']).'</td>
                            </tr>
                        ';
                    }
                }    
            ?>
            </tr>
          </tbody>
        </table>
    </div>
</div>
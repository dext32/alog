<div class="container boxes">
    <div class="row section-1">
        <div class="col-lg-2 thumb-img" style="padding:0px; height: 100%;">
            <img src="images/games/0<?php echo $leagueData['game']; ?>.jpg" class="img-responsive">
        </div>
        <div class="col-lg-10">
            <h1 style="margin-bottom:5px;"><?php echo $leagueData['gamename']; ?></h1>
            <h2 style="margin-bottom:20px;"><?php echo $leagueData['name']; ?></h2>
            <?php
                if (!empty($leagueChilds)){
                    echo '<form>
                            <input type="text" style="display:none;" id="search_league_parent" value="'.$_GET['ind'].'" readonly>
                            <input type="text" style="float:left; width:30%; height:22px; background-image: url(\'images/icons/search.png\'); background-repeat: no-repeat; background-position: left; background-size: contain; padding-left:4%;" placeholder="Nazwa ligi" class="form-control" id="search_league_input" required>
                        </form>';
                }
            ?>
        </div>
    </div>
    
    
    <?php
        if (!empty($leagueChilds)){            
            $leagueSubNum=1;
            echo '<div class="row section-2 my-2">
                    <div class="col-lg-12">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th scope="col">No.</th>
                              <th scope="col">ID</th>
                              <th scope="col">Nazwa</th>
                              <th scope="col">Status</th>
                              <th scope="col">Drużyny</th>
                            </tr>
                          </thead>
                          <tbody id="search_league_box">';
                    foreach($leagueChilds as $leagueSub){
                        echo "<tr>
                                <td>
                                    <p># ".$leagueSubNum."</p>
                                </td>
                                <td>
                                    <p>".$leagueSub['id']." # <a href=\"index.php?page=league&ind=".$leagueSub['id']."\" style=\"color:red;\">OTWÓRZ</a></p>
                                </td>
                                <td>
                                    <p>".$leagueSub['name']."</p>
                                    <p>Semester League</p>
                                </td>
                                <td>
                                    <p>".$leagueSub['status']."</p>
                                </td>
                                <td>
                                    <p>".$league->getLeagueTeamsNumber($leagueSub['id'])."</p>
                                </td>
                            </tr>";
                        $leagueSubNum++;
                    }
            echo '      </tbody>
                    </table>
                </div>
            </div>';
            
            
        } else {
            
            echo '<div class="row section-2 my-2">
                    <div class="col-lg-12">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th scope="col">No.</th>
                              <th scope="col">Tag</th>
                              <th scope="col">Nazwa</th>
                              <th scope="col">Uczelnia</th>
                              <th scope="col">Wygrane</th>
                              <th scope="col">Remisy</th>
                              <th scope="col">Przegrane</th>
                              <th scope="col">Punkty</th>
                            </tr>
                          </thead>
                          <tbody>';
                                foreach($leagueScores as $teamScore){
                                    echo '
                                        <tr>
                                          <th scope="row">'.$teamScore['place'].'</th>
                                          <td><a href="'.$team->getTeamLink($teamScore['id']).'" style="color:black;">'.substr($teamScore['tag'], 0, 20).'</a></td>
                                          <td><a href="'.$team->getTeamLink($teamScore['id']).'" style="color:black;">'.substr($team->getTeamName($teamScore['id']), 0, 20).'</a></td>
                                          <td>'.substr($team->getUniversityName($teamScore['id']), 0, 45).'</td>
                                          <td>'.$teamScore['win'].'</td>
                                          <td>'.$teamScore['draw'].'</td>
                                          <td>'.$teamScore['lose'].'</td>
                                          <td>'.$teamScore['points'].'</td>
                                        </tr>
                                    ';
                                }  
            echo '
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>';
   
        }

?>

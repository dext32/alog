<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

if(isset($_GET['page'])) {
    $page = strip_tags($_GET['page']);
} else {
    $page = null;
}

switch($page)
{
    /*
    case 'main':
        require 'main.php';
    break;
    */
    /*case 'register':
		$user = new User($pdo);
		$universityList = $user->getAvaliableUniversity();
        require 'register.php';
    break;*/

    /*case 'login':
        require 'login.php';
    break;*/

    /*case 'user':{
        require 'public_player.php';
    } break;*/

    /*case 'teams':{
        if(isset($_GET['i'])){
            $publicTeamData = $team -> getDataTeam($_GET['i']);
            if ($publicTeamData['league'] != null) {
                $league -> selectLeague($publicTeamData['league']);
                $leagueData = $league -> getLeagueData();

                $leagueDiagram = $league->resultDiagram($team);
                $leagueScores = $league->getLeagueScores();
                $leagueScoresMini = $league->getLeagueScoresMini();
                $leagueScoresMyTeam = $league->getLeagueScoresMini($team);
            }
            require 'public_team.php';
        }
    } break;*/

    /*case 'user_panel':{
		$user   = new User($pdo);
        $team   = new Team($pdo);
        $league = new League($pdo);
		$userData = $user -> getUserAllData();
		$teamData = $team -> getDataTeam($userData['team']);
        $league -> selectLeague($teamData['league']);

        $uniTeams       = $team->getTeamsFromUniversity($userData['university']);

        require 'user_panel.php';
    } break;*/

    /*case 'user_edit':{
		$user   = new User($pdo);
        $team   = new Team($pdo);
		$userData = $user -> getUserAllData();
		$teamData = $team -> getDataTeam($userData['team']);
        if (isset($_POST['user_update'])) {
            $auth = new Auth($pdo,$session);
            $updateResult = $auth -> update($userData['id'],['login' => $_POST['login'], 'password' => $_POST['password'], 'password_confirmation' => $_POST['password_confirmation'], 'email' => $_POST['email'], 'name' => $_POST['name'], 'surname' => $_POST['surname'], 'adress' => $_POST['adress']]);
            echo "<div class='container'><div class=\"alert alert-light\" role=\"alert\">".$updateResult."</div></div>";
        }
        if (isset($_POST['avatar_upload'])) {
            $swapResult = $user->changeUserAvatar($userData['id']);
            if ($swapResult == true || $swapResult == null) {
                if ($_FILES["fileToUpload"]["size"] > 0) {
                    $avatar_name = "avatar-user-".$userData['id'];
                    $avatarResult = Functions::UploadFile($avatar_name);
                }
            }
        }
        if (isset($_POST['user_game_login_update'])){
            if (isset($_POST['hs-battletag']) ) {
                if ($user->getUserGameLogins($userData['id'],core::$config['game']['hs_id'], $_POST['hs-battletag'])) {
                    echo "<div class='container'><div class=\"alert alert-light\" role=\"alert\">Zaktualizowano battle.tag gry Hearthstone</div></div>";
                }
            } elseif ( isset($_POST['lol-login']) ) {
                if ($user->getUserGameLogins($userData['id'],core::$config['game']['lol_id'], $_POST['lol-login'])) {
                    echo "<div class='container'><div class=\"alert alert-light\" role=\"alert\">Zaktualizowano nazwę użytkownika gry League of Legends</div></div>";
                }
            }

        }
        require 'user_edit.php';
    } break;*/

    /*case 'admin':
        $admin = new Admin($pdo,$session);

        if (isset ($_POST['update_lobby'])) {
            $lobby = new Lobby($pdo, $session);
            $lobby -> setConnectionParameter($_POST['id'], array($_POST['ip'],$_POST['pw']));
        }

        if (isset ($_POST['add_new_meet'])) {
            $cpt1 = $admin->checkExistCaptain($_POST['team1']);
            $cpt2 = $admin->checkExistCaptain($_POST['team2']);
            if ($cpt1 && $cpt2) {
                $newMeet_status = $admin -> setNeewMeet($_POST['team1'],$_POST['team2'],$_POST['league'],$_POST['date']);
                if ($newMeet_status) $_GET['new_meet_status'] = "true";
            } else {
                if(!$cpt1) $_GET['error']="cpt1_not_exist";
                if(!$cpt2) $_GET['error1']="cpt2_not_exist";
            }
        }

        if ( isset($_GET['a']) ){
            if ($_GET['a'] == "delete_meet") {
                $deleteResult = $admin -> deleteMeet($_POST['meet_id']);
                $_GET['delete_meet_result'] = $deleteResult;
            }
            if ($_GET['a'] == "lol_players_list") {
                return require 'admin_all_players.php';
            }
        }

        if (isset ($_POST['set_captain'])) {
            if ( $admin -> setTeamCaptain($_POST['captain']) ) $_GET['set_captain_status'] = "true";
        }

        if (isset($_GET['board']) ) {
            if (isset($_POST['match_end'])) {
                $match_end_result = $admin -> setLeagueMeetEnd(array("lobby"=>$_POST['lobby'],"team1"=>$_POST['team1'],"team2"=>$_POST['team2'],"score1"=>$_POST['score1'],"score2"=>$_POST['score2'],"date"=>$_POST['date']));
                if ($match_end_result) {$_GET['status']="match_end_true"; } else { $_GET['status']="match_end_false";}
            }
            require 'admin_board.php';
        } else {
            require 'admin.php';
        }
        break;*/

    /*case 'leader':
        $user   = new User($pdo);
        $team   = new Team($pdo);
        $game   = new Game($pdo,$session);
        $league = new League($pdo);
        $userData     = $user -> getUserAllData();
        $leaderTeams  = $team->getCreatedTeams($userData);
        $gameList     = $game->getGameList();
        if($user -> isUserLeader()){
            if (isset($_POST['addUserToTeam'])) {
                $result = $team -> addToTeam($_POST['user'],$_POST['team'],$_POST['type']);
                echo "<div class=\"alert alert-light\" role=\"alert\">".$result."</div>";
            } elseif (isset($_POST['removeInvite'])) {
                $result = $team -> removeInvite($_POST['user'],$_POST['team'],$_POST['type']);
                echo "<div class=\"alert alert-light\" role=\"alert\">".$result."</div>";
            } elseif (isset($_GET['a'])) {
                if ($_GET['a'] == "delete_member") {
                    $leader_kick_member_result = $team -> kickFromTeam($_POST['team_id'],$_POST['member_id']);
                }
            }
            if (isset($_POST['avatar_upload'])) {
                $swapResult = $team->changeTeamAvatar($_POST['teamid']);
                if ($swapResult == true || $swapResult == null) {
                    if ($_FILES["fileToUpload"]["size"] > 0) {
                        $avatar_name = "avatar-team-".$_POST['teamid'];
                        $avatarResult = Functions::UploadFile($avatar_name);
                    }
                }
            }
            require 'leader.php';
        }
        break;*/

    /*case 'add_team':
        if(isset($_GET['tag']) && isset($_GET['name']))
        {
            $user = new User($pdo);
            $userData = $user -> getUserAllData();
            $team = new Team($pdo);
            $team->addNewTeam($userData,['tag' => $_GET['tag'],'name' => $_GET['name'],'game' => $_GET['game']]);
            header("Refresh:0;URL=index.php?page=leader");
        }
        break;*/

    /*case 'team':{
        $user = new User($pdo);
        $userData = $user -> getUserAllData();
        if($userData['team'] === NULL)
        {
            $team    = new Team($pdo);
            $invList    = $team->listInvites("0", $user);
            $uniTeams   = $team->getTeamsFromUniversity($userData['university']);
            if(isset($_POST['applyToTeam'])){
                $team->setRequestAddToTeam($userData,$_POST['team']);
            }
            if(isset($_POST['acceptInvite'])){
                $team->setFlagAcceptPlToPlInvite($userData,$_POST['team']);
            }
            require 'no_team.php';
        } else {
            $team    = new Team($pdo);
            $teamData    = $team->getDataTeam($userData['team']);
            $teamMembers = $team->getTeamMembers($userData['team']);
            $invList     = $team->listInvites($userData['team'], $user);
            require 'my_team.php';
        }
    } break;

    case 'team_invite':{
        if(isset($_GET['inviteTo']))
        {
            $user = new User($pdo);
            $userData = $user -> getUserAllData();
            $team = new Team($pdo);
            $team->inviteToTeam($userData,$_GET['inviteTo']);
        }
    }break;*/

    /*case 'setTeamLeague':{
        $user = new User($pdo);
        $userData = $user -> getUserAllData();
        $team = new Team($pdo);
        $leaderTeams = $team->getCreatedTeams($userData);
        $league = new League($pdo);
        $setTeamLeague = $league->addTeamToLeague($leaderTeams,$_GET['team'],$_GET['league']);
        require 'leader.php';
    }break;*/

    /*case 'add_game':
        {
            $user = new User($pdo);
            if($user -> isUserAdmin()){
                $game = new Game($pdo,$session);
                $game -> addNewGame(['name' => $_GET['name'], 'max_slots' => $_GET['max_slots']]);
            }
        }
        break;*/

    /*case 'delete_game':
        {
            $user = new User($pdo);
            if($user -> isUserAdmin()){
                $game = new Game($pdo,$session);
                $game -> deleteGame($_GET['id']);
            }
        }
        break;*/

    /*case 'game':
        {
            if(isset($_GET['id'])){
                $games -> setSessionGame($_GET['id']);
                $gameData = $games->getGameData();
                if ($teamData['game'] == $_GET['id']){
                    $league -> selectLeague($teamData['league']);
                } else {
                    $defaultLeague  = $league -> getDefaultLeague($_GET['id']);
                    $league -> selectLeague($defaultLeague['id']);
                }
            } else {
                $defaultGame = $games->getDefaultGame();
                $gameId = (isset($teamData['game'])) ? $teamData['game'] : $defaultGame['id'];
                $defaultLeague  = $league -> getDefaultLeague($gameId);
                $league -> selectLeague($defaultLeague['id']);
                $gameData       = $games->getGameData($gameId);
                $_GET['id'] = $defaultGame['id'];
            }
            $leagueScores = $league->getLeagueScores();
            $leagueScoresMini = $league->getLeagueScoresMini();
            $leagueScoresMyTeam = $league->getLeagueScoresMini($team);
            require "games.php";
        }
        break;*/

    /*case 'lobby':{
        if (isset ($_POST['info_upload'])) {
            if ($_FILES["fileToUpload"]["size"] > 0) {
                $copy_file_name_inc = 1;
                $scrsht_name = $_POST['lobby']."-".$userData['id'];
                //$scrsht_name = basename($_FILES["fileToUpload"]["name"]);
                $scrsht_ext = strtolower(pathinfo($_FILES["fileToUpload"]["name"],PATHINFO_EXTENSION));
                $scrsht_path = $scrsht_name.".".$scrsht_ext;
                while(file_exists("uploads/".$scrsht_path)){
                    $scrsht_name = $scrsht_name."(".$copy_file_name_inc++.")";
                    $scrsht_path = $scrsht_name.".".$scrsht_ext;
                }
                $result = Functions::UploadFile($scrsht_name);
            }
            if (isset($_POST['score1']) && isset($_POST['score2'])) {
                $info = $_POST['team1']." ".$_POST['score1']." : ".$_POST['score2']." ".$_POST['team2'];
                $result .= "<br>Dziękujemy za udostępnienie wyniku.";
                echo "<div class=\"alert alert-light\" role=\"alert\">".$result."</div>";
            }
        }
        require "lobby.php";

        if (isset($_POST['info_upload'])){
            if ($_FILES["fileToUpload"]["size"] > 0) {
                $lobby -> toDbLobbyReport($info, $scrsht_path);
            } else {
                $lobby -> toDbLobbyReport($info);
            }
        }
    } break;*/

    /*case 'league':
        {
            if( isset($_GET['ind']) ){
                $league->selectLeague($_GET['ind']);
                $leagueData   = $league -> getLeagueData();
                $leagueScores = $league -> getLeagueScores();
                $leagueChilds = $league -> getLeagueChilds($_GET['ind']);
                require "league.php";
            } else {
                header("Refresh:0;URL=index.php?page=game");
            }
        }
        break;*/

    /*case 'add_league':
        {
            $user = new User($pdo);
            if($user -> isUserAdmin()){
                $league = new League($pdo,$session);
                $league -> setNewLeague($_GET['name'],$_GET['gameId']);
            }
        }
        break;*/

    /*case 'start_queue':
        {
            $user = new User($pdo);
            $userData = $user -> getUserQueueData();
            $game = new Game($pdo);
            $gameData = $game -> getGameData();
            $queue = new Queue($pdo);
            $queue -> initializeQueue($userData,['name' => $gameData['name'], 'max_slots' => $gameData['max_slots']]);
            //$queue -> initializeQueue($userData,$game);
        }
        break;
     */

    /*case 'register_action':
    {
        $register = new Auth($pdo, $session);
        $result = $register -> register(['login' => $_POST['login'], 'password' => $_POST['password'], 'email' => $_POST['email'], 'name' => $_POST['name'], 'surname' => $_POST['surname'], 'university' => $_POST['university'], 'adress1' => $_POST['adres1'], 'adress2' => $_POST['adres2'], 'city' => $_POST['city'], 'postal' => $_POST['postal']]);
        if (is_array($result) && $result[1] == true) {
            header('Location: index.php?page=register&done=true');
        } else {
            header('Location: index.php?page=register&done=false&alert='.$result);
        }
    }
    break;*/

    /*case 'login_action':
        {
            $login = new Auth($pdo, $session);
            $result = $login -> login(['login' => $_POST['login'], 'password' => $_POST['password']]);

            if(is_array($result) && $result[1] == true)
            {
                echo $result[0];
                header("Refresh:0;URL=index.php");
            } else {
                echo $result;
            }
        }
        break;*/

    /*case 'recovery_pass':
        {
            $recovery = new Auth($pdo, $session);
            $result = $recovery -> recoveryPass($_POST['email']);
            //$result = "Dostępne wkrótce.";
            echo "<div class='container'><div class=\"alert alert-light\" role=\"alert\">".$result."</div></div>";
            require "welcome.php";
        } break;*/

    /*case 'logout':
    {
        $login = new Auth($pdo, $session);
        $login -> logout();
    }
    break;*/
    // /*
    /*case 'tester':
    {
        /*
        //echo var_dump(Functions::dateCompare("2018-01-17T20:45"));
        $league = new League($pdo);
        $league->selectLeague($teamData['league']);
        //$league->setLeagueNewMeet("5","4","1","2018-03-21T20:45");
        echo "<pre>"; print_r($league->getLeagueMeets()); echo "</pre>";
        echo "<pre>"; print_r($league->getLeagueCloseMeet($teamData['id'])); echo "</pre>";
        //echo "<pre>"; print_r($league->getLeagueList()); echo "</pre>";

        //$team->addToTeam("3","5");


        require 'tester.php';
    }break;*/
    // */
    /*default:
        if($session -> exists('id')){
            //header("Refresh:0;URL=index.php?page=game");
            require "welcome.php";
        } else {
            require "welcome.php";
        }
    break;*/
}

<head>
  <link rel="stylesheet" type="text/css" href="css/user_edit_inputs.css" />
</head>
<div class="boxes profiles">
		<div class="container">
			<div class="col-lg-3">
				<div class="image-user">
					<img src="<?php echo $user->getUserAvatar($userData['id']); ?>" alt="" class="img-responsive">
					<div class="user-caption">
						<h2><?php echo $userData['login'] ?></h2>
						<h3><?php if(isset($userData['team'])){ echo $team->getTeamName($userData['team']); } ?></h3>
					</div>
				</div>
				<div class="menu-sidebar">
                    <nav>
						<li>
                            <?php if($avatarResult) {echo $avatarResult;} ?>
                            <br>Zmień avatar
                            <form action="index.php?page=user_edit" method="post" enctype="multipart/form-data">
                                <input class="select-file" name="fileToUpload" id="fileToUpload" type="file">
                                <input class="form-control" name="avatar_upload" value="Załaduj avatar" type="submit">
                            </form> 
                        </li>
                    </nav>
				</div>
            </div>
			<div class="col-lg-9" style="background-color: #fff">
            <form action="?page=user_edit" method="POST">
                <div class="profile-edit-title">
                    <h1>EDYCJA PROFILU</h1>
                </div>
                <div class="main-profile-view">
                    <div class="register-box-1">
                        <div class="col-lg-5">
                            <div class="group">
                                <div class="profile-edit-label">Login</div>
                            </div>
                            <div class="group">
                                <div class="profile-edit-label">Email</div>
                            </div>
                            <div class="group">
                                <div class="profile-edit-label">Hasło</div>
                            </div>
                            <div class="group">
                                <div class="profile-edit-label">Powtórz hasło</div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                          <div class="group">
                            <input type="text" name="login" pattern="[a-zA-Z0-9]+" placeholder="Login" class="form-control" value="<?php echo $userData['login']; ?>" required>
                          </div>
                          <div class="group">    
                            <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Ades e-mail" class="form-control" value="<?php echo $userData['email']; ?>" required>
                          </div>
                          <div class="group">  
                            <input type="password" name="password" placeholder="Hasło" class="form-control" required>
                          </div>
                          <div class="group">      
                            <input type="password" name="password_confirmation" placeholder="Powtórz hasło" class="form-control" required>
                          </div>
                        </div>
                    </div>
                    <div class="register-box-2">
                        <div class="col-lg-5">
                            <div class="group">
                                <div class="profile-edit-label">Imię</div>
                            </div>
                            <div class="group">
                                <div class="profile-edit-label">Nazwisko</div>
                            </div>
                            <div class="group">
                                <div class="profile-edit-label">Adres</div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                          <div class="group">
                            <input type="text" name="name" pattern="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]+" placeholder="Imię" class="form-control" value="<?php echo $userData['name']; ?>" required>
                          </div>
                          <div class="group">      
                            <input type="text" name="surname" pattern="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]+" placeholder="Nazwisko" class="form-control" value="<?php echo $userData['surname']; ?>" required>
                          </div>
                          <div class="group" style="height:60px">      
                              <textarea name="adress" pattern="[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]+" placeholder="Ulica" class="form-control" rows="2" style="height:60px; resize: none" required><?php echo $userData['adress']; ?></textarea>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="profile-edit-button">
                    <input type="submit" class="form-control" name="user_update" value="AKTUALIZUJ PROFIL">
                </div>
            </form>
            </div>
        </div>
        <?php 
            if ($teamData['game'] == core::$config['game']['hs_id']) {
                $hs_battletag = $user -> getUserGameLogins($userData['id'],core::$config['game']['hs_id']);
                echo '
                    <div class="container">
                        <div class="col-lg-offset-3 col-lg-9" style="background-color: #fff; margin-top:15px;padding:0px;">
                            <div class="col-lg-4 thumb-img" style="padding:0px;">
                                <img src="images/games/0'.core::$config['game']['hs_id'].'.jpg" alt="Hearthstone" class="img-responsive">
                            </div>
                            <div class="col-lg-8" >
                                <form action="?page=user_edit" method="POST">
                                    <label for="hs-battletag" style="margin-top:20px;">#battle.tag </label>
                                        <input type="text" name="hs-battletag" value="'.$hs_battletag['login'].'" id="hs-battletag" style="margin-top:20px;">
                                    <input type="submit" class="form-control" name="user_game_login_update" value="AKTUALIZUJ BATTLE.TAG" style="float:bottom;">
                                </form>
                            </div>
                        </div>
                    </div>
                ';
            }
            if ($teamData['game'] == core::$config['game']['lol_id']) {
                $lol_login = $user -> getUserGameLogins($userData['id'],core::$config['game']['lol_id']);
                echo '
                    <div class="container">
                        <div class="col-lg-offset-3 col-lg-9" style="background-color: #fff; margin-top:15px;padding:0px;">
                            <div class="col-lg-4 thumb-img" style="padding:0px;">
                                <img src="images/games/0'.core::$config['game']['lol_id'].'.jpg" alt="League of Legends" class="img-responsive">
                            </div>
                            <div class="col-lg-8" >
                                <form action="?page=user_edit" method="POST">
                                    <label for="lol-login" style="margin-top:20px;">Nazwa gracza </label>
                                        <input type="text" name="lol-login" value="'.$lol_login['login'].'" id="lol-login" style="margin-top:20px;">
                                    <input type="submit" class="form-control" name="user_game_login_update" value="AKTUALIZUJ NAZWĘ" style="float:bottom;">
                                </form>
                            </div>
                        </div>
                    </div>
                ';
            }
        ?>
</div>
<?php
    $user = new User($pdo,$session);
    $universityCoordData = $team->getUniversityCoord($publicTeamData['id']);
    $publicTeamMembers = $team->getTeamMembers($publicTeamData['id']);
?>

<style>
      .pie {
		position:absolute;
		width:90px;
		height:180px;
		overflow:hidden;
		left:150px;
        opacity: 0.5; 
        filter: alpha(opacity=50);
		-moz-transform-origin:left center;
		-ms-transform-origin:left center;
		-o-transform-origin:left center;
		-webkit-transform-origin:left center;
		transform-origin:left center;
	}
    
	.pie-big {
		width:180px;
		height:180px;
		left:60px;
		-moz-transform-origin:center center;
		-ms-transform-origin:center center;
		-o-transform-origin:center center;
		-webkit-transform-origin:center center;
		transform-origin:center center;
	}

	.pie:BEFORE {
		content:"";
		position:absolute;
		width:90px;
		height:180px;
		left:-90px;
		border-radius:90px 0 0 90px;
		-moz-transform-origin:right center;
		-ms-transform-origin:right center;
		-o-transform-origin:right center;
		-webkit-transform-origin:right center;
		transform-origin:right center;
		
	}

	.pie-big:BEFORE {
		left:0px;
	}

	.pie-big:AFTER {
		content:"";
		position:absolute;
		width:90px;
		height:180px;
		left:90px;
		border-radius:0 90px 90px 0;
	}
    
    .pie:hover {
        opacity: 1;
        filter: alpha(opacity=100);
    }

	.pie:nth-of-type(1):BEFORE,
	.pie:nth-of-type(1):AFTER {
		background-color:#43A23D;	
	}
	.pie:nth-of-type(2):AFTER,
	.pie:nth-of-type(2):BEFORE {
		background-color:#D7A11D;	
	}
	.pie:nth-of-type(3):AFTER,
	.pie:nth-of-type(3):BEFORE {
		background-color:#DD4E24;	
	}

<?php 
for ($arrInd = 1; $arrInd <= 3; $arrInd++){
if ($leagueDiagram['data_start'.$arrInd] != 0) {
    echo '  .pie[data-start="'.$leagueDiagram['data_start'.$arrInd].'"] {
            -moz-transform: rotate('.$leagueDiagram['data_start'.$arrInd].'deg); /* Firefox */
            -ms-transform: rotate('.$leagueDiagram['data_start'.$arrInd].'deg); /* IE */
            -webkit-transform: rotate('.$leagueDiagram['data_start'.$arrInd].'deg); /* Safari and Chrome */
            -o-transform: rotate('.$leagueDiagram['data_start'.$arrInd].'deg); /* Opera */
            transform:rotate('.$leagueDiagram['data_start'.$arrInd].'deg);
           }', PHP_EOL;
    }
}
for ($arrInd = 1; $arrInd <= 3; $arrInd++){
if ($leagueDiagram['data_value'.$arrInd] != 0) {
    echo '  .pie[data-value="'.$leagueDiagram['data_value'.$arrInd].'"]:BEFORE {
            -moz-transform: rotate('.$leagueDiagram['data_value'.$arrInd].'deg); /* Firefox */
            -ms-transform: rotate('.$leagueDiagram['data_value'.$arrInd].'deg); /* IE */
            -webkit-transform: rotate('.$leagueDiagram['data_value'.$arrInd].'deg); /* Safari and Chrome */
            -o-transform: rotate('.$leagueDiagram['data_value'.$arrInd].'deg); /* Opera */
            transform:rotate('.$leagueDiagram['data_value'.$arrInd].'deg);
           }', PHP_EOL;
}
}
?>
</style>

<div class="container boxes">
		<div class="row section-1 public-team">
			<div class="col-lg-3 thumb-img">
				<img src="<?php echo $team->getTeamAvatar($publicTeamData['id']); ?>" alt="Team Avatar" class="img-responsive">
			</div>
			<div class="col-lg-9">
				<h1><?php echo $publicTeamData['name']; ?></h1>
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1 mb-2">
						<div class="public-team-container">
							<div class="images-public-team">
								<img src="images/students-cap.png" alt="University" class="img-responsive">
							</div>
							<div class="title-public-team">
								<h2><?php echo $team->getUniversityName($publicTeamData['id']); ?></h2>
							</div>
                            <div class="col-lg-12 col-lg-offset-1" style="clear:both;">
								<img src="images/dot-tree.jpg" width="40px" style="vertical-align:middle;">
                                <span style="vertical-align:middle;">Koordynator : <a href="<?php echo $user->getUserLink($universityCoordData['id']); ?>" style="color:black;"><?php echo $universityCoordData['login']; ?></a></span>
							</div>
						</div>
                        <div class="clearfix"></div>
                        <div class="public-team-container">
							<div class="images-public-team">
								<img src="images/trophy.png" width="40px" alt="League" class="img-responsive">
							</div>
							<div class="title-public-team">
                                <h2><a href="<?php echo $league->getLeagueLink(); ?>" style="color:black;"><?php echo $league->getLeagueName($publicTeamData['league']); ?></a></h2>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container boxes">
		<div class="row section-1 diagrams">
			<div class="col-lg-3 thumb-img">
				<img src="images/templates/0<?php echo $leagueData['game']; ?>.jpg" alt="League" class="img-responsive">
			</div>
			<div class="col-lg-4">
                <div class="mt-3" style="height:75px;">
					<div class="team-image left">
						<div class="sq"><h1><?php echo ($leagueScoresMyTeam["place"]==null)?"#":$leagueScoresMyTeam["place"]; ?></h1></div>
					</div>
					<div class="team-rangs left">
						<h1> #MIEJSCE</h1>
					</div>
				</div>
				<div class="mt-3">
                    <div class="images-public-team ml-3">
                        <img src="images/rating-star.png" alt="Points" class="img-responsive">
                    </div>
                    <h2>PUNKTY LIGOWE: <?php echo ($leagueScoresMyTeam["points"]==null)?"BRAK":$leagueScoresMyTeam["points"]; ?></h2>
                    <div class="images-public-team ml-3">
                        <img src="images/group.png" alt="Team" class="img-responsive">
                    </div>
                    <h2>LICZBA GRACZY: <?php echo count($publicTeamMembers); ?></h2>
                </div>
			</div>
			<div class="col-lg-2">
                <div style="text-align:center;"><h1>STATYSTYKI</h1></div>
                <div class="col-lg-8">
                    <h2>WYGRANE:</h2>
                    <h2>REMISY:</h2>
                    <h2>PRZEGRANE:</h2>
                </div>
                <div class="col-lg-4">
                    <h2><?php echo ($leagueScoresMyTeam["win"]==null)?"BRAK":$leagueScoresMyTeam["win"]; ?></h2>
                    <h2><?php echo ($leagueScoresMyTeam["draw"]==null)?"BRAK":$leagueScoresMyTeam["draw"]; ?></h2>
                    <h2><?php echo ($leagueScoresMyTeam["lose"]==null)?"BRAK":$leagueScoresMyTeam["lose"]; ?></h2>
                </div>
			</div>
			<div class="col-lg-3">
				<div class="mt-4">
					<div class="pie<?php if($leagueDiagram['data_value1']>=180) echo " pie-big"; ?>" data-start="<?php echo $leagueDiagram['data_start1']; ?>" data-value="<?php echo $leagueDiagram['data_value1']; ?>"></div>
                    <div class="pie<?php if($leagueDiagram['data_value2']>=180) echo " pie-big"; ?>" data-start="<?php echo $leagueDiagram['data_start2']; ?>" data-value="<?php echo $leagueDiagram['data_value2']; ?>"></div>
                    <div class="pie<?php if($leagueDiagram['data_value3']>=180) echo " pie-big"; ?>" data-start="<?php echo $leagueDiagram['data_start3']; ?>" data-value="<?php echo $leagueDiagram['data_value3']; ?>"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row public-team-margin-0">
            <?php 
                foreach($publicTeamMembers as $pTeamMember){
                    echo '
                         <div class="col-lg-6 reset-padding boxes">
                            <div class="col-lg-6 thumb-img reset-padding" style="background-color:white; height:225px;">
                                <a href="'.$user->getUserLink($pTeamMember['id']).'"><img src="'.$user->getUserAvatar($pTeamMember['id']).'" alt="User Avatar" class="img-responsive" style="max-height:100%;"></a>
                            </div>
                            <div class="public-team-box col-lg-6">
                                <h2>'.$pTeamMember['name'].' "'.$pTeamMember['login'].'" '.$pTeamMember['surname'].'</h2>
                                <h2>'.$publicTeamData['tag'].'</h2>
                                <p>
                                    <a href="'.$user->getUserLink($pTeamMember['id']).'">Zobacz profil</a>
                                </p>
                            </div>
                        </div>  
                    </a>
                    ';
                }
                
            ?>
		</div>
	</div>

	<div class="container boxes">
		<div class="row section-2">
            <table class="table table-tournaments">
                <tbody>
                    <?php
                        $leagueHistoryLast = $league->getTeamScores($publicTeamData['id']);
                        if ($leagueHistoryLast) {
                            for ($i=0 ; $i<5; $i++){
                                if (isset($leagueHistoryLast[$i])) {
                                    echo '
                                        <tr>
                                            <td style="width:10%;"><img src="'.$team->getTeamAvatar($leagueHistoryLast[$i]['team1']).'" class="img-responsive"></td>
                                            <td style="width:10%;"><h1 style="font-size:36px;">'.$leagueHistoryLast[$i]['score1'].'</h1></td>
                                            <td style="width:25%;">
                                                <div style="text-align:center;">
                                                    <span style="color: '.(($publicTeamData['id']==$leagueHistoryLast[$i]['team1'])?"red":"black").' ;">
                                                    '.$team->getTeamName($leagueHistoryLast[$i]['team1']).'
                                                    </span>
                                                </div>
                                            </td>
                                            <td style="width:10%;"><div style="text-align:center;">vs.</div></td>
                                            <td style="width:25%;">
                                                <div style="text-align:center;">
                                                    <span style="color: '.(($publicTeamData['id']==$leagueHistoryLast[$i]['team2'])?"red":"black").' ;">
                                                    '.$team->getTeamName($leagueHistoryLast[$i]['team2']).'
                                                    </span>
                                                </div>
                                            </td>
                                            <td style="width:10%;"><h1 style="font-size:36px;">'.$leagueHistoryLast[$i]['score2'].'</h1></td>
                                            <td style="width:10%;"><img src="'.$team->getTeamAvatar($leagueHistoryLast[$i]['team2']).'" class="img-responsive"></td>
                                        </tr>
                                    ';
                                }
                            }
                        } else {
                            echo '<td style="width:100%;"><h1>BRAK ROZGRYWEK DO WYŚWIETLENIA</h1></td>';
                        }

                    ?>
                    <tr class="clickable" id="71">
                        <td colspan="7">
                            <h2>POKAŻ WSZYSTKIE ROZGRYWKI</h2>
                            <img src="images/arrow-down.png" alt="">
                        </td>
                    </tr>
                    <?php 
                        foreach ($leagueHistoryLast as $leagueHistoryLong){
                            echo '
                                <tr class="collapse out budgets 71collapsed">
                                    <td style="width:10%;"><img src="'.$team->getTeamAvatar($leagueHistoryLong['team1']).'" class="img-responsive"></td>
                                    <td style="width:10%;"><h1 style="font-size:36px;">'.$leagueHistoryLong['score1'].'</h1></td>
                                    <td style="width:25%;">
                                        <div style="text-align:center;">
                                            <span style="color: '.(($publicTeamData['id']==$leagueHistoryLong['team1'])?"red":"black").' ;">
                                            '.$team->getTeamName($leagueHistoryLong['team1']).'
                                            </span>
                                        </div>
                                    </td>
                                    <td style="width:10%;"><div style="text-align:center;">vs.</div></td>
                                    <td style="width:25%;">
                                        <div style="text-align:center;">
                                            <span style="color: '.(($publicTeamData['id']==$leagueHistoryLong['team2'])?"red":"black").' ;">
                                            '.$team->getTeamName($leagueHistoryLong['team2']).'
                                            </span>
                                        </div>
                                    </td>
                                    <td style="width:10%;"><h1 style="font-size:36px;">'.$leagueHistoryLong['score2'].'</h1></td>
                                    <td style="width:10%;"><img src="'.$team->getTeamAvatar($leagueHistoryLong['team2']).'" class="img-responsive"></td>
                                </tr>                      
                            ';
                        }
                    ?>
                </tbody>
            </table>
		</div>
	</div>
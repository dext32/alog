<!DOCTYPE html>
<html lang="<?php echo core::$config['system']['defaultLang']; ?>">
 <head>
  <meta charset="<?php echo core::$config['system']['charset']; ?>">
  <meta name="description" content="<?php echo core::$config['system']['defaultDescription']; ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo core::$config['system']['defaultTitle']; ?></title>
  <link href="css/styles-off.css" rel="stylesheet">
  <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/formularz.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
  <script type="text/javascript" src="jscripts/jquery.min.js"></script>
  <script type="text/javascript" src="jscripts/main.js"></script>
 </head>
 
 <body>
  <header>
  <!--
  <section class="main-content">

  <div class="left-section">
  <?php //if($session -> exists('id')) echo '	<div id="queue-popup"></div> ';  ?>
  <?php //if($session -> exists('id')) echo '	<div id="online"></div> ';  ?>
   <nav>
    <ul>
	 <li><a href="index.php">Strona główna</a></li>
	 <?php //if(!$session -> exists('id')) echo '<li><a href="?page=register">Rejestracja</a></li>'; ?>
	 <li><a href="?page=login">Logowanie</a></li>
	 <?php //if($session -> exists('id')) echo '<li><a href="?page=user_panel">Panel użytkownika</a></li>';  ?>
	 <?php //if($session -> exists('id')) echo '<li><a href="?page=start_queue">GRAJ</a></li>';  ?>
	 <?php //if($session -> exists('id')) echo '<li><button onclick="iniRequest(\'queue-popup\',\'queue\')">Play!</button></li>';  ?>
	 </ul>
   </nav>
  </div>
  -->
  
    <div class="modal fade" id="zaloguj" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Logowanie</h4>
				</div>
				<div class="modal-body">
					<form action="?page=login_action" method="POST">
						<div class="form-group">
							<label for="name" class="control-label formular-label">Nazwa użytkownika:</label>
							<input type="text" class="form-control formularz" id="name" name="login" placeholder="Wpisz swoj login">
						</div>
						<div class="form-group">
							<label for="password" class="control-label formular-label">Haslo:</label>
							<input type="password" class="form-control formularz" id="password" name="password" placeholder="Wpisz swoje hasło">
						</div>
				</div>
				<div class="modal-footer">
						<button type="button" class="btn btn-default zamknij" data-dismiss="modal">ZAMKNIJ</button>
						<button type="submit" class="btn btn-primary zaloguj">ZALOGUJ</button>
					</form>
				</div>
				Nie posiadasz konta? <a href="?page=register">Zarejestruj się!</a>
			</div>
		</div>
	</div>
        <section id="logo">
            <center>
                <a href="index.php"><img src="img/logo1.png" class="img-responsive" alt=""></a>
            </center>
        </section>
        <section id="top">Academic League of Games</section>
        <section id="calendar">Kalendarz</section>
    </header>
    <div style="clear:both"></div>
    <div id="container">
        <div id="west">
            <aside>
				<?php if($session -> exists('id')) echo '	<div id="online"></div> ';  ?>
				<?php if($session -> exists('id')) echo '<div class="uruchom"><button onclick="iniRequest(\'queue-popup\',\'queue\')" class="buttonplay">Play!</button></div>';  ?>
				<table id="westmenutop">
                    <tr>
                        <th class="logo">
                            <center><img src="img/logo1.png" class="img-responsive" alt=""></center>
                        </th>
                        <th>Zagraj online</th>
                    </tr>
                    <tr>
                        <td colspan="2"><?php if($session -> exists('id')) echo '<div id="queue-popup"></div>';  ?></td>
                    </tr>
               </table>
                <table id="westmenu">
                    <tr>
                        <th class="logo">
                            <center><img src="img/logo1.png" class="img-responsive" alt=""></center>
                        </th>
                        <th>Ranking</th>
                    </tr>
                    <tr>
                        <td colspan="2">Drużyny</td>
                    </tr>
                    <tr>
                        <td colspan="2">Gracze</td>
                    </tr>
                    <tr>
                        <th class="logo">
                            <center><img src="img/logo1.png" class="img-responsive" alt=""></center>
                        </th>
                        <th>Rozgrywki</th>
                    </tr>
                    <tr>
                        <td colspan="2">Najbliższe</td>
                    </tr>
                    <tr>
                        <td colspan="2">Historia</td>
                    </tr>
                    <tr>
                        <th class="logo">
                            <center><img src="img/logo1.png" class="img-responsive" alt=""></center>
                        </th>
                        <th>Konto zawodnika</th>
                    </tr>
                        <?php if(!($session -> exists('id'))) echo '<tr><td colspan="2"> <a href="#"data-toggle="modal" data-target="#zaloguj" data-whatever="@mdo">Zaloguj</a></td></tr>  ';  ?>
                        <?php if($session -> exists('id')) echo '<tr><td colspan="2"><a href="index.php?page=user_panel">Profil gracza</a></td></tr>';  ?>
						<?php if($session -> exists('id')) echo '<tr><td colspan="2"><a href="index.php?page=team">Drużyna</a></td></tr>';  ?>
						<?php if($session -> exists('id')) echo '<tr><td colspan="2"><a href="?page=logout">Wyloguj</a></td></tr>';  ?>
                </table>
            </aside>
        </div>

        <div id="east">

            <div id="eastmenu">
                <nav id="menu" class="navbar navbar-default">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu" aria-expanded="false">
        <span class="sr-only">Włącz menu</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php?page=pick_game&id=1">Counter Strike: Global Offensive</a></li>
                            <li><a href="index.php?page=pick_game&id=2">StarCraft II: Legacy of the Void</a></li>
                            <li><a href="index.php?page=pick_game&id=3">Hearthstone</a></li>
                            <li><a href="index.php?page=pick_game&id=4">Overwatch</a></li>
                            <li><a href="index.php?page=pick_game&id=5">Gwint: Wiedźmińska Gra Karciana</a></li>
                            <li><a href="index.php?page=pick_game&id=6">Leauge of Legends</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
    
	<article>
	
  <!-- <div class="right-section"> --!>
<?php

global $games;
global $user;
global $team;
global $league;

global $userData;
global $teamData;
global $leagueData;



if($session -> exists('id')){
        $games  =   new Game($pdo,$session);
        $user   =   new User($pdo);
        $team   =   new Team($pdo);
        $league =   new League($pdo);

		$userData     = $user -> getUserAllData();
		$teamData     = $team -> getDataTeam($userData['team']);
        $league ->  selectLeague($teamData['league']);
		$leagueData   = $league -> getLeagueData();
} else {
    $games  =   new Game($pdo,$session);
    $league =   new League($pdo);
    $team   =   new Team($pdo);
    
    $teamData   =   null;
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ALoG Games</title>
	<meta name="description" content="ALoG Games - Liga akademicka">
	<meta name="keywords" content="alog, liga akademicka">
	<meta name="author" content="Mateusz Bartelik <snaffx@wp.pl>">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/main.css?version=<?php echo filemtime('css/main.css');  ?>">
    <link rel="stylesheet" href="css/main2.css?version=<?php echo filemtime('css/main2.css');  ?>">
	<link rel="stylesheet" href="css/media.css">
</head>
<body>
	<header>
	<?php if($session -> exists('id')) echo '<div id="online"></div>'.PHP_EOL;  ?>
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.php"><p>ACADEMIC LEAGUE OF GAMES</p><p>POWERED BY ALOG</p></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<?php if ($session -> exists('id')) { echo '
							<div class="navbar-right profile-view">
							<img src="'.$user->getUserAvatar($userData['id']).'" class="img-responsive avatar"/>
							<div class="user-info">
						'; 			echo ($session -> exists("id"))?'<h5><a href="?page=user_panel">'.$userData["login"].'</a> <a href="index.php?page=logout">[Wyloguj]</a></h5>':'<a href="#" data-toggle="modal" data-target="#singIn" data-whatever="@login">ZALOGUJ</a>';
									if($session -> exists('id')) {
                                        if ($_SESSION['privilege'] == '0'){
                                             echo '<h6><a href="?page=admin">Admin Page</a></h6>';
                                        } elseif ($_SESSION['privilege'] == '2'){
                                             echo '<h6><a href="?page=leader">Team Leader</a></h6>';
                                        } elseif ($userData['team'] != null) {
                                            echo '<h6><a href="?page=team">'.$teamData["name"].'</a></h6>';
                                        } else {
                                            echo '<h6><a href="?page=team">Drużyna</a></h6>';
                                        }
                                    }
						echo 	'
							</div>
							<a href="?page=user_edit"><img src="images/settings-min.png" class="img-responsive settings"></a>
							</div>
							<div class="nav navbar-nav navbar-right user-settings">
								<div><a href="#"><img src="images/user-min.png" class="img-responsive settings img-menu"><span class="badge"></span></a></div>
								<div><a href="#"><img src="images/mail-min.png" class="img-responsive settings img-menu"><span class="badge"></span></a></div>
								<div><a href="#"><img src="images/world-min.png" class="img-responsive settings img-menu"><span class="badge"></span></a></div>
							</div>
						';} else { echo '
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav navbar-right menu-log">
									<li><a href="#" data-toggle="modal" data-target="#singIn" data-whatever="@queue">ZALOGUJ</a></li>
								</ul>
								<ul class="nav navbar-nav navbar-right menu-top">
									<li class="active"><a href="index.php?page=register">REJESTRACJA</a></li>
								</ul>
							</div>
						';}?>
						
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</div>
	</header>
	<div class="games-section">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
                            <?php
                                $gamesList    = $games -> getGameList();                            
                                foreach($gamesList as $game){
                                    echo "<li ";
                                    if((isset($_GET['id']))&&($_GET['id'] == $game['id'])){

                                        echo "class='active'";
                                    }
                                    echo "><a href='index.php?page=game&id=".$game['id']."'>".$game['name']."</a></li>";   
                                }
                            ?>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</div>
	</div>
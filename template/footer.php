<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<h1>GRAMY W</h1>
					<ul class="list">
						<li><a href="http://alogames.pl/gry.php">Counter Strike: Global Offensive</a></li>
						<li><a href="http://alogames.pl/gry.php">Leauge of Legends</a></li>
						<li><a href="http://alogames.pl/gry.php">Hearthstone</a></li>
						<li><a href="http://alogames.pl/gry.php">Overwatch</a></li>
					</ul>
				</div>
				<div class="col-lg-2">
					<h1>WSPÓŁPRACA</h1>
					<ul class="list">
						<li><a href="http://alogames.pl/onas.php">Informacje dla prasy</a></li>
						<li><a href="http://alogames.pl/rejestracja.php">Zgłoś swoją uczelnię!</a></li>
					</ul>
				</div>
				<div class="col-lg-2">
					<h1>KONTAKT</h1>
					<ul class="list">
						<li><a href="http://alogames.pl/kontakt.php">Formularz kontaktowy</a></li>
						<li><a href="https://www.facebook.com/ALoG.Official">Media społecznościowe</a></li>
						<li><a href="http://alogames.pl/kontakt.php">Zostań naszym partnerem</a></li>
					</ul>
				</div>
				<div class="col-lg-6">
					<div class="big-footer">
						<div>
							<h1>ACADEMIC LEAGUE OF GAMES</h1>
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2 tooltips">
									<img src="images/footer/phone.png" alt="" data-toggle="tooltip" data-placement="bottom" title="697-919-964" class="img-responsive">
									<img src="images/footer/mail.png" alt="" data-toggle="tooltip" data-placement="bottom" title="biuro@alogames.pl" class="img-responsive">
									<img src="images/footer/fb.png" alt="" data-toggle="tooltip" data-placement="bottom" title="facebook.com/ALoG.Official" class="img-responsive">
									<img src="images/footer/local.png" alt="" data-html="true" data-toggle="tooltip" data-placement="bottom" title="04-477 Warszawa <br>ul. Newelska 6" class="img-responsive">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row footer-info">
				<div class="left"><a href="http://mbartelik.ga"><img src="images/logo.png" alt=""></a></div>
				<div class="right">
					<p>Copyright © 2017 Academic League of Games sp. z o.o. All rights reserved.</p>
				</div>
			</div>
		</div>
	</footer>
    <div class="modal fade" id="singIn" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ModalLabel">PANEL LOGOWANIA</h4>
				</div>
				<div class="modal-body">
					<form action="?page=login_action" method="POST" id="form-login">
						<div class="form-group">
							<label for="login" class="control-label">Nazwa użytkownika:</label>
							<input type="text" class="form-control" id="login" name="login">
						</div>
						<div class="form-group">
							<label for="pass" class="control-label">Hasło:</label>
							<input type="password" class="form-control" id="pass" name="password">
						</div>
					</form>
					<div class="information"><p><a href="#" data-toggle="modal" data-target="#recoveryPass" data-dismiss="modal">Przypomnienie hasła</a></p><p>|</p><p>Nie posiadasz konta? <a href="#">Zarejestruj się!</a></p></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-back" data-dismiss="modal">POWRÓT</button>
					<button type="submit" class="btn btn-log" form="form-login">ZALOGUJ!</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="recoveryPass" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="ModalLabel">ODZYSKIWANIE HASŁA</h4>
				</div>
				<div class="modal-body">
					<form action="?page=recovery_pass" method="POST" id="form-recovery">
						<div class="form-group">
							<label for="email" class="control-label">Podaj adres e-mail:</label>
							<input type="text" class="form-control" id="email" name="email">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-back" data-dismiss="modal">ANULUJ</button>
					<button type="submit" class="btn btn-log" form="form-recovery">ODZYSKAJ KONTO</button>
				</div>
			</div>
		</div>
	</div>
    <?php 
        if($session -> exists('id')) { echo '
            <div class="modal fade" id="runQueue" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="ModalLabel">TWORZENIE KOLEJKI</h4>
                        </div>
                        <div class="modal-body">
                            <div id="queue-process"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-back" data-dismiss="modal">ZAMKNIJ OKNO</button>
                            <button type="button" id="actionQButton" class="btn btn-back" data-dismiss="modal">ANULUJ</button>
                        </div>
                    </div>
                </div>
            </div> '; }
        ?>
	<script src="jscripts/jquery.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="jscripts/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="jscripts/main.js"></script>
	<script>
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		})

		$(".awards a").popover();

		$(".clickable").click(function() {
			var id = $(this).attr('id');
			var target = '.'+id+'collapsed';

			if($(target).hasClass("out")) {
				$(target).addClass("in");
				$(target).removeClass("out");
			} else {
				$(target).addClass("out");
				$(target).removeClass("in");
			}
		});

		$('#singIn').modal('hide');
		$('#recoveryPass').modal('hide');

		$('#recoveryPass').on('show.bs.modal', function (event) {
			$(document.body).css({overflow: "hidden"});
		});

		$('#recoveryPass').on('hidden.bs.modal', function (event) {
			$(document.body).css({paddingRight: "0px"});
			$(document.body).css({overflowY: "visible"});
		});
	</script>
</body>
</html>
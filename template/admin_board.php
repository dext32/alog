<?php
if(!$session -> exists('id') || !$session -> exists('login') || !($_SESSION['privilege'] === '0') ) return 'Brak uprawnień.';
?>

<?php 
    $boardData = $admin -> getLobbyReports($_GET['board']);
    $lobbyData = $admin -> getLobbyData($_GET['board']);
?>

<div class="container boxes" style="background-color:#fff;">
    <div class="row">
        <?php if (isset($_GET['status'])) {
                    if ($_GET['status'] == "match_end_true") {
                        echo '
                        <div class="alert alert-success" role="alert">
                          Pomyślnie zamknięto spotkanie. Wynik został zapisany.
                        </div>
                        '; 
                    } elseif ($_GET['status'] == "match_end_false") {
                        echo '
                        <div class="alert alert-danger" role="alert">
                          Operacja zakończenia spotkania nie przebiegła poprawnie.
                        </div>
                        '; 
                    }
                }
        ?>
        <form action="index.php?page=admin&board=<?php echo $lobbyData['id']; ?>" method="post" enctype="multipart/form-data">
        <div class="col-lg-12" style="background-color:#fff;">
            <div class="col-lg-4"><h1 style="text-align:center;">ZAMKNIJ SPOTKANIE</h1></div>
            <div class="col-lg-4">
                <div class="team-middle">
                    <div class="team-score-left">
                        <h3><?php echo $team->getTeamName($lobbyData['team1']); ?></h3>
                        <input type="text" name="score1" placeholder="0" required>
                        <input type="text" name="team1" value="<?php echo $lobbyData['team1']; ?>" style="display:none" readonly>
                    </div>
                    <div class="team-score-center">
                        <p>:</p>
                        <input type="text" name="lobby" value="<?php echo $lobbyData['id']; ?>" style="display:none" readonly>
                        <input type="text" name="date" value="<?php echo $lobbyData['start_time']; ?>" style="display:none" readonly>
                    </div>
                    <div class="team-score-right">
                        <h3><?php echo $team->getTeamName($lobbyData['team2']); ?></h3>
                        <input type="text" name="score2" placeholder="0" required>
                        <input type="text" name="team2" value="<?php echo $lobbyData['team2']; ?>" style="display:none" readonly>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="col-lg-4 register-form send-input end-match">
                    <input class="form-control" style="margin:40px; width:210px; height: 55px;" name="match_end" value="USTAW WYNIK" type="submit">
                </div>
            </div>
        </div>
        </form>
        <?php 
            if ( empty($boardData) ) { echo "<h1 style=\"text-align:center;margin:20px;\">BRAK RAPORTÓW DO WYŚWIETLENIA.</h1>"; }
            foreach($boardData as $boardReport){
                $userGameLogin = $user->getUserGameLogins($boardReport['user'],$lobbyData['game']);
                echo '
                  <div class="card" style="width:45%; float:left;margin:2%;">
                  ';  if ($boardReport['screenshot'] != null) { echo '<img class="card-img-top" src="uploads/'.$boardReport['screenshot'].'">';}
                echo '<div class="card-body">
                    <h5 class="card-title">Użytkownik : '.$user->getUserName($boardReport['user']).' ('.$userGameLogin['login'].')</h5>
                    <p class="card-text"><h2>'.$boardReport['info'].'</h2><br>[id]NAME SCORE_1 : SCORE_2 [id]NAME <- jak czytać</p>
                  </div>
                  <div class="card-body">
                    <a href="uploads/'.$boardReport['screenshot'].'" class="card-link" target="_blank" style="color:red;">Screenshot</a>
                  </div>
                </div>
                ';
            }
        ?>
    </div>
</div>
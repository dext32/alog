<div class="boxes profiles">
		<div class="container">
			<div class="col-lg-3">
				<div class="image-user">
					<img src="<?php echo $user->getUserAvatar($userData['id']); ?>" alt="" class="img-responsive">
					<div class="user-caption">
						<h2><?php echo $userData['login'] ?></h2>
						<h3><?php if(isset($userData['team'])){ echo $team->getTeamName($userData['team']); } ?></h3>
					</div>
				</div>
				<div class="menu-sidebar">
					<h1>RANKINGI</h1>
					<nav>
						<li><a href="#">Link01</a></li>
						<li><a href="#">Link02</a></li>
						<li><a href="#">Link03</a></li>
						<li><a href="#">Link04</a></li>
					</nav>
					<h1>ROZGRYWKI</h1>
					<nav>
						<li><a href="#">Link01</a></li>
						<li><a href="#">Link02</a></li>
						<li><a href="#">Link03</a></li>
						<li><a href="#">Link04</a></li>
					</nav>
					<h1>KONTO GRACZA</h1>
					<nav>
						<li><a href="#">Link01</a></li>
						<li><a href="#">Link02</a></li>
						<li><a href="#">Link03</a></li>
						<li><a href="#">Link04</a></li>
					</nav>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="main-profile-view" style="display: inline-block;">
					<div class="center-text">
						<h1><?php echo strtoupper($user->getUserUniversityName()); ?></h1>
                    </div>
                    <?php
                        foreach($uniTeams as $uniTeamKeys){
                            $team -> id = $uniTeamKeys['id'];
                            $reviewTeam = $league->getLeagueScoresMini($team);
                            echo '
                               <div class="col-lg-6 mt-4 mb-4">
                                   <div class="col-lg-6"><img src="'.$team->getTeamAvatar($uniTeamKeys['id']).'" alt="" class="img-responsive"><a href="#">      <button type="button" class="btn btn-default btn-stats">STATYSTYKI</button></a></div>
                                      <div class="col-lg-6">
                            ';
                            if ($uniTeamKeys['id'] == $userData['team']){
                                echo '<h2>TWOJA DRUŻYNA TO:</h2>';
                            }
                            echo '
                                        <h3>'.$uniTeamKeys['name'].'</h3>
                                        <p>POZYCJA TO:<span>'.$reviewTeam['place'].'</span></p>
                                        <p>PUNKTY: <span>'.$reviewTeam['points'].'</span></p>
                                    </div>
                                </div>
                            ';
                        }
                    ?>
				</div>
				<div class="history-view boxes main-profile-view">
					<table class="table">
						<thead>
							<tr>
								<th>DATA</th>
								<th>DATA</th>
								<th>DATA</th>
								<th>DATA</th>
								<th>DATA</th>
							</tr>
							<tbody>
								<tr>
									<td>01</td>
									<td>02</td>
									<td>03</td>
									<td>04</td>
									<td>05</td>
								</tr>
								<tr>
									<td>01</td>
									<td>02</td>
									<td>03</td>
									<td>04</td>
									<td>05</td>
								</tr>
								<tr>
									<td>01</td>
									<td>02</td>
									<td>03</td>
									<td>04</td>
									<td>05</td>
								</tr>
								<tr>
									<td>01</td>
									<td>02</td>
									<td>03</td>
									<td>04</td>
									<td>05</td>
								</tr>
								<tr>
									<td>01</td>
									<td>02</td>
									<td>03</td>
									<td>04</td>
									<td>05</td>
								</tr>
							</tbody>
						</thead>
					</table>
				</div>
				<div class="calendar boxes">
					<h1>KALENDARZ...</h1>
				</div>
				<div class="ranging-profile-view main-profile-view">
					<div class="row">
						<div class="col-lg-4">
							<h1>RANKING UŻYTKOWNIKOW</h1>
							<table class="table extend-table">
								<thead>
									<tr>
										<th>Lp</th>
										<th>Użytkownik</th>
										<th>Punkty</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1.</td>
										<td>damian2323</td>
										<td>1690</td>
									</tr>
									<tr>
										<td>2.</td>
										<td>robert44</td>
										<td>1200</td>
									</tr>
									<tr>
										<td>3.</td>
										<td>krzychu</td>
										<td>800</td>
									</tr>
									<tr>
										<td>4.</td>
										<td>jacek</td>
										<td>470</td>
									</tr>
									<tr>
										<td>5.</td>
										<td>Gienek</td>
										<td>69</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-4">
							<h1>RANKING UŻYTKOWNIKOW</h1>
							<table class="table extend-table">
								<thead>
									<tr>
										<th>Lp</th>
										<th>Użytkownik</th>
										<th>Punkty</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1.</td>
										<td>damian2323</td>
										<td>1690</td>
									</tr>
									<tr>
										<td>2.</td>
										<td>robert44</td>
										<td>1200</td>
									</tr>
									<tr>
										<td>3.</td>
										<td>krzychu</td>
										<td>800</td>
									</tr>
									<tr>
										<td>4.</td>
										<td>jacek</td>
										<td>470</td>
									</tr>
									<tr>
										<td>5.</td>
										<td>Gienek</td>
										<td>69</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-4">
							<h1>RANKING UŻYTKOWNIKOW</h1>
							<table class="table extend-table">
								<thead>
									<tr>
										<th>Lp</th>
										<th>Użytkownik</th>
										<th>Punkty</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1.</td>
										<td>damian2323</td>
										<td>1690</td>
									</tr>
									<tr>
										<td>2.</td>
										<td>robert44</td>
										<td>1200</td>
									</tr>
									<tr>
										<td>3.</td>
										<td>krzychu</td>
										<td>800</td>
									</tr>
									<tr>
										<td>4.</td>
										<td>jacek</td>
										<td>470</td>
									</tr>
									<tr>
										<td>5.</td>
										<td>Gienek</td>
										<td>69</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
if(!$session -> exists('id') || !$session -> exists('login'))
{
 echo 'Brak uprawnień.';
 return false;
}
else
{
echo '
   <h3>Nowa drużyna</h3>
	<div id="player">
		<div class="player">
			<figure>
				<a href="#"><img src="img/ow.jpg" class="img-responsive" alt=""></a>
			</figure>
		</div>
		<div class="teamname">
		<form action="index.php" method="GET">
			<input type="text" name="page" value="team" style="display:none" readonly>
			<div class="form-group">
				<input type="text" name="name" class="form-control input-sm" placeholder="Nazwa drużyny">
			</div>
			<div class="form-group">
				<input type="text" name="tag" class="form-control input-sm" placeholder="Tag drużyny">
			</div>
			<input type="submit" value="Dodaj drużynę" class="btn btn-info btn-block">
		</form>
		</div>
    <div style="clear:both"></div>
 </div>
';
}

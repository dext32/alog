<div class="container-fluid">
        <div class="row centered-form">
		<?php if($session -> exists('id')) { header("Location: index.php"); } ?>
		<div class="col-lg-8 col-lg-offset-2 col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-2">
        	<div class="panel panel-default">
        		<div class="panel-heading">
			    		<h3 class="panel-title"> REJESTRACJA</h3>
			 			</div>
			 			<div class="panel-body">
			    		<form action="?page=register_action" method="POST" class="register-form" role="form">
			    			<div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			    					<div class="form-group">
			                <input type="text" name="name" class="form-control input-sm" placeholder="Imię">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			    					<div class="form-group">
			    						<input type="text" name="surname" class="form-control input-sm" placeholder="Nazwisko">
			    					</div>
			    				</div>
			    			</div>

			    			<div class="form-group">
			    				<input type="text" name="login" class="form-control input-sm" placeholder="Login">
			    			</div>
			    			
			    			<div class="form-group">
			    				<input type="email" name="email" class="form-control input-sm" placeholder="Email Address">
			    			</div>

			    			<div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			    					<div class="form-group">
			    						<input type="password" name="password" class="form-control input-sm" placeholder="Hasło">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			    					<div class="form-group">
			    						<input type="password" name="password_confirmation" class="form-control input-sm" placeholder="Powtórz hasło">
			    					</div>
			    				</div>
			    			</div>
			    			
			    			<div class="form-group">
								<select name="university" class="form-control input-sm">
									<?php
									foreach($universityList as $universityArray) {
										echo "<option value=".$universityArray['id'].">".$universityArray['name']."</option>";
									}
									?>
								</select>
			    			</div>	
			    			
			    					<div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			    					<div class="form-group">
			    						<input type="text" name="adres1" class="form-control input-sm" placeholder="Ulica">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			    					<div class="form-group">
			    						<input type="text" name="adres2" class="form-control input-sm" placeholder="Numer budynku / Lokalu">
			    					</div>
			    				</div>
			    			</div>
			    			
			    						<div class="row">
			    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			    					<div class="form-group">
			    						<input type="text" name="city" class="form-control input-sm" placeholder="Miejscowość">
			    					</div>
			    				</div>
			    				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
			    					<div class="form-group">
			    						<input type="text" name="postal" class="form-control input-sm" placeholder="Kod pocztowy">
			    					</div>
			    				</div>
			    			</div>
			    			
			    			<input type="submit" value="Zarejestruj" class="btn btn-info btn-block">
			    		
			    		</form>
			    	</div>
	    		</div>
    		</div>
    	</div>
    </div>
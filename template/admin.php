<?php
if(!$session -> exists('id') || !$session -> exists('login') || !($_SESSION['privilege'] === '0') ) return 'Brak uprawnień.';
?>

<?php    
$allTeams   = $admin->getAllTeams();
$leagues    = $admin->getAllLeagues();
$lobbyList  = $admin->getLobbyList();
$queueListWithPlayersData = $admin->getQueueListWithPlayers();
$lobbyPasswords = $admin->passwords;
$allMeets   = $admin->getMeetsList();
?>

<div class="container boxes">
    <div class="row section-1">
        <div class="col-lg-10 col-lg-offset-1">
            <form action="index.php?page=admin" method="post">
                <h2>Nowe spotkanie</h2>
                <?php if (isset($_GET['new_meet_status']) && $_GET['new_meet_status'] == "true") {
                    echo '
                    <div class="alert alert-success" role="alert">
                      Dodano nowe spotkanie! '.$team->getTeamName($_POST['team1']).' vs. '.$team->getTeamName($_POST['team2']).'
                    </div>
                    '; } 
                    if (isset($_GET['set_captain_status']) && $_GET['set_captain_status'] == "true") {
                    echo '
                    <div class="alert alert-success" role="alert">
                      Ustawiono kapitana '.$user->getUserName($_POST['captain']).' drużyny '.$team->getTeamName($_POST['team']).'
                    </div>
                    '; }
                    if (isset($_GET['delete_meet_result'])) {
                        if ($_GET['delete_meet_result'] == "true") {
                            echo '
                            <div class="alert alert-success" role="alert">
                              Usunięto spotkanie. 
                            </div>
                            '; 
                        } else {
                            echo '
                            <div class="alert alert-warning" role="alert">
                              Coś poszło nie tak. 
                            </div>
                            '; 
                        }
                    }
                ?>
                <label for="league">Liga</label>
                <select id="league" name="league" style="width:150px;">
                <?php
                    foreach($leagues as $leagueKeys){
                        echo "<option value=\"".$leagueKeys['id']."\">".$leagueKeys['name']."</option>";
                    }
                ?>
                </select>
                <label for="teams1">Drużyna 1</label>
                <select id="teams1" name="team1" style="width:50px;">
                <?php
                    foreach($allTeams as $teamKeys){
                        echo "<option value=\"".$teamKeys['id']."\">".$teamKeys['name']."(".$games->getGameName($teamKeys['game']).")</option>";
                    }
                ?>
                </select>
                <label for="teams2">Drużyna 2</label>
                <select id="teams2" name="team2" style="width:50px;">
                <?php
                    foreach($allTeams as $teamKeys){
                        echo "<option value=\"".$teamKeys['id']."\">".$teamKeys['name']."(".$games->getGameName($teamKeys['game']).")</option>";
                    }
                ?>
                </select>
                <input type="datetime-local" name="date" required/>
                <input type="submit" name="add_new_meet" value="Dodaj spotkanie">
            </form>
                <?php 
                    if(isset($_GET['error']) && $_GET['error']=="cpt1_not_exist") {
                        $teamMembers = $team->getTeamMembers($_POST['team1']);
                        echo "Nie wyznaczono kapitana drużyny ".$team->getTeamName($_POST['team1']).".<br> Czy wyznaczyć ";
                        echo '<form action="index.php?page=admin" method="post">
                                <select name="captain" required>';
                                foreach($teamMembers as $member){
                                    echo "<option value=\"".$member['id']."\">".$member['name']." ".$member['surname']." (".$member['login'].")</option>";
                                }
                        echo '  </select>
                                <input type="text" name="team" value="'.$_POST['team1'].'" style="display:none;">
                                <input type="submit" name="set_captain" value="OK">
                            </form>';
                    }
                    if(isset($_GET['error1']) && $_GET['error1']=="cpt2_not_exist") {
                        $teamMembers = $team->getTeamMembers($_POST['team2']);
                        echo "Nie wyznaczono kapitana drużyny ".$team->getTeamName($_POST['team2']).".<br> Czy wyznaczyć ";
                        echo '<form action="index.php?page=admin" method="post">
                                <select name="captain" required>';
                                foreach($teamMembers as $member){
                                    echo "<option value=\"".$member['id']."\">".$member['name']." ".$member['surname']." (".$member['login'].")</option>";
                                }
                        echo '  </select>
                                <input type="text" name="team" value="'.$_POST['team2'].'" style="display:none;">
                                <input type="submit" name="set_captain" value="OK">
                            </form>';
                    }
                ?>
            <br>
        </div>
        <div class="col-lg-10 col-lg-offset-1 scroll-window">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Drużyny</th>
                  <th scope="col">Liga</th>
                  <th scope="col" style="width:145px;">Termin</th>
                    <th scope="col">Usuń</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                    foreach($allMeets as $metData){
                        echo '
                            <tr>
                              <td>'.$team->getTeamName($metData['team1']).' vs. '.$team->getTeamName($metData['team2']).'</td>
                              <td>'.$league->getLeagueName($metData['league']).'</td>
                              <td>'.date_format(date_create($metData['date']),"d-m-Y H : i").'</td>
                              <td><form action="index.php?page=admin&a=delete_meet" method="post"><input type="text" value="'.$metData['id'].'" name="meet_id" style="display:none;" readonly><input type="image" name="submit" src="images/icons/delete.png" border="0" alt="Usuń" width="15"/></form></td>
                            </tr>
                        ';
                    }
                ?>   
              </tbody>
            </table>     
        </div>
        <div class="col-lg-12">
            <h1>QUEUE MANAGER</h1>
            <div class="col-lg-12" style="padding-bottom:50px;">
                <h3 style="margin-top:50px;">Niepełna kolejka</h3>
                <?php
                    foreach ($queueListWithPlayersData as $queueData){
                        echo '
                            <div class="container" style="margin-bottom:5px;">
                                <div class="col-lg-9" style="background-color: #f9f9f9; margin-top:15px;padding:0px; height:auto">
                                    <div class="col-lg-3 thumb-img" style="padding:0px; height: 100%;">
                                        <img src="images/games/0'.$queueData['game'].'.jpg" style="width:100%;">
                                    </div>
                                    <div class="col-lg-7" >
                                        '.$games->getGameName($queueData['game']).' ( '.$queueData['slots'].' / '.$queueData['max_slots'].' graczy)<br>
                                        <span style="text-align:middle">
                                            <h1>'.$team->getTeamName($queueData['team1']).' vs. '.$team->getTeamName($queueData['team2']).'</h1>
                                        </span>';

                                    echo '<div style="border:1px solid lightgrey"> Gracze nie połączeni: <br>';
                                        foreach ($queueData['players_off'] as $queuePlayerOff) {
                                            echo '[ID '.$queuePlayerOff['id'].'] Login: '.$queuePlayerOff['login'].' ('.$team->getTeamName($queuePlayerOff['team']).')<br>';
                                        }
                                    echo '</div>';

                        echo '     </div>
                                </div>
                            </div>
                        ';
                    }
                ?>
            </div>
        </div>
        <div class="col-lg-12">
            <h1>LOBBY MANAGER</h1>
            <div class="col-lg-12" style="padding-bottom:50px;">
                <h3 style="margin-top:50px;">Nieskonfigurowane lobby</h3>
                <?php 
                    foreach($lobbyList['not_configured'] as $lobbyData){
                        echo '
                            <div class="container" style="margin-bottom:5px;">
                                <div class="col-lg-9" style="background-color: #f9f9f9; margin-top:15px;padding:0px; height:110px">
                                    <div class="col-lg-3 thumb-img" style="padding:0px; height: 100%;">
                                        <img src="images/games/0'.$lobbyData['game'].'.jpg" style="height:100%;">
                                    </div>
                                    <div class="col-lg-7" >
                                        '.$games->getGameName($lobbyData['game']).' ( '.$lobbyData['start_time'].' ) W lobby : '.count(unserialize($lobbyData['joined'])).' graczy<br>
                                        <span style="text-align:middle"><h1>'.$team->getTeamName($lobbyData['team1']).' vs. '.$team->getTeamName($lobbyData['team2']).'</h1>
                                        </span>
                                        <form action="index.php?page=admin" method="post">
                                            <input type="text" name="id" value="'.$lobbyData['id'].'" style="display:none;">
                                            <label for="ip">Lobby / IP</label>
                                            <input id="ip" type="text" name="ip" value="'; if($lobbyData['game']==core::$config['game']['csgo_id']){echo "213.135.48.28:27011";} else {echo 'ALoG #'.$lobbyData['id'];} echo '" style="width:130px;" required>
                                            <label for="pw">Hasło</label>
                                            <input id="pw" type="text" name="pw" style="width:100px;" required>
                                            <!-- <select name="pw" required>';
                                            foreach($lobbyPasswords as $password){
                                                echo "<option value=\"".$password."\">".$password."</option>";
                                            }
                                    echo '  </select> -->
                                        <input type="submit" name="update_lobby" value="Gra utworzona">
                                        </form>
                                    </div>
                                    <div class="col-lg-2" style="background-color:red;height:100%;">
                                        <span style="text-align:center;"><h1 style="margin-top:40%;"><a href="#">#'.$lobbyData['id'].'</a></h1></span>
                                    </div>
                                </div>
                            </div>
                        ';
                    }
                ?>
                
                <h3 style="margin-top:50px;">Oczekujące na wynik</h3>
                <?php 
                    foreach($lobbyList['not_scored'] as $lobbyData){
                        echo '
                            <div class="container">
                                <div class="col-lg-9" style="background-color: #f9f9f9; margin-top:15px;padding:0px; height:110px">
                                    <div class="col-lg-3 thumb-img" style="padding:0px; height: 100%;">
                                        <img src="images/games/0'.$lobbyData['game'].'.jpg" style="height:100%;">
                                    </div>
                                    <div class="col-lg-7" >
                                        '.$games->getGameName($lobbyData['game']).' ( '.$lobbyData['start_time'].' ) W lobby : '.count(unserialize($lobbyData['joined'])).' graczy<br>
                                        <span style="text-align:middle"><h1>'.$team->getTeamName($lobbyData['team1']).' vs. '.$team->getTeamName($lobbyData['team2']).'</h1>
                                        </span>
                                        <a href="index.php?page=admin&board='.$lobbyData['id'].'" style="color:red;">Przejdź do tabeli propozycji</a>
                                    </div>
                                    <div class="col-lg-2" style="background-color:red;height:100%;">
                                        <span style="text-align:center;"><h1 style="margin-top:40%;"><a href="#">#'.$lobbyData['id'].'</a></h1></span>
                                    </div>
                                </div>
                            </div>
                        ';
                    }
                ?>
                
                <h3 style="margin-top:50px;">Rozegrane</h3>
                <?php 
                    foreach($lobbyList['finalized'] as $lobbyData){
                        echo '
                            <div class="container">
                                <div class="col-lg-9" style="background-color: #f9f9f9; margin-top:15px;padding:0px; height:110px">
                                    <div class="col-lg-3 thumb-img" style="padding:0px; height: 100%;">
                                        <img src="images/games/0'.$lobbyData['game'].'.jpg" style="height:100%;">
                                    </div>
                                    <div class="col-lg-7" >
                                        '.$games->getGameName($lobbyData['game']).' ( '.$lobbyData['start_time'].' ) W lobby : '.count(unserialize($lobbyData['joined'])).' graczy<br>
                                        <span style="text-align:middle"><h1>'.$team->getTeamName($lobbyData['team1']).' vs. '.$team->getTeamName($lobbyData['team2']).'</h1>
                                        </span>
                                        <span style="text-align:middle"><h1>'.$lobbyData['score1'].' : '.$lobbyData['score2'].'</h1></span>
                                    </div>
                                    <div class="col-lg-2" style="background-color:red;height:100%;">
                                        <span style="text-align:center;"><h1 style="margin-top:40%;"><a href="#">#'.$lobbyData['id'].'</a></h1></span>
                                    </div>
                                </div>
                            </div>
                        ';
                    }
                ?>
            </div>
        </div>
        

        <!-- 
        <form action="index.php">
          <input type="text" name="page" value="add_game" style="display:none" readonly>
          Nazwa gry:<br>
          <input type="text" name="name">
          <br>
          Maksymalna liczba slotów:<br>
          <input type="text" name="max_slots">
          <br><br>
          <input type="submit" value="Dodaj grę">
        </form> 
        <br>

        <form action="index.php">
          <input type="text" name="page" value="delete_game" style="display:none" readonly>
          Id gry:<br>
          <input type="text" name="id">
          <br><br>
          <input type="submit" value="Usuń grę">
        </form> 

        <form action="index.php">
          <input type="text" name="page" value="add_league" style="display:none" readonly>
          <input list="games" name="gameId" class="form-control input-sm" placeholder="Gra ligii">
            <datalist id="games">
            <?php
                /*foreach($gameList as $gameArray) {
                    echo "<option value=".$gameArray['id'].">".$gameArray['name']."</option>";
                }*/
             ?>
            </datalist>
          Nazwa ligii :<br>
          <input type="text" name="name">
          <br><br>
          <input type="submit" value="Dodaj turniej">
        </form>

        -->
    </div>
</div>

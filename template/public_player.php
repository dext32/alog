<div class="container boxes">
    <div class="row section-1">
        <div class="col-lg-12">
            <div class="col-lg-3">
                <div class="left">
                    <img src="<?php echo $user->getUserAvatar($userPubData['id']); ?>" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-lg-9">
                <div class="col-lg-offset-1">
                    <div class="images-public-user mt-4">
                        <img src="images/user.png" alt="Me" class="img-responsive"> 
                        <h2><?php echo $userPubData['login']; ?></h2>
                    </div>
                    <div class="images-public-user mt-3">
                        <img src="images/students-cap.png" alt="University" class="img-responsive"> 
                        <h2><a href="#user_university" style="color:black;"><?php echo $user->getUniversityName($userPubData['id']); ?></a></h2>
                    </div>
                    <div class="images-public-user mt-3">
                        <img src="images/group.png" alt="Team" class="img-responsive"> 
                        <h2><a href="<?php echo $team->getTeamLink($userPubData['team']); ?>" style="color:black;"><?php echo ($team->getTeamName($userPubData['team'])) ? $team->getTeamName($userPubData['team']) : "BRAK DRUŻYNY"; ?></a></h2>
                    </div>
                </div>
            </div>            
        </div>
    </div>
    
    <div class="row section-2 mt-3">
        <div class="col-lg-12">
            <h1><?php echo $games->getGameName($userTeamData['game']); ?></h1>
            <div class="col-lg-3" style="height:180px">
                <div style="margin-top:25%;"><?php echo ($league->getLeagueName($userTeamData['league'])) ? '<a href="'.$league->getLeagueLink().'" style="color:black;">'.$league->getLeagueName($userTeamData['league']).'</a>' : "Drużyna nie gra w żadnej lidze"; ?></div>
            </div>
            <div class="col-lg-9" style="border-left:1px solid #b8b8b8;">
                <h1 class="text-center-off" ><?php echo $userTeamData['name']; ?></h1>
                <h1 class="mt-3 text-center-off" >MIEJSCE # <?php echo $userTeamStats['place']; ?></h1>
                <h1 class="mt-3 text-center-off" >STATYSTYKI : 
                    <span style="color:green;"><?php echo $userTeamStats['win']; ?></span> / 
                    <span><?php echo $userTeamStats['draw']; ?></span> / 
                    <span style="color:red;"><?php echo $userTeamStats['lose']; ?></span>
                </h1>
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-tournaments">
					<tbody>
                        <?php
                            $leagueHistoryLast = $league->getTeamScores($userPubData['team']);
                            if ($leagueHistoryLast) {
                                for ($i=0 ; $i<=5; $i++){
                                    if (isset($leagueHistoryLast[$i])) {
                                        echo '
                                            <tr>
                                                <td style="width:10%;"><img src="'.$team->getTeamAvatar($leagueHistoryLast[$i]['team1']).'" class="img-responsive"></td>
                                                <td style="width:10%;"><h1 style="font-size:36px;">'.$leagueHistoryLast[$i]['score1'].'</h1></td>
                                                <td style="width:25%;"><div style="text-align:center;">'.$team->getTeamName($leagueHistoryLast[$i]['team1']).'</div></td>
                                                <td style="width:10%;"><div style="text-align:center;">vs.</div></td>
                                                <td style="width:25%;"><div style="text-align:center;">'.$team->getTeamName($leagueHistoryLast[$i]['team2']).'</div></td>
                                                <td style="width:10%;"><h1 style="font-size:36px;">'.$leagueHistoryLast[$i]['score2'].'</h1></td>
                                                <td style="width:10%;"><img src="'.$team->getTeamAvatar($leagueHistoryLast[$i]['team2']).'" class="img-responsive"></td>
                                            </tr>
                                        ';
                                    }
                                }
                            } else {
                                echo '<td style="width:100%;"><h1>BRAK ROZGRYWEK DO WYŚWIETLENIA</h1></td>';
                            }
                            
                        ?>
						<tr class="clickable" id="71">
							<td colspan="7">
								<h2>POKAŻ WSZYSTKIE ROZGRYWKI</h2>
								<img src="images/arrow-down.png" alt="">
							</td>
						</tr>
                        <?php 
                            foreach ($leagueHistoryLast as $leagueHistoryLong){
                                echo '
                                    <tr class="collapse out budgets 71collapsed">
                                        <td style="width:10%;"><img src="'.$team->getTeamAvatar($leagueHistoryLong['team1']).'" class="img-responsive"></td>
                                        <td style="width:10%;"><h1 style="font-size:36px;">'.$leagueHistoryLong['score1'].'</h1></td>
                                        <td style="width:25%;"><div style="text-align:center;">'.$team->getTeamName($leagueHistoryLong['team1']).'</div></td>
                                        <td style="width:10%;"><div style="text-align:center;">vs.</div></td>
                                        <td style="width:25%;"><div style="text-align:center;">'.$team->getTeamName($leagueHistoryLong['team2']).'</div></td>
                                        <td style="width:10%;"><h1 style="font-size:36px;">'.$leagueHistoryLong['score2'].'</h1></td>
                                        <td style="width:10%;"><img src="'.$team->getTeamAvatar($leagueHistoryLong['team2']).'" class="img-responsive"></td>
                                    </tr>                      
                                ';
                            }
                        ?>
					</tbody>
				</table>
        </div>
    </div>
    
    <div class="row section-2 mt-3">
        <div class="col-lg-12">
            <h2 style="text-align:center;"><a id="user_university"></a><?php echo $user->getUniversityName($userPubData['id']); ?></h2>
            <h1>POZOSTAŁE DRUŻYNY</h1>
            <?php
                foreach($uniTeams as $uniTeamKeys){
                    $league -> id = $uniTeamKeys['league'];
                    $specificLeagueData = $league -> getLeagueData();
                    $team -> id = $uniTeamKeys['id'];
                    $reviewTeam = $league->getLeagueScoresMini($team);
                    echo '
                       <div class="col-lg-6 mt-4 mb-4">
                           <div class="col-lg-6"><img src="'.$team->getTeamAvatar($uniTeamKeys['id']).'" alt="" class="img-responsive">
                               <a href="'.$team->getTeamLink($uniTeamKeys['id']).'">
                                    <button type="button" class="btn btn-default btn-stats">STATYSTYKI</button>
                                </a>
                            </div>
                            <div class="col-lg-6">
                    ';
                    if ($uniTeamKeys['id'] == $userPubData['team']){
                        echo '<h2>GRA W DRUŻYNIE</h2>';
                    }
                    echo '
                                <h3>'.$uniTeamKeys['name'].'</h3>
                                <p>'.$specificLeagueData['gamename'].'</p>
                                <p>MIEJSCE W LIDZE: <span>'.$reviewTeam['place'].'</span></p>
                                <p>PUNKTY W LIDZE: <span>'.$reviewTeam['points'].'</span></p>
                            </div>
                        </div>
                    ';
                }
            ?>
        </div>
    </div>
</div>
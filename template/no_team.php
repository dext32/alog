<?php

    if(!$session -> exists('id') || !$session -> exists('login'))
    {
        echo 'Brak uprawnień.';
        return false;
    }

?>


<div class="container boxes">
    <div class="row section-1">
        <div class="col-lg-6 col-lg-offset-1">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tag</th>
                        <th scope="col">Nazwa</th>
                        <th scope="col">Gra</th>
                        <th scope="col">Liga</th>
                        <th scope="col">Aplikuj</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    $i = 1;
                    if ( $_SESSION['privilege'] === '2' ) {
                        echo "<tr>
                                <th scope=\"row\" colspan=\"5\">    Jako lider nie możesz dołączyć do żadnej z drużyn.</th>
                              </tr>";
                    } elseif ( !empty($uniTeams) ){
                        foreach($uniTeams as $uniTeam){
                            echo "<tr>
                                    <th scope=\"row\">".$i++."</th>
                                    <td>".$uniTeam['tag']."</td>
                                    <td>".$uniTeam['name']."</td>
                                    <td>".$games->getGameName($uniTeam['game'])."</td>
                                    <td>".$league->getLeagueName($uniTeam['league'])."</td>
                                    <td>";
                                echo '<form action="index.php?page=team" method="POST">
                                        <input type="text" name="applyToTeam" value="true" style="display:none" readonly>
                                        <input type="text" name="team" value="'.$uniTeam['id'].'" style="display:none" readonly>
                                    <input type="submit" value="&#10003;" class="btn btn-info" style="float:left; padding: 0px 5px !important; margin-left:5px;"></form>
                                    </td>
                                </tr>';
                        }
                    } else {
                        echo "<tr>
                                <th scope=\"row\" colspan=\"5\">    Aktualnie brak utworzonych drużyn na twojej uczelni.</th>
                              </tr>";
                    }
                ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4">
            <h4>Otrzymane zaproszenia</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>Zaproszony przez</th>
                        <th>Drużyna</th>
                        <th>Akceptuj</th>
                    </tr>
                </thead>
                <tbody>
                   <?php
                        if (!empty($invList)) {
                            foreach($invList as $row) {
                                echo "<tr>
                                        <td>".$user->getUserName($row['from'])."</td>
                                        <td>".$team->getTeamName($row['teamid'])."</td>
                                        <td>";
                                    echo '<form action="index.php?page=team" method="POST">
                                            <input type="text" name="acceptInvite" value="true" style="display:none" readonly>
                                            <input type="text" name="team" value="'.$row['teamid'].'" style="display:none" readonly>
                                        <input type="submit" value="&#10003;" class="btn btn-info" style="float:left; padding: 0px 5px !important; margin-left:5px;"></form>';
                                echo "</td></tr>";
                            }
                        } else {
                            echo "<tr>
                                    <td colspan=\"2\"> Brak nowych zaproszeń. </td>
                                </tr>";
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
                
                



<div class="container boxes">
		<div class="row section-1">
			<div class="col-lg-4 thumb-img">
				<img src="images/templates/csgo.jpg" alt="Opis" class="img-responsive">
			</div>
			<div class="col-lg-4">
				<h1>DOPASOWANIE DRUŻYNY</h1>
				<div class="rangs clearfix">
					<div class="team-image left">
						<img src="images/team/team-rang.jpg" class="img-responsive">
					</div>
					<div class="team-rangs left">
						<h3>Bez rankingu</h3>
						<p>Aby otrzymać pozycję w lidze rankingowej, musisz rozegrać jeszcze 3 razy</p>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="meeting">
					<div class="meeting-date left">
						<h2>NAJBLIŻSZE SPOTKANIE</h2>
						<p>24 listopada 2017</p>
					</div>
					<div class="start-game right"><a href="#"><button type="button">GRAJ</button></a></div>
				</div>
			</div>
			<div class="col-lg-4">
				<h1>RANKING</h1>
				<table class="table extend-table">
					<thead>
						<tr>
							<th>Poz.</th>
							<th>Drużyna</th>
							<th>Punkty</th>
						</tr>
						<tbody>
                            <?php $i = '1'; 
                            echo '
							<tr style="font-weight: bold;">
								<td>'.$leagueScoresMyTeam["place"].'</td>
								<td>'.$leagueScoresMyTeam["tag"].'</td>
								<td>'.$leagueScoresMyTeam["points"].'</td>
							</tr>
                            <tr>
								<td>...</td>
								<td>...</td>
								<td>...</td>
							</tr>';
                            foreach($leagueScoresMini as $leagueTeam){
                            echo '
							<tr>
								<td>'.$leagueTeam["place"].'</td>
								<td>'.$leagueTeam["tag"].'</td>
								<td>'.$leagueTeam["points"].'</td>
							</tr>'; }
                            ?>
						</tbody>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div class="container boxes">
		<div class="row section-2">
			<div class="col-lg-6 col-lg-offset-1">
				<h1>TURNIEJE</h1>
				<!--<div class="tournaments-table clearfix">
					<div class="row-tables clearfix">
						<div class="left col-2">
							<div>
								<h3>OTWÓRZ</h3>
								<p>1</p>
							</div>
						</div>
						<div class="left col-2">
							<div>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</div>
						</div>
						<div class="left col-1">
							<p>5 Nagrody</p>
						</div>
						<div class="left col-1">
							<p>85 453</p>
						</div>
					</div>
					<div class="row-tables clearfix">
						<div class="left col-2">
							<div>
								<h3>OTWÓRZ</h3>
								<p>2</p>
							</div>
						</div>
						<div class="left col-2">
							<div>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</div>
						</div>
						<div class="left col-1">
							<p>5 Nagrody</p>
						</div>
						<div class="left col-1">
							<p>85 453</p>
						</div>
					</div>
					<div class="row-tables clearfix">
						<div class="left col-2">
							<div>
								<h3>OTWÓRZ</h3>
								<p>3</p>
							</div>
						</div>
						<div class="left col-2">
							<div>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</div>
						</div>
						<div class="left col-1">
							<p>5 Nagrody</p>
						</div>
						<div class="left col-1">
							<p>85 453</p>
						</div>
					</div>
					<div class="row-tables clearfix">
						<div class="left col-2">
							<div>
								<h3>OTWÓRZ</h3>
								<p>4</p>
							</div>
						</div>
						<div class="left col-2">
							<div>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</div>
						</div>
						<div class="left col-1">
							<p>5 Nagrody</p>
						</div>
						<div class="left col-1">
							<p>85 453</p>
						</div>
					</div>
					<div class="all-tournaments clearfix">
						<h2>POKAŻ WSZYSTKIE TURNIEJE</h2>
						<div class="arrow"><img src="images/arrow-down.png" alt=""></div>
					</div>
				</div>-->

				<table class="table table-tournaments">
					<tbody>
						<tr>
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 01</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
						<tr>
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 02</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
						<tr>
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 03</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
						<tr>
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 04</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
						<tr class="clickable" id="69">
							<td colspan="4">
								<h2>POKAŻ WSZYSTKIE TURNIEJE</h2>
								<img src="images/arrow-down.png" alt="">
							</td>
						</tr>
						<tr class="collapse out budgets 69collapsed">
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 01</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
						<tr class="collapse out budgets 69collapsed">
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 02</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
						<tr class="collapse out budgets 69collapsed">
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 03</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
						<tr class="collapse out budgets 69collapsed">
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 04</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
						<tr class="collapse out budgets 69collapsed">
							<td>
								<h3>OTWÓRZ</h3>
								<p>NUMER: 05</p>
							</td>
							<td>
								<p>SteelSeries Polish</p>
								<p>Weekly Leauge</p>
							</td>
							<td>
								<p>5 Nagrody</p>
							</td>
							<td>
								<p>85 426</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-lg-4">
				<h1>PULA NAGRÓD</h1>
				<div class="awards">
					<a role="button" tabindex="0" data-container="body" data-toggle="popover" data-placement="left" title="Myszka Razer" data-content="Wartość 259zł"><img src="images/awards/01.jpg" alt="" class="img-responsive" ></a>
					<a role="button" tabindex="0" data-container="body" data-toggle="popover" data-placement="left" title="Klawiatura Razer" data-content="Wartość 200zł"><img src="images/awards/02.jpg" alt="" class="img-responsive"></a>
					<a role="button" tabindex="0" data-container="body" data-toggle="popover" data-placement="left" title="Słuchawki Razer" data-content="Wartość 159zł"><img src="images/awards/03.jpg" alt="" class="img-responsive"></a>
					<a role="button" tabindex="0" data-container="body" data-toggle="popover" data-placement="left" title="Opaska Razer" data-content="Wartość 59zł"><img src="images/awards/04.jpg" alt="" class="img-responsive"></a>
				</div>
			</div>
		</div>
	</div>
	<div class="container boxes">
		<div class="row section-3">
			<div class="col-lg-3 meeting-info">
				<div>
					<h1>NASTĘPNE SPOTKANIE</h1>
					<h2>VIRTUS PRO VS GAINTS</h2>
				</div>
			</div>
			<div class="col-lg-2 logo-meeting-1">
				<img src="images/logo/virtus.png" alt="" class="img-responsive">
			</div>
			<div class="col-lg-2 versus">
				<h1>VS</h1>
			</div>
			<div class="col-lg-2 logo-meeting-2">
				<img src="images/logo/gaints.png" alt="" class="img-responsive">
			</div>
			<div class="col-lg-3 match-saw">
				<h1>OBEJRZYJ MECZ</h1>
				<img src="images/arrow.png" alt="" class="img-responsive">
			</div>
		</div>
	</div>
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
class Json{

	private $pdo = null;
	
	/**
     * @var null
     */
    private $session = null;
			
	public function __construct($pdo,$session){
			
		if (is_object($pdo)) {
			$this->pdo = $pdo;
		}
		
		if (is_object($session)) {
            $this->session = $session;
        }
	}

	public function JsonThread($user,$jsonCommand){
		$case = $jsonCommand->dataAction;
		$data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
		if (empty($data['login']) || empty($data['id'])) { return false; }
				else {
					switch($case)
					{
						case 'onlineCheck':{
							$user = new User($this->pdo);
							$userData = $user -> getUserAllData();
							$team = new Team($this->pdo);
							$checkOnlineInTeam = $team -> getOnlineMembers($userData['team']);
							echo json_encode($checkOnlineInTeam);
						} break;
                            
                        case 'lobbyConnected':{
                            $lobby = new Lobby($this->pdo,$this->session);
                            $lobby -> setLobbyCaptains();
                            $captainsArr = array("team1_captain" => $lobby->team1_captain, "team2_captain" => $lobby->team2_captain,"team1_captain_name" => $user->getUserName($lobby->team1_captain), "team2_captain_name" => $user->getUserName($lobby->team2_captain));
                            $lobbyData = array($lobby->getJoinedPlayers(),$captainsArr);
                            echo json_encode($lobbyData);
                        } break;
                            
                        case 'lobbyUpdate':{
                            $lobby = new Lobby($this->pdo,$this->session);
                            echo json_encode($lobby -> getConnectionParameter());
                        } break; 
                            
                        case 'firstlyConnected':{
                            $lobby = new Lobby($this->pdo,$this->session);
                            $lobbyJoined = $lobby->getJoinedPlayers();
                            $firstlyConnectedTeam1 = null;
                            $firstlyConnectedTeam2 = null;
                            foreach($lobby->team1_players as $teamPlayers){
                                foreach($lobbyJoined as $joinedPlayer) {
                                    if (!isset($firstlyConnectedTeam1) && $teamPlayers['id'] == $joinedPlayer) {
                                        $firstlyConnectedTeam1 = $joinedPlayer;
                                    }
                                }
                            }
                            foreach($lobby->team2_players as $teamPlayers){
                                foreach($lobbyJoined as $joinedPlayer) {
                                    if (!isset($firstlyConnectedTeam2) && $teamPlayers['id'] == $joinedPlayer) {
                                        $firstlyConnectedTeam2 = $joinedPlayer;
                                    }
                                }
                            }
                            //echo json_encode(array("conn_t1" => $firstlyConnectedTeam1, "conn_t2" => $firstlyConnectedTeam2));
                            $hs_battletag1 = $user -> getUserGameLogins($firstlyConnectedTeam1,core::$config['game']['hs_id']);
                            $hs_battletag2 = $user -> getUserGameLogins($firstlyConnectedTeam2,core::$config['game']['hs_id']);
                            echo json_encode(array("conn_t1" => $hs_battletag1['login'], "conn_t2" => $hs_battletag2['login']));
                        } break;
					}
				}
	}

}

$user = new User($pdo);
$json = new Json($pdo, $session);
$json -> JsonThread($user,json_decode($_POST["json"], false));

?>



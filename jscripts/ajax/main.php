<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
class Ajax{

	private $pdo = null;
	
	/**
     * @var null
     */
    private $session = null;
			
	public function __construct($pdo,$session){
			
		if (is_object($pdo)) {
			$this->pdo = $pdo;
		}
		
		if (is_object($session)) {
            $this->session = $session;
        }
	}

	public function AjaxThread($user,$case){
		$data = ['login' => $_SESSION['login'], 'id' => $_SESSION['id']];
        if ($case == "search_league") {
            $data = ['login' => "guest", 'id' => "guest"];
        }
		if (empty($data['login']) || empty($data['id'])) { return false; }
				else {
					switch($case)
					{
						case 'queue':{
							$user   = new User($this->pdo);
                            $team   = new Team($this->pdo);
                            $game   = new Game($this->pdo,$this->session);
                            $league = new League($this->pdo);
                            $queue  = new Queue($this->pdo);
							$userData    = $user -> getUserQueueData();
                            $teamData    = $team -> getDataTeam($userData['team']);
							$gameData    = $game -> getGameData($teamData['game']);
                            
                            $league -> selectLeague($teamData['league']);
                            if ($teamData['game'] == core::$config['game']['hs_id']) {
                                $leagueMeet  = $league -> getLeagueCloseMeet($userData['team'], "30");
                            } else {
                                $leagueMeet  = $league -> getLeagueCloseMeet($userData['team']);
                            }
                            

                            if (isset($leagueMeet['id'])) {
                                $meetData = $queue -> initializeQueue($userData,$gameData,$leagueMeet);
                                if (isset($meetData)) {
                                    echo $meetData["slots"]."/".$meetData["max_slots"]." ";
                                } else {
                                    echo '<span style="text-align:center;">Wyszukiwanie kolejki..</span>';
                                }
                                for($i = 1; $i <= $meetData["max_slots"]; $i++){
                                    if ($i <= $meetData["slots"]) {
                                        echo '<img src="https://cdn3.iconfinder.com/data/icons/security-2-1/512/valid-256.png" width="15" height="15">';
                                    } else {
                                        echo '<img src="https://maxcdn.icons8.com/Share/icon/Very_Basic//cancel1600.png" width="15" height="15">';
                                    }
                                }
                                if ( isset($meetData["slots"]) && $meetData["slots"] >= $meetData["max_slots"] ){
                                    $this->session->set(["lobby" => $meetData['id']]);
                                    echo "<div class='center-item' style='width:100px;clear:both;'><a href='?page=lobby'><button>GOTOWY</button></a></div>";
                                }
                            } elseif (!isset($leagueMeet['id']) && isset($leagueMeet['time_left'])) {
                                echo "Pozostało do rozpoczęcia : ".$leagueMeet['time_left'];
                            } else {
                                echo "Brak spotkania, do uruchomienia kolejki.";
                            }
						} break;
						
						case 'online':{
							$user = new User($this->pdo);
							$userOnline = $user -> setOnlineActivity();
						} break;
                            
                        case 'search_user':{
                            $usersList = $user->getUsersList($_GET['string']);
                            if ( !empty($usersList) ){
                                foreach($usersList as $userKeys)
                                {
                                    echo "<div class=\"search_user_box_element\" onclick=\"innerValue('search_user_id','".$userKeys['id']."'); innerValue('search_user_input','".$userKeys['name']." ".$userKeys['surname']."'); document.getElementById('search_user_box').style.display='none'; return false;\">".$userKeys['name']." ".$userKeys['surname']."</div>";
                                }
                            }
                        } break;
                            
                        case 'overwatch_modes':{
                            $lobby          = new Lobby($this->pdo,$this->session);
                            $game_overwatch = new Overwatch($lobby);
                            $gameMods = $game_overwatch -> iniGameModes();
                            $modeBanCounts = array_count_values($gameMods);
                            
                            echo "PICKER : ".$user->getUserName($lobby->picker)."<br>";
                            
                            if($modeBanCounts[0] <= 2){
                                $gameMaps = $game_overwatch -> iniGameMaps();

                                $map1BanCounts = array_count_values($gameMaps[0][1]);
                                $map2BanCounts = array_count_values($gameMaps[0][2]);
                                if($map1BanCounts[0] != 1){
                                    if(isset($_GET['ban_map1'])){
                                        foreach($gameMaps[0][1] as $map => $banned_bool){
                                            if($map == $_GET['ban_map1']) { $gameMaps[0][1][$_GET['ban_map1']] = "1";}
                                        }
                                        $game_overwatch->lobby->dbSerialArr("banned", $gameMaps);
                                        $lobby->swapLobbyPicker();
                                    }
                                    foreach($gameMaps[0][1] as $map => $banned_bool){
                                        echo "<div style='clear:both;margin-bottom:20px;"; echo $banned_bool==1 ? "color:red;" : ""; 
                                        if ($banned_bool == 0 && $lobby->picker == $_SESSION['id']) {
                                            echo "' onclick=\"reqServer('gameMode','overwatch_modes&ban_map1=".$map."');\">".$map."</div>";
                                        } else {
                                            echo "'>".$map."</div>";
                                        }
                                    }
                                } else {
                                    if($map2BanCounts[0] != 1){
                                        if(isset($_GET['ban_map2'])){
                                            foreach($gameMaps[0][2] as $map => $banned_bool){
                                                if($map == $_GET['ban_map2']) { $gameMaps[0][2][$_GET['ban_map2']] = "1";}
                                            }
                                            $game_overwatch->lobby->dbSerialArr("banned", $gameMaps);
                                            $lobby->swapLobbyPicker();
                                        }
                                        foreach($gameMaps[0][2] as $map => $banned_bool){
                                            echo "<div style='clear:both;margin-bottom:20px;"; echo $banned_bool==1 ? "color:red;" : ""; 
                                            if ($banned_bool == 0 && $lobby->picker == $_SESSION['id']) {
                                                echo "' onclick=\"reqServer('gameMode','overwatch_modes&ban_map2=".$map."');\">".$map."</div>";
                                            } else {
                                                echo "'>".$map."</div>";
                                            }
                                        }
                                    } else {                                    
                                        echo "MAPS CHOOSEN";
                                    }
                                }
                            } else {
                                if(isset($_GET['ban_mode'])){
                                    foreach($gameMods as $mode => $banned_bool){
                                        if($mode == $_GET['ban_mode']) { $gameMods[$_GET['ban_mode']] = "1";}
                                    }
                                    $game_overwatch->lobby->dbSerialArr("banned", $gameMods);
                                    $lobby->swapLobbyPicker();
                                }
                                foreach($gameMods as $mode => $banned_bool){
                                    echo "<div style='clear:both;margin-bottom:20px;"; echo $banned_bool==1 ? "color:red;" : ""; 
                                    if ($banned_bool == 0 && $lobby->picker == $_SESSION['id']) {
                                        echo "' onclick=\"reqServer('gameMode','overwatch_modes&ban_mode=".$mode."');\">".$mode."</div>";
                                    } else {
                                        echo "'>".$mode."</div>";
                                    }
                                }
                            }
                            
                        } break;
                            
                        case 'captain_picker_changer':{
                            $lobby      = new Lobby($this->pdo,$this->session);
                            $lobby -> setLobbyCaptains();
                            if (!isset($_SESSION['captain_picker'])) {
                                $this->session->set(['captain_picker' => $lobby->team1_captain]);
                            } else {
                                if ($_SESSION['captain_picker'] == $lobby->team1_captain) {
                                    $_SESSION['captain_picker'] = $lobby->team2_captain; 
                                } else {
                                    $_SESSION['captain_picker'] = $lobby->team1_captain;
                                }
                            }
                        } break;
                            
                        
                        case 'search_league':{
                            if( isset($_GET['parent']) ){
                                $league = new League($this->pdo);
                                $league -> selectLeague($_GET['parent']);
                                $leagueChilds = $league -> getLeagueChilds($_GET['parent'],$_GET['string']);
                            }
                            $leagueSubNum=1;
                            foreach($leagueChilds as $leagueSub){
                                echo "<tr>
                                        <td>
                                            <p># ".$leagueSubNum."</p>
                                        </td>
                                        <td>
                                            <p>".$leagueSub['id']." # <a href=\"index.php?page=league&ind=".$leagueSub['id']."\" style=\"color:red;\">OTWÓRZ</a></p>
                                        </td>
                                        <td>
                                            <p>".$leagueSub['name']."</p>
                                            <p>Semester League</p>
                                        </td>
                                        <td>
                                            <p>".$leagueSub['status']."</p>
                                        </td>
                                        <td>
                                            <p>".$league->getLeagueTeamsNumber($leagueSub['id'])."</p>
                                        </td>
                                    </tr>";
                                $leagueSubNum++;
                            }
                        } break;
                            
					}
				}
	}

}

$user = new User($pdo);
$ajax = new Ajax($pdo, $session);
$ajax -> AjaxThread($user,strip_tags($_GET['ajax']));

?>



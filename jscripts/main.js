function reqServer(frame,action) {
    if (action.length == 0) { 
        document.getElementById(frame).innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById(frame).innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET", "index.php?ajax=" + action, true);
        xmlhttp.send();
    }
	
}
 
	 function iniRequest(frame,action) {
		  reqServer(frame, action); //first run
		  setInterval(function(){ reqServer(frame, action); }, 2000); //refresh
	}
 
function reqServerJSON(frame,action){
	var obj, dbParam, xmlhttp, myObj, x, txt = "";
	obj = { "dataAction":action };
	dbParam = JSON.stringify(obj);
	xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			myObj = JSON.parse(this.responseText);
			if (frame == "isOnline"){
				for (x in myObj) {
					/*txt = myObj[x].online + "<br>";
					txt += myObj[x].last_activity + "<br>";
					document.getElementById(frame + "_" + myObj[x].id).innerHTML = txt;*/

					if(myObj[x].online == 'T') { document.getElementById(frame + "_" + myObj[x].id).innerHTML = "<img src='https://cdn3.iconfinder.com/data/icons/security-2-1/512/valid-256.png' width='15' height='15'> /ON"; } 
					else { document.getElementById(frame + "_" + myObj[x].id).innerHTML = "<img src='https://maxcdn.icons8.com/Share/icon/Very_Basic//cancel1600.png' width='15' height='15'> /OFF"; }
					document.getElementById(frame + "_" + myObj[x].id).innerHTML += "<br> Ostatnia aktywność : " + myObj[x].last_activity;
				}
			}
            if (frame == "isConnected"){
                for (x in myObj[0]) {
                    document.getElementById(frame + "-" + myObj[0][x]).innerHTML = "CONNECTED";
                }
                if(myObj[1].team1_captain != null){ document.getElementById(frame + "-cpt-" + myObj[1].team1_captain).style.display = "initial"; }
                if(myObj[1].team2_captain != null){ document.getElementById(frame + "-cpt-" + myObj[1].team2_captain).style.display = "initial"; }
                
                
                if ($("#lol-lobby-creator").length) {
                    console.log(myObj[1]);
                    if(myObj[1].team1_captain != null){ 
                        document.getElementById("lol-lobby-creator").innerHTML = myObj[1].team1_captain_name;
                    }
                }
            }
            if (frame == "lobby-status-configuring") {
                if(myObj != null) {
                    if(myObj.ip != null){ document.getElementById("lobby-name").value = myObj.ip; }
                    if(myObj.pw != null){ document.getElementById("lobby-pw").value = myObj.pw; }
                    if(myObj.ip != null && myObj.pw != null) { 
                        document.getElementById("lobby-status-configuring").style.display = "none";
                        document.getElementById("lobby-status-ready").style.display = "initial";
                    }
                }
            }
            if (frame == "hs-battletag"){
                if(myObj.conn_t1 != null){ 
                    if ( $("#"+frame+"-t1").length ) { document.getElementById(frame + "-t1").value = myObj.conn_t1; }
                }
                if(myObj.conn_t2 != null){ 
                    if ( $("#"+frame+"-t2").length ) { document.getElementById(frame + "-t2").value = myObj.conn_t2; }
                }
            }
		}
	};
	xmlhttp.open("POST", "index.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("json=" + dbParam);
}

	function iniJSONRequest(frame,action) {
      reqServerJSON(frame, action); //first run
	  setInterval(function(){ reqServerJSON(frame, action); }, 2000); //refresh
	}

	//Run tasks 
 $("#online").ready(function() {
   		iniRequest("online","online");
		if ( $('#team_viewer').length ) { 
			iniJSONRequest("isOnline","onlineCheck"); 
		};
        if ( $('#lobby_viewer').length ) { 
            iniJSONRequest("isConnected","lobbyConnected"); 
            if ( $('#lobby-status-configuring').length ) { 
                iniJSONRequest("lobby-status-configuring","lobbyUpdate"); 
            };
            if ( $('#game-overwatch').length ) { 
                iniRequest("gameMode","overwatch_modes"); 
            };
            if ( $('#hs-lobby').length) {
                iniJSONRequest("hs-battletag","firstlyConnected");
            };
        };
        $("#search_user_input").on("paste keyup", function() {
            document.getElementById("search_user_box").style.display='initial';
            reqServer("search_user_box","search_user&string="+$("#search_user_input").val());
        });
        $("#search_league_input").on("paste keyup", function() {
            reqServer("search_league_box","search_league&parent="+$("#search_league_parent").val()+"&string="+$("#search_league_input").val());
        });
 	});

function innerValue(frame,value){
    document.getElementById(frame).value = value;
}

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).val()).select();
  document.execCommand("copy");
  $temp.remove();
}
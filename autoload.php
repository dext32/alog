<?php
//Define autoloader
spl_autoload_register('__autoload');

//Include vendor autoloader
require_once 'vendor/autoload.php';

function __autoload($className) {

    if (file_exists($className . '.php')) {
        require_once $className . '.php';
        return true;
    }
    if (file_exists('core/'.$className . '.class.php')) {
        require_once 'core/'.$className . '.class.php';
        return true;
    }
    if (file_exists('core/Controllers/'.$className . '.php')) {
        require_once 'core/Controllers/'.$className . '.php';
        return true;
    }
    if (file_exists('core/Repositories/'.$className . '.php')) {
        require_once 'core/Repositories/'.$className . '.php';
        return true;
    }
    return false;
}
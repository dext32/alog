SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE DATABASE IF NOT EXISTS `LIGA` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `LIGA`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` VARCHAR(30) NOT NULL, 
  `surname` VARCHAR(30) NOT NULL, 
  `university` INT(10) NULL DEFAULT '0', 
  `adress` VARCHAR(150) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `privilege` int(3) NOT NULL DEFAULT '1',
  `level` varchar(10) NOT NULL DEFAULT '1',
  `lobby` INT(10) NULL DEFAULT NULL,
  `team` INT(10) NULL DEFAULT NULL,
  `last_activity` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `liga`.`usersgames` ( 
    `user` INT(10) NOT NULL , 
    `game` INT(10) NOT NULL ,
    `login` VARCHAR(255) NOT NULL 
) ENGINE = InnoDB;

CREATE TABLE `liga`.`queue` ( 
	`id` INT(255) NOT NULL AUTO_INCREMENT,
	`game` VARCHAR(255) NOT NULL , 
    `team1` INT(10) NOT NULL ,
    `team2` INT(10) NOT NULL ,
	`level` VARCHAR(10) NULL DEFAULT NULL , 
	`slots` INT(10) NOT NULL , 
	`createdBy` VARCHAR(255) NOT NULL ,
	PRIMARY KEY (`id`)
	) ENGINE = InnoDB;
	
CREATE TABLE `liga`.`lobby` ( 
    `id` INT(10) NOT NULL , 
    `team1` INT(10) NOT NULL , 
    `team2` INT(10) NOT NULL ,
    `game` INT(10) NOT NULL ,
    `start_time` VARCHAR(255) NOT NULL , 
    `joined` VARCHAR(255) NOT NULL DEFAULT 'a:0:{}' ,
    `banned` VARCHAR(250) NOT NULL DEFAULT 'a:0:{}' ,
    `picker` INT(10) NULL ,
    `server` VARCHAR(255) NOT NULL DEFAULT 'a:0:{}'
    ) ENGINE = InnoDB;    
    
CREATE TABLE `liga`.`lobbyreports` (
    `id` INT(10) NOT NULL AUTO_INCREMENT, 
    `lobby` INT(10) NOT NULL , 
    `user` INT(10) NOT NULL , 
    `info` VARCHAR(255) NOT NULL ,
    `screenshot` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE = InnoDB;    
    
CREATE TABLE `liga`.`games` ( 
	`id` INT(10) NOT NULL AUTO_INCREMENT ,
	`name` VARCHAR(255) NOT NULL , 
	`max_slots` INT(10) NOT NULL , 
	PRIMARY KEY (`id`)
	) ENGINE = InnoDB;

CREATE TABLE `liga`.`gameservers` (
    `id` INT(10) NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR(250) NOT NULL ,
    `ip` VARCHAR(250) NOT NULL ,
    `port` VARCHAR(10) NOT NULL ,
    `lobby_id` INT(10) NULL ,
    `run` BOOLEAN NOT NULL ,
    `in_use` BOOLEAN NOT NULL ,
    `start_time` VARCHAR(255) NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE = InnoDB;
	
CREATE TABLE `liga`.`teams` ( 
	`id` INT(10) NOT NULL AUTO_INCREMENT ,
	`tag` VARCHAR(30) NOT NULL , 
	`name` VARCHAR(50) NOT NULL ,
	`leader` INT(10) NOT NULL , 
	`university` INT(10) NOT NULL ,
	`game` INT(10) NOT NULL DEFAULT '0',
	`league` INT(10) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
	) ENGINE = InnoDB;
    
CREATE TABLE `liga`.`teamInvites` (
	`from` VARCHAR(25) NOT NULL ,
	`to` VARCHAR(25) NULL DEFAULT NULL,
	`teamid` INT(10) NOT NULL ,
	`type` INT(5) NOT NULL DEFAULT '1',
	`flag` INT(1) NOT NULL DEFAULT '0'
	) ENGINE = InnoDB;
	
CREATE TABLE `liga`.`university` (
	`id` INT(11) NOT NULL , 
	`name` VARCHAR(100) NOT NULL 
	) ENGINE = InnoDB;	
	
CREATE TABLE `liga`.`league` ( 
	`id` INT(10) NOT NULL AUTO_INCREMENT ,
	`name` VARCHAR(250) NOT NULL , 
	`game` INT(10) NOT NULL , 
    `type` VARCHAR(30) NOT NULL,
    `parent` INT(10) NULL DEFAULT NULL,
    `status` VARCHAR(10) NOT NULL DEFAULT 'open',
	PRIMARY KEY (`id`)
	) ENGINE = InnoDB;
	
CREATE TABLE `liga`.`leaguescores` ( 
    `lobby` INT(10) NOT NULL ,
	`team1` INT(10) NOT NULL , 
	`team2` INT(10) NOT NULL , 
	`score1` INT(10) NULL DEFAULT NULL , 
	`score2` INT(10) NULL DEFAULT NULL , 
	`date` VARCHAR(255) NOT NULL 
	) ENGINE = InnoDB;
    
CREATE TABLE `liga`.`leaguemeets` (
    `id` INT(10) NOT NULL AUTO_INCREMENT , 
    `team1` INT(10) NOT NULL , 
    `team2` INT(10) NOT NULL , 
    `league` INT(10) NOT NULL , 
    `date` VARCHAR(100) NOT NULL , 
    PRIMARY KEY (`id`)
    ) ENGINE = InnoDB;
 
 
 -- ACL extend User
 
CREATE TABLE roles (
  role_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  role_name VARCHAR(50) NOT NULL,

  PRIMARY KEY (role_id)
);

CREATE TABLE permissions (
  perm_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  perm_desc VARCHAR(50) NOT NULL,

  PRIMARY KEY (perm_id)
);

CREATE TABLE role_perm (
  role_id INTEGER UNSIGNED NOT NULL,
  perm_id INTEGER UNSIGNED NOT NULL,

  FOREIGN KEY (role_id) REFERENCES roles(role_id),
  FOREIGN KEY (perm_id) REFERENCES permissions(perm_id)
);

CREATE TABLE user_role (
  user_id INTEGER UNSIGNED NOT NULL,
  role_id INTEGER UNSIGNED NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id),
  FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

-- Alternative references users(id)
ALTER TABLE `user_role` ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `user_role` ADD CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles`(`role_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
